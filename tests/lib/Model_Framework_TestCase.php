<?php
/**
 * Model_Framework_TestCase.php Class for do model testing
 */

define( 'CORE__BASE_DIR'        , realpath( dirname( __FILE__ ) . '/../../' ) );
define( 'CORE__CLASSES_DIR'		, CORE__BASE_DIR . '/app/class' );
define( 'CORE__CONFIG_DIR'      , CORE__BASE_DIR . '/app/config' );

require_once CORE__CONFIG_DIR . '/defines.config.php';

require_once 'app/class/database.iface.php';
require_once 'app/class/mysqldatabase.php';
require_once 'app/class/model.php';


/**
 * Class for do model testing
 *
 * This class allow us to create temporary tables and insert data through
 * arrays.
 *
 * @author meneame group.
 */
class Model_Framework_TestCase extends PHPUnit_Framework_TestCase
{
	/**
	 * Class for database connection
	 *
	 * @var Mysqli
	 */
	protected $db;

	/**
	 * Set a database connection
	 *
	 * @param string $host
	 * @param string $user
	 * @param string $pass
	 * @param string $database
	 */
	public function setDatabaseConnection( $profile )
	{
		$this->db = MysqlDatabase::getInstance( $profile );
	}

	/**
	 * Insert all provided data in temporary tables
	 *
	 * $data example:
	 * 		array( 'table_name' => array(
	 *									array(
	 *										'field1' => 'value1',
	 *										'field2' => 'value2',
	 *										'field3' => 'value3'
	 *									),
	 *									array(
	 *										'field1' => 'value10',
	 *										'field2' => 'value20',
	 *										'field3' => 'value30'
	 *									),
	 * 								)
	 *
	 * @param array $data
	 */
	public function insertData( $data )
	{
		foreach ( $data AS $table => $rows )
		{
			$this->createTemporary( $table );

			foreach ( $rows AS $fields )
			{
				$this->insertRow( $table, $fields );
			}
		}
	}

	/**
	 * Insert a row in given table with given fields
	 *
	 * @param string $table
	 * @param array $fields
	 */
	protected function insertRow( $table, $fields )
	{
		$fields_name 	= implode( '`,`', array_keys( $fields ) );
		$fields_values 	= implode( '\',\'', $fields );

		$sql =<<<SQL
INSERT INTO {$table}
	(`{$fields_name}`)
VALUES
	('{$fields_values}');
SQL;
		$this->db->query( $sql );
	}

	/**
	 * Create a temporary table
	 *
	 * @param string $table_name
	 */
	protected function createTemporary( $table_name )
	{
		$sql 			= 'SHOW CREATE TABLE '. $table_name;
		list( $row ) 	= $this->db->query( $sql );

		$sql = $this->parse( $row['Create Table'] );

		if ( !$sql )
		{
			$sql = 'TRUNCATE TABLE ' . $table_name;
		}

		$this->db->query( $sql );
	}

	/**
	 * Parse original create table for cleanup innodb attributes.
	 * If original table is temporary return false.
	 *
	 * @param string $create_table
	 * @return mixed Return sql for create temporary table or false if table is temporary.
	 */
	protected function parse( $create_table )
	{
		if ( 0 === preg_match('/CREATE TEMPORARY TABLE/', $create_table ) )
		{
			$create_table = preg_replace( '/CREATE TABLE/' , 'CREATE TEMPORARY TABLE' , $create_table );
			$create_table = preg_replace( '/CONSTRAINT .* FOREIGN KEY .* REFERENCES .*/' , '' , $create_table );
			$create_table = preg_replace( '/,?\n( *\n)+? *\) ENGINE.*/m', ')', $create_table );

			return $create_table;
		}

		return false;
	}
}
?>
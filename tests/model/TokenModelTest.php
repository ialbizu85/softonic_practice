<?php
require_once 'tests/lib/Model_Framework_TestCase.php';
require_once 'app/model/token.model.php';

/**
 * TokenModelTest.php tester class
 *
 * @author meneame group
 */

/**
 * Token model tester class
 */
class TokenModelTest extends Model_Framework_TestCase
{
	/**
	 * TokenModel instance
	 * @var TokenModel
	 */
	private $token_model;


	/**
	 * Create TokenModel instance
	 */
	public function  setUp( )
	{
		$this->token_model = new TokenModel( );
	}

	/**
	 * Test generate two numeric tokens, must return tho different numbers
	 */
	public function testGenerateNumericWithTwiceExecutionWillReturnDifferentResults( )
	{
		$first_token	= $this->token_model->generateNumeric( );
		$second_token 	= $this->token_model->generateNumeric( );

		$this->assertNotEquals( $first_token, $second_token, 'Two numeric tokens not must be the same' );
	}

	/**
	 * Test generate two alphanumeric tokens, must return tho different numbers
	 */
	public function testGenerateAlphanumericWithTwiceExecutionWillReturnDifferentResults( )
	{
		$first_token	= $this->token_model->generateAlphanumeric('username', '123456789');
		$second_token 	= $this->token_model->generateAlphanumeric('otheruser', '123456789');

		$this->assertNotEquals( $first_token, $second_token, 'Two alphanumeric tokens not must be the same' );
	}
	
	/**
	 * Test getToken method.
	 */
	public function testGetTokenNonExistentUserReturnFalse( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'zarba',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										),
						),
						'mnm_user_token' => array( 
												array(
													'user_id' => 1,
													'token' => 'abcDE12367wertyd4ty6aswerdfgtyace4569ij'
												)

										)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );
		
		$result = $this->token_model->getToken( 3 );
		$this->assertFalse( $result, 'Get Token method from non existent user expects to return False.' );
	}
	
	/**
	 * Test getToken method.
	 */
	public function testGetTokenExistentUserReturnTokenString( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'zarba',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										),
						),
						'mnm_user_token' => array( 
												array(
													'user_id' => 1,
													'token' => 'abcDE12367wertyd4ty6aswerdfgtyace4569ij'
												)

										)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );
		
		$result = $this->token_model->getToken( 1 );
		$this->assertEquals( $result, 'abcDE12367wertyd4ty6aswerdfgtyace4569ij', 
							'Get Token method from existent user expects to return True.' );
	}
	
	/**
	 * Test deleteToken method.
	 */
	public function testDeleteTokenReturnTrue( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'zarba',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										),
						),
						'mnm_user_token' => array( 
												array(
													'user_id' => 1,
													'token' => 'abcDE12367wertyd4ty6aswerdfgtyace4569ij'
												)

										)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );
		
		$result = $this->token_model->deleteToken( 1 );
		$this->assertTrue( $result, 'Delete token method expects to return True' );
	}
	
	/**
	 * Test setToken method.
	 */
	public function testSetTokenReturnTrue( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'zarba',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										),
										 array(
											'user_id' => 2,
											'username' => 'Javier',
											'password' => 'RHl987654',
											'email' => 'javier@hotmail.com',
											'status' => 'ACTIVE'
										)
						),
						'mnm_user_token' => array( )
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->token_model->setToken( 'abcDE12367wertyd4ty6aswerdfgtyace4569ijy', 1 );
		$this->assertTrue( $result, 'Set token method expects to return True' );
	}
}

?>
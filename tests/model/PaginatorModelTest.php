<?php
require_once 'tests/lib/Model_Framework_TestCase.php';
require_once 'app/model/paginator.model.php';

/**
 * PaginatorModelTest.php tester class
 *
 * @author meneame group
 */

/**
 * paginator model tester class
 */
class PaginatorModelTest extends Model_Framework_TestCase
{
	/**
	 * PaginatorModel instance
	 * @var PaginatorModel
	 */
	private $paginator_model;


	/**
	 * Create PaginatorModel instance
	 */
	public function  setUp( )
	{
		$this->paginator_model = new PaginatorModel( );
	}


	/**
	 * Test getPageRanges method with exist title slug
	 */
	public function testGetPageRangesReturnCurrenPage4TotalPages10InitialPage2FinalPage6( )
	{
		$paginator  = $this->paginator_model->getPagesRange( 4, 10, 2 );
		$this->assertEquals( 4, $paginator[ 'current_page' ] );
		$this->assertEquals( 10, $paginator[ 'total_pages' ] );
		$this->assertEquals( 2, $paginator[ 'initial_page' ] );
		$this->assertEquals( 6, $paginator[ 'final_page' ] );
	}


	/**
	 * Test getPageRanges method with exist title slug
	 */
	public function testGetPageRangesReturnCurrenPage2TotalPages10InitialPage1FinalPage5( )
	{
		$paginator  = $this->paginator_model->getPagesRange( 2, 10, 2 );
		$this->assertEquals( 2, $paginator[ 'current_page' ] );
		$this->assertEquals( 10, $paginator[ 'total_pages' ] );
		$this->assertEquals( 1, $paginator[ 'initial_page' ] );
		$this->assertEquals( 5, $paginator[ 'final_page' ] );
	}


	/**
	 * Test getPageRanges method with exist title slug
	 */
	public function testGetPageRangesReturnCurrenPageMinus1TotalPages10InitialPage1FinalPage5( )
	{
		$paginator  = $this->paginator_model->getPagesRange( -1, 10, 2 );
		$this->assertEquals( -1, $paginator[ 'current_page' ] );
		$this->assertEquals( 10, $paginator[ 'total_pages' ] );
		$this->assertEquals( 1, $paginator[ 'initial_page' ] );
		$this->assertEquals( 5, $paginator[ 'final_page' ] );
	}


	/**
	 * Test getPageRanges method with exist title slug
	 */
	public function testGetPageRangesReturnCurrenPage9TotalPages10InitialPage6FinalPage10( )
	{
		$paginator  = $this->paginator_model->getPagesRange( 9, 10, 2 );
		$this->assertEquals( 9, $paginator[ 'current_page' ] );
		$this->assertEquals( 10, $paginator[ 'total_pages' ] );
		$this->assertEquals( 6, $paginator[ 'initial_page' ] );
		$this->assertEquals( 10, $paginator[ 'final_page' ] );
	}


	/**
	 * Test getPageRanges method with exist title slug
	 */
	public function testGetPageRangesReturnCurrenPage15TotalPages10InitialPage6FinalPage10( )
	{
		$paginator  = $this->paginator_model->getPagesRange( 15, 10, 2 );
		$this->assertEquals( 15, $paginator[ 'current_page' ] );
		$this->assertEquals( 10, $paginator[ 'total_pages' ] );
		$this->assertEquals( 6, $paginator[ 'initial_page' ] );
		$this->assertEquals( 10, $paginator[ 'final_page' ] );
	}

	/**
	 * Test getPageRanges method with exist title slug
	 */
	public function testGetPageRangesReturnCurrenPage1TotalPages2InitialPage1FinalPage2( )
	{
		$paginator  = $this->paginator_model->getPagesRange( 1, 2, 2 );
		$this->assertEquals( 1, $paginator[ 'current_page' ] );
		$this->assertEquals( 2, $paginator[ 'total_pages' ] );
		$this->assertEquals( 1, $paginator[ 'initial_page' ] );
		$this->assertEquals( 2, $paginator[ 'final_page' ] );
	}


	/**
	 * Test getPageRanges method with exist title slug
	 */
	public function testGetPageRangesReturnCurrenPage5TotalPages6InitialPage1FinalPage6( )
	{
		$paginator  = $this->paginator_model->getPagesRange( 5, 6, 3 );
		$this->assertEquals( 5, $paginator[ 'current_page' ] );
		$this->assertEquals( 6, $paginator[ 'total_pages' ] );
		$this->assertEquals( 1, $paginator[ 'initial_page' ] );
		$this->assertEquals( 6, $paginator[ 'final_page' ] );
	}
	
	
	/**
	 * Test getTotalPages method with exist title slug
	 */
	public function testGetTotalPagesReturn2( )
	{
		$total_pages  = $this->paginator_model->getTotalPages( 10, 5 );
		$this->assertEquals( 2, $total_pages );
	}

	/**
	 * Test getTotalPages method with exist title slug
	 */
	public function testGetTotalPagesReturn3( )
	{
		$total_pages  = $this->paginator_model->getTotalPages( 11, 5 );
		$this->assertEquals( 3, $total_pages );
	}

	/**
	 * Test getTotalPages method with exist title slug
	 */
	public function testGetTotalPagesReturn4( )
	{
		$total_pages  = $this->paginator_model->getTotalPages( 7, 2 );
		$this->assertEquals( 4, $total_pages );
	}
}

?>
<?php
require_once 'tests/lib/Model_Framework_TestCase.php';
require_once 'app/model/news.model.php';

/**
 * NewsModelTest.php tester class
 *
 * @author meneame group
 */

/**
 * news model tester class
 */
class NewsModelTest extends Model_Framework_TestCase
{
	/**
	 * UserModel instance
	 * @var UserModel
	 */
	private $news_model;


	/**
	 * Create NewsModel instance
	 */
	public function  setUp( )
	{
		$this->news_model = new NewsModel( );
	}


	/**
	 * Test getNewsByTitleSlug method with exist title slug
	 */
	public function testGetNewsByTitleSlugReturnTrue( )
	{
		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE') ), 
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'proba bat da',
													'title_slug' => 'proba-bat-da',
													'summary' => 'Hau proba bat da.',
													'url' => 'www.proba.com',
													'create_at' => time( ),
													'votes' => 100 
												) ) );
		
		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );
		
		$result = $this->news_model->getNewsByTitleSlug( 'proba-bat-da' );
		$this->assertGreaterThan( 0, count( $result ), 'Deber�a de haber una noticia.' );
	}


	/**
	 * Test getNewsByTitleSlug method with not exist title slug
	 */
	public function testGetNewsByTitleSlugReturnFalse( )
	{
		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE') ),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'proba bat da',
													'title_slug' => 'proba-bat-da',
													'summary' => 'Hau proba bat da.',
													'url' => 'www.proba.com',
													'create_at' => time( ),
													'votes' => 100
												) ) );

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getNewsByTitleSlug( 'hau-ez-da-proba-bat' );
		$this->assertGreaterThan( 0, count( $result ), 'Deber�a de haber una noticia.' );
	}
	
	
	/**
	 * Test insertNews method
	 */
	public function testInsertNews( )
	{

		$data = array( 'mnm_user' => array(
										array('user_id' => 1,
												'username' => 'ion',
												'password' => 'Abc12345',
												'email' => 'albixu_euskadi@hotmail.com',
												'status' => 'ACTIVE')
						),
						'mnm_news' => array( )
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->insertNews( 'Prueba 1', 'Prueba-1', 'Esta es la prueba 1', 'http://meneame.ion.internship', 1 );
		$this->assertTrue( $result );
	}

	/**
	 * Test insert votes into news
	 */
	public function testInsertVoteIntoNewAddVoteToUserAndNoticeWillReturnTrue( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'ion',
											'password' => 'Abc12345',
											'email' => 'albixu_euskadi@hotmail.com',
											'status' => 'ACTIVE'
										),
										array(
											'user_id' => 2,
											'username' => 'ion2',
											'password' => 'Abc12345',
											'email' => 'albixu_euskadi2@hotmail.com',
											'status' => 'ACTIVE'
										)
					),
					'mnm_news' => array(
										array(
												'news_id' => 1,
												'user_id' => 1,
												'title' => 'Mi noticia',
												'summary' => 'Mi resumen',
												'url' => 'http://meneate.com'
											)
					),
					'mnm_news_vote' => array( )
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->insertVote( 1, 2 );
		$this->assertTrue( $result );
	}

	/**
	 * Test vote twice given false
	 */
	public function testInsertVoteIntoNewAddTwoVotesToUserAndNoticeWillReturnFalse( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'ion',
											'password' => 'Abc12345',
											'email' => 'albixu_euskadi@hotmail.com',
											'status' => 'ACTIVE'
										),
										array(
											'user_id' => 2,
											'username' => 'ion2',
											'password' => 'Abc12345',
											'email' => 'albixu_euskadi2@hotmail.com',
											'status' => 'ACTIVE'
										)
					),
					'mnm_news' => array(
										array(
												'news_id' => 1,
												'user_id' => 1,
												'title' => 'Mi noticia',
												'summary' => 'Mi resumen',
												'url' => 'http://meneate.com'
											)
					),
					'mnm_news_vote' => array( )
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->insertVote( 1, 2 );
		$this->assertTrue( $result );

		$result = $this->news_model->insertVote( 1, 2 );
		$this->assertFalse( $result );
	}
	
	
	/**
	 * Test getNewsVotes method.
	 */
	public function testGetNewsVotesWithId1Return2( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'ion',
											'password' => 'Abc12345',
											'email' => 'albixu_euskadi@hotmail.com',
											'status' => 'ACTIVE'
										),
										array(
											'user_id' => 2,
											'username' => 'ion2',
											'password' => 'Abc12345',
											'email' => 'albixu_euskadi2@hotmail.com',
											'status' => 'ACTIVE'
										)
					),
					'mnm_news' => array(
										array(
												'news_id' => 1,
												'user_id' => 1,
												'title' => 'Mi noticia',
												'summary' => 'Mi resumen',
												'url' => 'http://meneate.com',
												'votes' => 2
											)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getNewsVotes( 1 );
		$this->assertEquals( 2, $result, 'Debía devolver 2 en vez de ' . $result );
	}
	
	
	/**
	 * Test getNewsVotes method.
	 */
	public function testGetNewsVotesWithId1Return0( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'ion',
											'password' => 'Abc12345',
											'email' => 'albixu_euskadi@hotmail.com',
											'status' => 'ACTIVE'
										),
										array(
											'user_id' => 2,
											'username' => 'ion2',
											'password' => 'Abc12345',
											'email' => 'albixu_euskadi2@hotmail.com',
											'status' => 'ACTIVE'
										)
					),
					'mnm_news' => array(
										array(
												'news_id' => 1,
												'user_id' => 1,
												'title' => 'Mi noticia',
												'summary' => 'Mi resumen',
												'url' => 'http://meneate.com'
											)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getNewsVotes( 1 );
		$this->assertEquals( 0, $result, 'Debía devolver 0 en vez de ' . $result );
	}
	
	
	/**
	 * Test getNewsVotes method.
	 */
	public function testGetNewsVotesWithId2Return0( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'ion',
											'password' => 'Abc12345',
											'email' => 'albixu_euskadi@hotmail.com',
											'status' => 'ACTIVE'
										),
										array(
											'user_id' => 2,
											'username' => 'ion2',
											'password' => 'Abc12345',
											'email' => 'albixu_euskadi2@hotmail.com',
											'status' => 'ACTIVE'
										)
					),
					'mnm_news' => array(
										array(
												'news_id' => 1,
												'user_id' => 1,
												'title' => 'Mi noticia',
												'summary' => 'Mi resumen',
												'url' => 'http://meneate.com',
												'votes' => 2
											)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getNewsVotes( 2 );
		$this->assertEquals( 0, $result, 'Debía devolver 0 en vez de ' . $result );
	}
	

	/**
	 * Test that a valid news_id return that it exits (true)
	 */
	public function testIsValidWithValidNewsIdWillReturnTrue( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'ion',
											'password' => 'Abc12345',
											'email' => 'albixu_euskadi@hotmail.com',
											'status' => 'ACTIVE'
										)
					),
					'mnm_news' => array(
										array(
												'news_id' => 1,
												'user_id' => 1,
												'title' => 'Mi noticia',
												'summary' => 'Mi resumen',
												'url' => 'http://meneate.com'
											)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->isValid( 1 );
		$this->assertTrue( $result );
	}

	/**
	 * Test that a invalid news_id return that it not exits (false)
	 */
	public function testIsValidWithInvalidNewsIdWillReturnFalse( )
	{
		$data = array( 'mnm_news' => array( ) );

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->isValid( 1 );
		$this->assertFalse( $result );
	}

	/**
	 * Test that an url stored return true
	 */
	public function testIsRegisteredUrlWithAnExistingUrlWillReturnTrue( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'ion',
											'password' => 'Abc12345',
											'email' => 'albixu_euskadi@hotmail.com',
											'status' => 'ACTIVE'
										)
					),
					'mnm_news' => array(
										array(
												'news_id' => 1,
												'user_id' => 1,
												'title' => 'Mi noticia',
												'summary' => 'Mi resumen',
												'url' => 'http://meneate.com'
											)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->isUrlRegistered( 'http://meneate.com' );
		$this->assertTrue( $result, 'An existent url must return true' );
	}

	/**
	 * Test that an url not stored return false
	 */
	public function testIsRegisteredUrlWithAnInexistingUrlWillReturnFalse( )
	{
		$data = array( 'mnm_news' => array(	) );

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->isUrlRegistered( 'http://meneate.com' );
		$this->assertFalse( $result, 'An inexistent url must return false' );
	}

	/**
	 * Test that clean a cleaned uri, return the same
	 */
	public function testCleanUriWithAnNormalUriReturnNormalUri( )
	{
		$expected		=  'http://meneame.net';
		$cleaned_uri	= $this->news_model->cleanUri( $expected );

		$this->assertEquals( $expected, $cleaned_uri, 'A normal uri must return the same normal uri.' );
	}

	/**
	 * Test that anchor uri, return the same without anchor
	 */
	public function testCleanUriWithAnAnchorUriReturnUriWithoutAnchor( )
	{
		$expected		=  'http://meneame.net';
		$cleaned_uri	= $this->news_model->cleanUri( $expected . '#anchor' );

		$this->assertEquals( $expected, $cleaned_uri, 'An anchor uri must return the cleaned uri.' );
	}

	/**
	 * Test that multiple anchor uri, return the same without anchor
	 */
	public function testCleanUriWithAMultipleAnchorUriReturnUriWithoutAnchor( )
	{
		$expected		=  'http://meneame.net';
		$cleaned_uri	= $this->news_model->cleanUri( $expected . '#anchor1#anchor_ilegal' );

		$this->assertEquals( $expected, $cleaned_uri, 'A multiple uri must return the cleaned uri.' );
	}

	/**
	 * Test that a given url with final backslash, return an uri without backslash
	 */
	public function testCleanUriWithABackslashUriReturnUriWithoutBackslash( )
	{
		$expected		=  'http://meneame.net';
		$cleaned_uri	= $this->news_model->cleanUri( $expected . '/' );

		$this->assertEquals( $expected, $cleaned_uri, 'A backslashed uri must return the uri without backslash.' );
	}

	/**
	 * Test that all spaces are cleaned
	 */
	public function testCleanUriWithAUriWithSpacesAtTheEndReturnAnUriWithoutSpaces( )
	{
		$expected		=  'http://meneame.net';
		$cleaned_uri	= $this->news_model->cleanUri( $expected . '          ' );

		$this->assertEquals( $expected, $cleaned_uri, 'A backslashed uri must return the uri without backslash.' );
	}

	/**
	 * Test a complex uri return uri without anchors, backslash and extra spaces
	 */
	public function testCleanUriWithASpacesAnchorAndBackSlashWillReturnCleanedUri( )
	{
		$expected		=  'http://meneame.net';
		$cleaned_uri	= $this->news_model->cleanUri( $expected . '/#my_anchor           ' );

		$this->assertEquals( $expected, $cleaned_uri, 'A backslashed uri must return the uri without backslash.' );
	}
	
	
	/**
	 * Test getFrontPageNews method
	 */
	public function testGetFrontPageNewsReturn2( )
	{

		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenego berria da',
													'title_slug' => 'Hau-lehengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							),
							'mnm_news_vote' => array(
												array( 'user_id' => 1,
														'news_id' => 1 ),
												array( 'user_id' => 2,
														'news_id' => 1 ),
												array( 'user_id' => 3,
														'news_id' => 1 ),
												array( 'user_id' => 4,
														'news_id' => 1 ),
												array( 'user_id' => 1,
														'news_id' => 2 ),
												array( 'user_id' => 2,
														'news_id' => 2 ),
												array( 'user_id' => 3,
														'news_id' => 2 ),
												array( 'user_id' => 1,
														'news_id' => 3 ),
												array( 'user_id' => 2,
														'news_id' => 3 ),
												array( 'user_id' => 1,
														'news_id' => 4 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getFrontPageNews( 1, 2, '', 3 );
		$this->assertEquals( 2, count( $result ) );
	}


	/**
	 * Test getPendingNews method
	 */
	public function testGetPendingNewsReturn1( )
	{

		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenego berria da',
													'title_slug' => 'Hau-lehengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'Laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							),
							'mnm_news_vote' => array(
												array( 'user_id' => 1,
														'news_id' => 1 ),
												array( 'user_id' => 2,
														'news_id' => 1 ),
												array( 'user_id' => 3,
														'news_id' => 1 ),
												array( 'user_id' => 4,
														'news_id' => 1 ),
												array( 'user_id' => 1,
														'news_id' => 2 ),
												array( 'user_id' => 2,
														'news_id' => 2 ),
												array( 'user_id' => 3,
														'news_id' => 2 ),
												array( 'user_id' => 1,
														'news_id' => 3 ),
												array( 'user_id' => 2,
														'news_id' => 3 ),
												array( 'user_id' => 1,
														'news_id' => 4 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getPendingNews( 1, 2, '', 2 );
		$this->assertEquals( 1, count( $result ) );
	}


	/**
	 * Test getPopularNews method
	 */
	public function testGetPopularNewsReturn3( )
	{

		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenego berria da',
													'title_slug' => 'Hau-lehengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							),
							'mnm_news_vote' => array(
												array( 'user_id' => 1,
														'news_id' => 1 ),
												array( 'user_id' => 2,
														'news_id' => 1 ),
												array( 'user_id' => 3,
														'news_id' => 1 ),
												array( 'user_id' => 4,
														'news_id' => 1 ),
												array( 'user_id' => 1,
														'news_id' => 2 ),
												array( 'user_id' => 2,
														'news_id' => 2 ),
												array( 'user_id' => 3,
														'news_id' => 2 ),
												array( 'user_id' => 1,
														'news_id' => 3 ),
												array( 'user_id' => 2,
														'news_id' => 3 ),
												array( 'user_id' => 1,
														'news_id' => 4 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getPopularNews( 1, 2, '', 2 );
		$this->assertEquals( 2, count( $result ) );
	}


	/**
	 * Test getPopularNews method
	 */
	public function testGetUserNewsReturn2( )
	{

		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenego berria da',
													'title_slug' => 'Hau-lehengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							),
							'mnm_news_vote' => array(
												array( 'user_id' => 1,
														'news_id' => 1 ),
												array( 'user_id' => 2,
														'news_id' => 1 ),
												array( 'user_id' => 3,
														'news_id' => 1 ),
												array( 'user_id' => 4,
														'news_id' => 1 ),
												array( 'user_id' => 1,
														'news_id' => 2 ),
												array( 'user_id' => 2,
														'news_id' => 2 ),
												array( 'user_id' => 3,
														'news_id' => 2 ),
												array( 'user_id' => 1,
														'news_id' => 3 ),
												array( 'user_id' => 2,
														'news_id' => 3 ),
												array( 'user_id' => 1,
														'news_id' => 4 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getUserNews( 1, 2, '', 'ion' );
		$this->assertEquals( 2, count( $result ) );
	}


	/**
	 * Test getTotalUserNews method
	 */
	public function testGetTotalUserNewsReturn2( )
	{

		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenego berria da',
													'title_slug' => 'Hau-lehengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							),
							'mnm_news_vote' => array(
												array( 'user_id' => 1,
														'news_id' => 1 ),
												array( 'user_id' => 2,
														'news_id' => 1 ),
												array( 'user_id' => 3,
														'news_id' => 1 ),
												array( 'user_id' => 4,
														'news_id' => 1 ),
												array( 'user_id' => 1,
														'news_id' => 2 ),
												array( 'user_id' => 2,
														'news_id' => 2 ),
												array( 'user_id' => 3,
														'news_id' => 2 ),
												array( 'user_id' => 1,
														'news_id' => 3 ),
												array( 'user_id' => 2,
														'news_id' => 3 ),
												array( 'user_id' => 1,
														'news_id' => 4 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getTotalUserNews( 'ion' );
		$this->assertEquals( 2, $result );
	}



	/**
	 * Test getTotalFrontPageNews method
	 */
	public function testGetTotalFrontPageNewsReturn1( )
	{

		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenego berria da',
													'title_slug' => 'Hau-lehengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							),
							'mnm_news_vote' => array(
												array( 'user_id' => 1,
														'news_id' => 1 ),
												array( 'user_id' => 2,
														'news_id' => 1 ),
												array( 'user_id' => 3,
														'news_id' => 1 ),
												array( 'user_id' => 4,
														'news_id' => 1 ),
												array( 'user_id' => 1,
														'news_id' => 2 ),
												array( 'user_id' => 2,
														'news_id' => 2 ),
												array( 'user_id' => 3,
														'news_id' => 2 ),
												array( 'user_id' => 1,
														'news_id' => 3 ),
												array( 'user_id' => 2,
														'news_id' => 3 ),
												array( 'user_id' => 1,
														'news_id' => 4 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getTotalFrontPageNews( 3 );
		$this->assertEquals( 2, $result );
	}



	/**
	 * Test getTotalPopularNews method
	 */
	public function testGetTotalPopularNewsReturn2( )
	{

		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenego berria da',
													'title_slug' => 'Hau-lehengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							),
							'mnm_news_vote' => array(
												array( 'user_id' => 1,
														'news_id' => 1 ),
												array( 'user_id' => 2,
														'news_id' => 1 ),
												array( 'user_id' => 3,
														'news_id' => 1 ),
												array( 'user_id' => 4,
														'news_id' => 1 ),
												array( 'user_id' => 1,
														'news_id' => 2 ),
												array( 'user_id' => 2,
														'news_id' => 2 ),
												array( 'user_id' => 3,
														'news_id' => 2 ),
												array( 'user_id' => 1,
														'news_id' => 3 ),
												array( 'user_id' => 2,
														'news_id' => 3 ),
												array( 'user_id' => 1,
														'news_id' => 4 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getTotalPopularNews( 2 );
		$this->assertEquals( 3, $result );
	}


	/**
	 * Test getTotalPendingNews method
	 */
	public function testGetTotalPendingNews( )
	{

		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenego berria da',
													'title_slug' => 'Hau-lehengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							),
							'mnm_news_vote' => array(
												array( 'user_id' => 1,
														'news_id' => 1 ),
												array( 'user_id' => 2,
														'news_id' => 1 ),
												array( 'user_id' => 3,
														'news_id' => 1 ),
												array( 'user_id' => 4,
														'news_id' => 1 ),
												array( 'user_id' => 1,
														'news_id' => 2 ),
												array( 'user_id' => 2,
														'news_id' => 2 ),
												array( 'user_id' => 3,
														'news_id' => 2 ),
												array( 'user_id' => 1,
														'news_id' => 3 ),
												array( 'user_id' => 2,
														'news_id' => 3 ),
												array( 'user_id' => 1,
														'news_id' => 4 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getTotalPendingNews( 2 );
		$this->assertEquals( 1, $result );
	}

	/**
	 * Test getVotesNews with no news, must return empty array
	 */
	public function testGetVotedNewsWithEmptyArrayReturnEmptyArray( )
	{
		$data = array(
					'mnm_news_vote' => array( )
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$news_id = array( );
		$result = $this->news_model->getVotedNews( '1', $news_id );

		$this->assertEmpty( $result, 'If no given news id, must return empty array' );
	}

	/**
	 * Test getVotesNews with one not voted news, must return an empty array
	 */
	public function testGetVotedNewsWithOneNewsNotVotedIdReturnEmptyArray( )
	{
		$data = array(
					'mnm_news_vote' => array(
											array(
												'user_id' => 1,
												'news_id' => 1
											)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$news_id = array( '23' );
		$result = $this->news_model->getVotedNews( '1', $news_id );

		$this->assertEmpty( $result, 'If no given news id, must return empty array' );
	}

	/**
	 * Test getVotedNews with one voted news, must return an array with one
	 * position indicating that these news was voted
	 */
	public function testGetVotedNewsWithOneNewsVotedIdReturnArrayWithNewsVoted( )
	{
		$data = array(
					'mnm_news_vote' => array(
											array(
												'user_id' => 1,
												'news_id' => 1
											)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$news_id = array( '1' );
		$result = $this->news_model->getVotedNews( '1', $news_id );

		$this->assertNotEmpty( $result, 'News was voted, must return a non empty array' );
	}

	/**
	 * Test getVotedNews with one voted news, must return an array with one
	 * position indicating that these news was voted
	 */
	public function testGetVotedNewsWithOneNewsVotedIdReturnArrayWithNewsVoted_v2( )
	{
		$data = array(
					'mnm_news_vote' => array(
											array(
												'user_id' => 1,
												'news_id' => 1
											),
											array(
												'user_id' => 1,
												'news_id' => 2
											)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$news_id = array( '2' );
		$result = $this->news_model->getVotedNews( '1', $news_id );

		$this->assertNotEmpty( $result, 'News was voted, must return a non empty array' );
	}

	/**
	 * Test isRegisteredTitleSlug with empty title must return false
	 */
	public function testIsRegisteredTitleSlugWithEmptyTitleReturnFalse( )
	{
		$data = array(
					'mnm_news' => array( )
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$is_registered = $this->news_model->isRegisteredTitleSlug( '' );

		$this->assertFalse( $is_registered, 'Empty Title must return false' );
	}

	/**
	 * Test isRegisteredTitleSlug with title unregistered must return false
	 */
	public function testIsRegisteredTitleSlugWithTitleUnregisteredReturnFalse( )
	{
		$data = array(
					'mnm_news' => array( )
		);
		
		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$is_registered = $this->news_model->isRegisteredTitleSlug( 'papa-pitufo' );

		$this->assertFalse( $is_registered, 'Unregistered title must return false' );
	}

	/**
	 * Test isRegisteredTitleSlug with registered title must return true
	 */
	public function testIsRegisteredTitleSlugWithRegisteredTitleReturnTrue( )
	{
		$data = array(
					'mnm_news' => array(
									array(
										'news_id' => 1,
										'user_id' => 1,
										'title' => 'Hau lehenego berria da',
										'title_slug' => 'Hau-lehengo-berria-da',
										'summary' => 'Lehenengo berriaren laburpena da.',
										'url' => 'http://berria1.com',
										'votes' => 4
									)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );
		
		$is_registered = $this->news_model->isRegisteredTitleSlug( 'Hau-lehengo-berria-da' );

		$this->assertTrue( $is_registered, 'Registered Title must return true' );
	}

	/**
	 * Test searchNews method.
	 */
	public function testSearchNewsWithLehengoReturn1( )
	{
		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenego berria da',
													'title_slug' => 'Hau-lehengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->searchNews( 1, 2,'lehenego' );
		$this->assertEquals( 1, count( $result ) );
	}

	/**
	 * Test searchNews method  al realizar una búsqueda.
	 */
	public function testSearchNewsWithProbaReturn0( )
	{
		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenego berria da',
													'title_slug' => 'Hau-lehengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->searchNews( 1, 2,'proba' );
		$this->assertEquals( 0, count( $result ) );
	}

	/**
	 * Test getFrontPageNews method  al realizar una búsqueda.
	 */
	public function testGetFrontPageNewsWithTextLehenengoReturn1( )
	{

		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenego berria da',
													'title_slug' => 'Hau-lehengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							),
							'mnm_news_vote' => array(
												array( 'user_id' => 1,
														'news_id' => 1 ),
												array( 'user_id' => 2,
														'news_id' => 1 ),
												array( 'user_id' => 3,
														'news_id' => 1 ),
												array( 'user_id' => 4,
														'news_id' => 1 ),
												array( 'user_id' => 1,
														'news_id' => 2 ),
												array( 'user_id' => 2,
														'news_id' => 2 ),
												array( 'user_id' => 3,
														'news_id' => 2 ),
												array( 'user_id' => 1,
														'news_id' => 3 ),
												array( 'user_id' => 2,
														'news_id' => 3 ),
												array( 'user_id' => 1,
														'news_id' => 4 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getFrontPageNews( 1, 2, 'lehenengo', 3 );
		$this->assertEquals( 1, count( $result ), 'Esta consulta debería de haber devuelto 1 resultado.' );
	}


	/**
	 * Test getPendingNews method  al realizar una búsqueda.
	 */
	public function testGetPendingNewsWithTextLaugarrenReturn1( )
	{

		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenego berria da',
													'title_slug' => 'Hau-lehengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'Laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							),
							'mnm_news_vote' => array(
												array( 'user_id' => 1,
														'news_id' => 1 ),
												array( 'user_id' => 2,
														'news_id' => 1 ),
												array( 'user_id' => 3,
														'news_id' => 1 ),
												array( 'user_id' => 4,
														'news_id' => 1 ),
												array( 'user_id' => 1,
														'news_id' => 2 ),
												array( 'user_id' => 2,
														'news_id' => 2 ),
												array( 'user_id' => 3,
														'news_id' => 2 ),
												array( 'user_id' => 1,
														'news_id' => 3 ),
												array( 'user_id' => 2,
														'news_id' => 3 ),
												array( 'user_id' => 1,
														'news_id' => 4 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getPendingNews( 1, 2, 'laugarren', 2 );
		$this->assertEquals( 1, count( $result ), 'Esta consulta debería de haber devuelto 1 resultado.' );
	}


	/**
	 * Test getPopularNews method  al realizar una búsqueda.
	 */
	public function testGetPopularNewsWithTextLehenengoReturn1( )
	{

		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenego berria da',
													'title_slug' => 'Hau-lehengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							),
							'mnm_news_vote' => array(
												array( 'user_id' => 1,
														'news_id' => 1 ),
												array( 'user_id' => 2,
														'news_id' => 1 ),
												array( 'user_id' => 3,
														'news_id' => 1 ),
												array( 'user_id' => 4,
														'news_id' => 1 ),
												array( 'user_id' => 1,
														'news_id' => 2 ),
												array( 'user_id' => 2,
														'news_id' => 2 ),
												array( 'user_id' => 3,
														'news_id' => 2 ),
												array( 'user_id' => 1,
														'news_id' => 3 ),
												array( 'user_id' => 2,
														'news_id' => 3 ),
												array( 'user_id' => 1,
														'news_id' => 4 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getPopularNews( 1, 2, 'lehenengo', 2 );
		$this->assertEquals( 1, count( $result ), 'Esta consulta debería de haber devuelto un resultado.' );
	}


	/**
	 * Test getPopularNews method  al realizar una búsqueda.
	 */
	public function testGetUserNewsWithTextBigarrenUsernameIonReturn1( )
	{

		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenego berria da',
													'title_slug' => 'Hau-lehengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							),
							'mnm_news_vote' => array(
												array( 'user_id' => 1,
														'news_id' => 1 ),
												array( 'user_id' => 2,
														'news_id' => 1 ),
												array( 'user_id' => 3,
														'news_id' => 1 ),
												array( 'user_id' => 4,
														'news_id' => 1 ),
												array( 'user_id' => 1,
														'news_id' => 2 ),
												array( 'user_id' => 2,
														'news_id' => 2 ),
												array( 'user_id' => 3,
														'news_id' => 2 ),
												array( 'user_id' => 1,
														'news_id' => 3 ),
												array( 'user_id' => 2,
														'news_id' => 3 ),
												array( 'user_id' => 1,
														'news_id' => 4 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getUserNews( 1, 2, 'bigarren', 'ion' );
		$this->assertEquals( 1, count( $result ), 'Esta consulta debería de haber devuelto 1 resultado.' );
	}

	/**
	 * Test getPopularNews method al realizar una búsqueda.
	 */
	public function testGetUserNewsWithTextProbaUsernameIonReturn1( )
	{

		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenego berria da',
													'title_slug' => 'Hau-lehengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							),
							'mnm_news_vote' => array(
												array( 'user_id' => 1,
														'news_id' => 1 ),
												array( 'user_id' => 2,
														'news_id' => 1 ),
												array( 'user_id' => 3,
														'news_id' => 1 ),
												array( 'user_id' => 4,
														'news_id' => 1 ),
												array( 'user_id' => 1,
														'news_id' => 2 ),
												array( 'user_id' => 2,
														'news_id' => 2 ),
												array( 'user_id' => 3,
														'news_id' => 2 ),
												array( 'user_id' => 1,
														'news_id' => 3 ),
												array( 'user_id' => 2,
														'news_id' => 3 ),
												array( 'user_id' => 1,
														'news_id' => 4 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getUserNews( 1, 2, 'proba', 'ion' );
		$this->assertEquals( 0, count( $result ), 'Esta consulta debería de haber devuelto 0 resultados.' );
	}

		/**
	 * Test getTotalUserNews method al realizar una búsqueda.
	 */
	public function testGetTotalUserNewsWithTextLehenegoUsernameIonReturn1( )
	{

		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenego berria da',
													'title_slug' => 'Hau-lehengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							),
							'mnm_news_vote' => array(
												array( 'user_id' => 1,
														'news_id' => 1 ),
												array( 'user_id' => 2,
														'news_id' => 1 ),
												array( 'user_id' => 3,
														'news_id' => 1 ),
												array( 'user_id' => 4,
														'news_id' => 1 ),
												array( 'user_id' => 1,
														'news_id' => 2 ),
												array( 'user_id' => 2,
														'news_id' => 2 ),
												array( 'user_id' => 3,
														'news_id' => 2 ),
												array( 'user_id' => 1,
														'news_id' => 3 ),
												array( 'user_id' => 2,
														'news_id' => 3 ),
												array( 'user_id' => 1,
														'news_id' => 4 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getTotalUserNews( 'ion', 'lehenengo' );
		$this->assertEquals( 1, $result );
	}



	/**
	 * Test getTotalFrontPageNews method al realizar una búsqueda.
	 */
	public function testGetTotalFrontPageNewsWithTextLehenengoReturn1( )
	{

		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenengo berria da',
													'title_slug' => 'Hau-lehenengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							),
							'mnm_news_vote' => array(
												array( 'user_id' => 1,
														'news_id' => 1 ),
												array( 'user_id' => 2,
														'news_id' => 1 ),
												array( 'user_id' => 3,
														'news_id' => 1 ),
												array( 'user_id' => 4,
														'news_id' => 1 ),
												array( 'user_id' => 1,
														'news_id' => 2 ),
												array( 'user_id' => 2,
														'news_id' => 2 ),
												array( 'user_id' => 3,
														'news_id' => 2 ),
												array( 'user_id' => 1,
														'news_id' => 3 ),
												array( 'user_id' => 2,
														'news_id' => 3 ),
												array( 'user_id' => 1,
														'news_id' => 4 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getTotalFrontPageNews( 3, 'lehenengo' );
		$this->assertEquals( 1, $result );
	}



	/**
	 * Test getTotalPopularNews method al realizar una búsqueda.
	 */
	public function testGetTotalPopularNewsWithTextBigarrenReturn1( )
	{

		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenego berria da',
													'title_slug' => 'Hau-lehengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							),
							'mnm_news_vote' => array(
												array( 'user_id' => 1,
														'news_id' => 1 ),
												array( 'user_id' => 2,
														'news_id' => 1 ),
												array( 'user_id' => 3,
														'news_id' => 1 ),
												array( 'user_id' => 4,
														'news_id' => 1 ),
												array( 'user_id' => 1,
														'news_id' => 2 ),
												array( 'user_id' => 2,
														'news_id' => 2 ),
												array( 'user_id' => 3,
														'news_id' => 2 ),
												array( 'user_id' => 1,
														'news_id' => 3 ),
												array( 'user_id' => 2,
														'news_id' => 3 ),
												array( 'user_id' => 1,
														'news_id' => 4 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getTotalPopularNews( 2, 'bigarren' );
		$this->assertEquals( 1, $result );
	}


	/**
	 * Test getTotalPendingNews method al realizar una búsqueda.
	 */
	public function testGetTotalPendingNewsWithTextLaugarrenReturn1( )
	{

		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenego berria da',
													'title_slug' => 'Hau-lehengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							),
							'mnm_news_vote' => array(
												array( 'user_id' => 1,
														'news_id' => 1 ),
												array( 'user_id' => 2,
														'news_id' => 1 ),
												array( 'user_id' => 3,
														'news_id' => 1 ),
												array( 'user_id' => 4,
														'news_id' => 1 ),
												array( 'user_id' => 1,
														'news_id' => 2 ),
												array( 'user_id' => 2,
														'news_id' => 2 ),
												array( 'user_id' => 3,
														'news_id' => 2 ),
												array( 'user_id' => 1,
														'news_id' => 3 ),
												array( 'user_id' => 2,
														'news_id' => 3 ),
												array( 'user_id' => 1,
														'news_id' => 4 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getTotalPendingNews( 2, 'laugarren' );
		$this->assertEquals( 1, $result );
	}

	/**
	 * Test getTotalNewsByText method al realizar una búsqueda.
	 */
	public function testgetTotalNewsByTextWithTextLehenengoReturn1( )
	{

		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 2,
													'username' => 'salva',
													'password' => 'Abc12345',
													'email' => 'salva@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 3,
													'username' => 'jose',
													'password' => 'Abc12345',
													'email' => 'jose@meneame.internship',
													'status' => 'ACTIVE'),
											array( 'user_id' => 4,
													'username' => 'natalio',
													'password' => 'Abc12345',
													'email' => 'natalio@meneame.internship',
													'status' => 'ACTIVE')
							),
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'Hau lehenengo berria da',
													'title_slug' => 'Hau-lehenengo-berria-da',
													'summary' => 'Lehenengo berriaren laburpena da.',
													'url' => 'http://berria1.com',
													'votes' => 4 ),
											array( 'news_id' => 2,
													'user_id' => 1,
													'title' => 'Hau bigarren berria da',
													'title_slug' => 'Hau-bigarren-berria-da',
													'summary' => 'Bigarren berriaren laburpena da.',
													'url' => 'http://berria2.com',
													'votes' => 3 ),
											array( 'news_id' => 3,
													'user_id' => 2,
													'title' => 'Hau hirugarren berria da',
													'title_slug' => 'Hau-hirugarren-berria-da',
													'summary' => 'Hirugarren berriaren laburpena da.',
													'url' => 'http://berria3.com',
													'votes' => 2 ),
											array( 'news_id' => 4,
													'user_id' => 3,
													'title' => 'Hau laugarren berria da',
													'title_slug' => 'Hau-laugarren-berria-da',
													'summary' => 'laugarren berriaren laburpena da.',
													'url' => 'http://berria4.com',
													'votes' => 1 )
							),
							'mnm_news_vote' => array(
												array( 'user_id' => 1,
														'news_id' => 1 ),
												array( 'user_id' => 2,
														'news_id' => 1 ),
												array( 'user_id' => 3,
														'news_id' => 1 ),
												array( 'user_id' => 4,
														'news_id' => 1 ),
												array( 'user_id' => 1,
														'news_id' => 2 ),
												array( 'user_id' => 2,
														'news_id' => 2 ),
												array( 'user_id' => 3,
														'news_id' => 2 ),
												array( 'user_id' => 1,
														'news_id' => 3 ),
												array( 'user_id' => 2,
														'news_id' => 3 ),
												array( 'user_id' => 1,
														'news_id' => 4 )
							)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->news_model->getTotalNewsByText( 'lehenengo' );
		$this->assertEquals( 1, $result );
	}


}

?>
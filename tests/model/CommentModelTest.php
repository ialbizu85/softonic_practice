<?php
require_once 'tests/lib/Model_Framework_TestCase.php';
require_once 'app/model/comment.model.php';

/**
 * CommentModelTest.php tester class
 * 
 * @author meneame group
 */

/**
 * comment model tester class
 */
class CommentModelTest extends Model_Framework_TestCase
{
	/**
	 * CommentModel instance
	 * @var CommentModel
	 */
	private $comment_model;
	

	/**
	 * Create CommentModel instance
	 */
	public function  setUp( )
	{
		$this->comment_model = new CommentModel( );
	} 
	
	
	/**
	 * Test insertComment method
	 */
	public function testInsertCommentTrue( )
	{
		
		$data		= array( 'mnm_user' => array(
											array( 'user_id' => 1,
													'username' => 'ion',
													'password' => 'Abc12345',
													'email' => 'ion@meneame.internship',
													'status' => 'ACTIVE') ), 
							'mnm_news' => array(
											array( 'news_id' => 1,
													'user_id' => 1,
													'title' => 'proba bat da',
													'title' => 'proba-bat-da',
													'summary' => 'Hau proba bat da.',
													'url' => 'www.proba.com',
													'create_at' => time( )
												) ), 
							'mnm_news_comment' => array( ) );
		
		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );
		
		$result = $this->comment_model->insertcomment( 1, 'oso ondo', 1 );
		$this->assertTrue( $result );
	}
	
	
	/**
	 * Test getNewsComments method
	 */
	public function testGetNewsCommentsReturn1( )
	{
		
		$data		= array( 'mnm_user' => array(
											array(
												'user_id' => 1,
												'username' => 'ion',
												'password' => 'Abc12345',
												'email' => 'ion@meneame.internship',
												'status' => 'ACTIVE') ), 
							'mnm_news' => array(
											array(
												'news_id' => 1,
												'user_id' => 1,
												'title' => 'proba bat da',
												'title' => 'proba-bat-da',
												'summary' => 'Hau proba bat da.',
												'url' => 'www.proba.com',
												'create_at' => time( )
												) ), 
							'mnm_news_comment' => array(
												array(
													'comment_id' => 1,
													'news_id' => 1,
													'author' => 1,
													'create_at' => time( ),
													'content' => 'hau primeran dago'
												) ),
							'mnm_news_vote' => array( ) );
		
		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );
		
		$result = $this->comment_model->getNewsComments( 1, 1, 2 );
		$this->assertGreaterThan( 0, count( $result ) );
	}


	/**
	 * Test getNewsComments method
	 */
	public function testGetNewsCommentsReturn0( )
	{

		$data		= array( 'mnm_user' => array(
											array(
												'user_id' => 1,
												'username' => 'ion',
												'password' => 'Abc12345',
												'email' => 'ion@meneame.internship',
												'status' => 'ACTIVE') ),
							'mnm_news' => array(
											array(
												'news_id' => 1,
												'user_id' => 1,
												'title' => 'proba bat da',
												'title' => 'proba-bat-da',
												'summary' => 'Hau proba bat da.',
												'url' => 'www.proba.com',
												'create_at' => time( )
												) ),
							'mnm_news_comment' => array(
												array(
													'comment_id' => 1,
													'news_id' => 1,
													'author' => 1,
													'create_at' => time( ),
													'content' => 'hau primeran dago'
												) ),
							'mnm_news_vote' => array( ) );

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->comment_model->getNewsComments( 0, 1, 2 );
		$this->assertEquals( 0, count( $result ) );
	}
	
	
	/**
	 * Test getNewsComments method
	 */
	public function testGetNewsCommentsReturn2( )
	{

		$data		= array( 'mnm_user' => array(
											array(
												'user_id' => 1,
												'username' => 'ion',
												'password' => 'Abc12345',
												'email' => 'ion@meneame.internship',
												'status' => 'ACTIVE'),
											array(
												'user_id' => 2,
												'username' => 'natalio',
												'password' => 'Abc12345',
												'email' => 'natalio@meneame.internship',
												'status' => 'ACTIVE') ),
							'mnm_news' => array(
											array(
												'news_id' => 1,
												'user_id' => 1,
												'title' => 'proba bat da',
												'title' => 'proba-bat-da',
												'summary' => 'Hau proba bat da.',
												'url' => 'www.proba.com',
												'create_at' => time( )
												) ),
							'mnm_news_comment' => array(
												array(
													'comment_id' => 1,
													'news_id' => 1,
													'author' => 1,
													'create_at' => time( ),
													'content' => 'hau primeran dago'
												),
												array(
													'comment_id' => 2,
													'news_id' => 1,
													'author' => 1,
													'create_at' => time( ),
													'content' => 'Beste iritzi bat da.'
												),
												array(
													'comment_id' => 3,
													'news_id' => 1,
													'author' => 2,
													'create_at' => time( ),
													'content' => '3. garren iritzi bat.'
												) ),
							'mnm_news_vote' => array( ) );

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->comment_model->getNewsComments( 1, 1, 2 );
		$this->assertEquals( 2, count( $result ) );
	}
	
	
	
	/**
	 * Test getTotalNewsComments method
	 */
	public function testGetTotalNewsCommentsReturn3( )
	{

		$data		= array( 'mnm_user' => array(
											array(
												'user_id' => 1,
												'username' => 'ion',
												'password' => 'Abc12345',
												'email' => 'ion@meneame.internship',
												'status' => 'ACTIVE'),
											array(
												'user_id' => 2,
												'username' => 'natalio',
												'password' => 'Abc12345',
												'email' => 'natalio@meneame.internship',
												'status' => 'ACTIVE') ),
							'mnm_news' => array(
											array(
												'news_id' => 1,
												'user_id' => 1,
												'title' => 'proba bat da',
												'title' => 'proba-bat-da',
												'summary' => 'Hau proba bat da.',
												'url' => 'www.proba.com',
												'create_at' => time( )
												) ),
							'mnm_news_comment' => array(
												array(
													'comment_id' => 1,
													'news_id' => 1,
													'author' => 1,
													'create_at' => time( ),
													'content' => 'hau primeran dago'
												),
												array(
													'comment_id' => 2,
													'news_id' => 1,
													'author' => 1,
													'create_at' => time( ),
													'content' => 'Beste iritzi bat da.'
												),
												array(
													'comment_id' => 3,
													'news_id' => 1,
													'author' => 2,
													'create_at' => time( ),
													'content' => '3. garren iritzi bat.'
												) ),
							'mnm_news_vote' => array( ) );

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->comment_model->getTotalNewsComments( 1 );
		$this->assertEquals( 3, $result );
	}
	
	
	/**
	 * Test getTotalNewsComments method
	 */
	public function testGetTotalNewsCommentsReturn0( )
	{

		$data		= array( 'mnm_user' => array(
											array(
												'user_id' => 1,
												'username' => 'ion',
												'password' => 'Abc12345',
												'email' => 'ion@meneame.internship',
												'status' => 'ACTIVE'),
											array(
												'user_id' => 2,
												'username' => 'natalio',
												'password' => 'Abc12345',
												'email' => 'natalio@meneame.internship',
												'status' => 'ACTIVE') ),
							'mnm_news' => array(
											array(
												'news_id' => 1,
												'user_id' => 1,
												'title' => 'proba bat da',
												'title' => 'proba-bat-da',
												'summary' => 'Hau proba bat da.',
												'url' => 'www.proba.com',
												'create_at' => time( )
												) ),
							'mnm_news_comment' => array(
												array(
													'comment_id' => 1,
													'news_id' => 1,
													'author' => 1,
													'create_at' => time( ),
													'content' => 'hau primeran dago'
												),
												array(
													'comment_id' => 2,
													'news_id' => 1,
													'author' => 1,
													'create_at' => time( ),
													'content' => 'Beste iritzi bat da.'
												),
												array(
													'comment_id' => 3,
													'news_id' => 1,
													'author' => 2,
													'create_at' => time( ),
													'content' => '3. garren iritzi bat.'
												) ),
							'mnm_news_vote' => array( ) );

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->comment_model->getTotalNewsComments( 2 );
		$this->assertEquals( 0, $result );
	}
}

?>
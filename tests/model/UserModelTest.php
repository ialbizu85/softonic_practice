<?php
require_once 'tests/lib/Model_Framework_TestCase.php';
require_once 'app/model/user.model.php';

/**
 * UserModelTest.php tester class
 *
 * @author meneame group
 */

/**
 * user model tester class
 */
class UserModelTest extends Model_Framework_TestCase
{
	/**
	 * UserModel instance
	 * @var UserModel
	 */
	protected $user_model;

	/**
	 * Create UserModel instance
	 */
	public function  setUp( )
	{
		$this->user_model = new UserModel( );
	}

	/**
	 * Test isUserRegistered method.
	 */
	public function testIsRegisteredExistentReturnTrue( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'salva',
											'password' => 'Abc12345',
											'email' => 'salva@meneame.internship',
											'status' => 'ACTIVE'
											)
					)
		);
		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->isRegistered( 'salva' );
		$this->assertTrue( $result, 'Checking if existent user is registered expects return True' );
	}

	/**
	 * Test isUserRegistered method.
	 */
	public function testIsRegisteredInexistentReturnFalse( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'salva',
											'password' => 'Abc12345',
											'email' => 'salva@meneame.internship',
											'status' => 'ACTIVE'
											)
					)
		);
		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->isRegistered( 'natalio' );
		$this->assertFalse( $result, 'Checking if non existent user is registered expects return False' );
	}

	/**
	 * Test isRegisteredMail method.
	 */
	public function testIsRegisteredMailExistentReturnTrue( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'salva',
											'password' => 'Abc12345',
											'email' => 'salva@meneame.internship',
											'status' => 'ACTIVE'
											)
					)
		);
		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->isRegisteredMail( 'salva@meneame.internship' );
		$this->assertTrue( $result, 'Checking if existent e-mail is registered expects return True' );
	}

	/**
	 * Test isRegisteredMail method.
	 */
	public function testIsRegisteredMailInexistentReturnFalse( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'salva',
											'password' => 'Abc12345',
											'email' => 'salva@meneame.internship',
											'status' => 'ACTIVE'
											)
					)
		);
		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->isRegisteredMail( 'nuevosalva@meneame.internship' );
		$this->assertFalse( $result, 'Checking if non existent e-mail is registered expects return False' );
	}

	/**
	 * Test setUser method.
	 */
	public function testSetUserReturnTrue( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'zarba',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										),
										 array(
											'user_id' => 2,
											'username' => 'Javier',
											'password' => 'RHl987654',
											'email' => 'javier@hotmail.com',
											'status' => 'ACTIVE'
										)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->setUser( 'natalio', 'abC123456', 'natalio@meneame.com' );
		$this->assertEquals( $result, 3, 'Set user operation expects return True' );
	}

	/**
	 * Test that a valid news_id return that it exits (true)
	 */
	public function testgetUserByCredentialUsernameTorcuatoReturnUserTorcuato( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'Torcuato',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										),
										 array(
											'user_id' => 2,
											'username' => 'Javier',
											'password' => 'RHl987654',
											'email' => 'javier@hotmail.com',
											'status' => 'ACTIVE'
										)
					)
		);

		$expected = array(	'user_id' => 1,
							'username' => 'Torcuato',
							'password' => 'Abc12345',
							'status' => 'ACTIVE' );

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->getUserByCredential( 'Torcuato' );
		$this->assertEquals( $result, $expected, 'Incorrect return for get user named Torcuato by credentials' );
	}

	/**
	 * Test that a invalid news_id return that it not exits (false)
	 */
	public function testgetUserByCredentialUsernameNonExistentReturnFalse( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'Torcuato',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										),
										 array(
											'user_id' => 2,
											'username' => 'Javier',
											'password' => 'RHl987654',
											'email' => 'javier@hotmail.com',
											'status' => 'ACTIVE'
										)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->getUserByCredential('Miguel');
		$this->assertFalse( $result, 'get user non existent by credentials expects to return False' );
	}

	/**
	 * Test setPasswd method.
	 */
	public function testSetPasswdReturnTrue( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'Torcuato',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										),
										 array(
											'user_id' => 2,
											'username' => 'Javier',
											'password' => 'RHl987654',
											'email' => 'javier@hotmail.com',
											'status' => 'ACTIVE'
										)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->setPasswd( 'abC23713', 1 );
		$this->assertTrue( $result, 'Set password method expects to return True' );
	}

	/**
	 * Test getPasswd method.
	 */
	public function testGetPasswdNonExistentReturnFalse( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'Torcuato',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										),
										 array(
											'user_id' => 2,
											'username' => 'Javier',
											'password' => 'RHl987654',
											'email' => 'javier@hotmail.com',
											'status' => 'ACTIVE'
										)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->getPasswd( 3 );
		$this->assertFalse( $result, 'Get password from non existent user expects to return False' );
	}

	/**
	 * Test getPasswd method.
	 */
	public function testGetPasswdExistentReturnString( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'Torcuato',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										),
										 array(
											'user_id' => 2,
											'username' => 'Javier',
											'password' => 'RHl987654',
											'email' => 'javier@hotmail.com',
											'status' => 'ACTIVE'
										)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->getPasswd( 1 );
		$this->assertEquals( $result, 'Abc12345', 'Get password from existent user expects to return string' );
	}

	/**
	 * Test getMail method.
	 */
	public function testGetMailNonExistentReturnFalse( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'Torcuato',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										),
										 array(
											'user_id' => 2,
											'username' => 'Javier',
											'password' => 'RHl987654',
											'email' => 'javier@hotmail.com',
											'status' => 'ACTIVE'
										)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->getMail( 3 );
		$this->assertFalse( $result, 'Get mail from non existent user expects to return False' );
	}

	/**
	 * Test getMail method.
	 */
	public function testGetMailExistentReturnFalse( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'Torcuato',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										),
										 array(
											'user_id' => 2,
											'username' => 'Javier',
											'password' => 'RHl987654',
											'email' => 'javier@hotmail.com',
											'status' => 'ACTIVE'
										)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->getMail( 1 );
		$this->assertEquals( $result, 'torcuato@hotmail.com', 'Get mail from existent user expects to return string' );
	}

	/**
	 * Test getProfileInfo method.
	 */
	public function testGetProfileInfoByUserNameNonExistentReturnFalse( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'Torcuato',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										),
										 array(
											'user_id' => 2,
											'username' => 'Javier',
											'password' => 'RHl987654',
											'email' => 'javier@hotmail.com',
											'status' => 'ACTIVE'
										)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->getProfileInfoByUserName( 'Natalio' );
		$this->assertFalse( $result, 'Get profile info from non existent user method expects to return false' );
	}

	/**
	 * Test getProfileInfo method.
	 */
	public function testGetProfileInfoByUserNameExistentReturnArray( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'Torcuato',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										),
										 array(
											'user_id' => 2,
											'username' => 'Javier',
											'password' => 'RHl987654',
											'email' => 'javier@hotmail.com',
											'status' => 'ACTIVE'
										)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->getProfileInfoByUserName( 'Torcuato' );
		$this->assertCount( 4, $result, 'Get Profile info from existent user expects to receive 4 fields' );
	}

	/**
	 * Test getUserId method.
	 */
	public function testGetUserIdNotExistentReturnFalse( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'Torcuato',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										),
										 array(
											'user_id' => 2,
											'username' => 'Javier',
											'password' => 'RHl987654',
											'email' => 'javier@hotmail.com',
											'status' => 'ACTIVE'
										)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->getUserId( 'pelonete' );
		$this->assertFalse( $result, 'Get user id from non existent user expects to return False' );
	}

	/**
	 * Test getUserId method.
	 */
	public function testGetUserIdExistentNamedTorcuatoReturn1( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'Torcuato',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										),
										 array(
											'user_id' => 2,
											'username' => 'Javier',
											'password' => 'RHl987654',
											'email' => 'javier@hotmail.com',
											'status' => 'ACTIVE'
										)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->getUserId( 'Torcuato' );
		$this->assertEquals( $result, 1, 'Get user id from user Torcuato expects to return 1' );
	}

	/**
	 * Test getUsername method.
	 */
	public function testGetUsernameByNonExistentIdReturnFalse( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'Torcuato',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										),
										 array(
											'user_id' => 2,
											'username' => 'Javier',
											'password' => 'RHl987654',
											'email' => 'javier@hotmail.com',
											'status' => 'ACTIVE'
										)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->getUsername( 3 );
		$this->assertFalse( $result, 'Get Username entering non existent id expects to return False' );
	}

	/**
	 * Test getUsername method.
	 */
	public function testGetUsernameById1ReturnTorcuato( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'Torcuato',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										),
										 array(
											'user_id' => 2,
											'username' => 'Javier',
											'password' => 'RHl987654',
											'email' => 'javier@hotmail.com',
											'status' => 'ACTIVE'
										)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->getUsername( 1 );
		$this->assertEquals( $result, 'Torcuato', 'Get Username entering id = 1 expects to return Torcuato');
	}

	/**
	 * Test setValidUserStatus method.
	 */
	public function testSetValidUserStatusReturnTrue( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'Torcuato',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'PENDING'
										),
										 array(
											'user_id' => 2,
											'username' => 'Javier',
											'password' => 'RHl987654',
											'email' => 'javier@hotmail.com',
											'status' => 'ACTIVE'
										)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->setValidUserStatus( 1 );
		$this->assertTrue( $result, 'SetValidUserStatus method expects to return True.' );
	}


	/**
	 * Test that a valid user_id return that it exits (true)
	 */
	public function testIsValidWithValidUserIdWillReturnTrue( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'ion',
											'password' => 'Abc12345',
											'email' => 'albixu_euskadi@hotmail.com',
											'status' => 'ACTIVE'
										)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->isValid( 1 );
		$this->assertTrue( $result );
	}

	/**
	 * Test that a invalid user_id return that it not exits (false)
	 */
	public function testIsValidWithInvalidUserIdWillReturnFalse( )
	{
		$data = array( 'mnm_user' => array( ) );

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->isValid( 1 );
		$this->assertFalse( $result );
	}

	/**
	 * Test unsubscribeUser method.
	 */
	public function testUnsubscribeUserExistentReturnTrue( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'ion',
											'password' => 'Abc12345',
											'email' => 'albixu_euskadi@hotmail.com',
											'status' => 'ACTIVE'
										)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->unsubscribeUser( 1 );

		$this->assertEquals( $result, true, 'Unsubscribe existent user expects to return true' );
	}

	/**
	 * Test unsubscribeUser method.
	 */
	public function testUnsubscribeUserNonExistentReturnFalse( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'ion',
											'password' => 'Abc12345',
											'email' => 'albixu_euskadi@hotmail.com',
											'status' => 'ACTIVE'
										)
					)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->unsubscribeUser( 3 );

		$this->assertEquals( $result, false, 'Unsubscribe non existent user expects to return false' );
	}

	/**
	* Test getUsername method.
	*/
	public function testGetUsernameNonExistentReturnFalse( )
	{
		$data = array( 'mnm_user' => array( ) );

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->getUsername( 3 );
		$this->assertFalse( $result, 'Get username from non existent user expects to return False' );
	}

	/**
	* Test getUsername method.
	*/
	public function testGetUsernameExistentReturnUsername( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'Torcuato',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										),
										array(
											'user_id' => 2,
											'username' => 'Javier',
											'password' => 'RHl987654',
											'email' => 'javier@hotmail.com',
											'status' => 'ACTIVE'
										)
									)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->getUsername( 1 );
		$this->assertEquals( 'Torcuato', $result, 'Get username from existent user expects to return username' );
	}

	/**
	 * Test delete cookie login to an existent cookie login return true
	 */
	public function testDeleteCookieLoginWithExistentTokenReturnTrue( )
	{
		require CORE__CONFIG_DIR . '/rememberpass.config.php';
		$token = hash_hmac( 'sha1', 'my_token', $_seed );

		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'Torcuato',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										)
									),
						'mnm_login_cookie' => array(
										array(
											'user_id' => 1,
											'token' => $token
										)
									)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->deleteCookieLogin( 1, 'my_token' );
		$this->assertTrue( $result, 'Delete an existent cookie login return true' );
	}

	/**
	* Test delete cookie login to an inexistent cookie login return false
	*/
	public function testDeleteCookieLoginWithInexistentTokenReturnFalse( )
	{
		$data = array( 'mnm_user' => array(
											array(
												'user_id' => 1,
												'username' => 'Torcuato',
												'password' => 'Abc12345',
												'email' => 'torcuato@hotmail.com',
												'status' => 'ACTIVE'
											)
									),
						'mnm_login_cookie' => array( )
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->deleteCookieLogin( 1, 'my_token' );
		$this->assertFalse( $result, 'Delete an inexistent cookie login return false' );
	}

	/**
	* Test delete cookie login to an inexistent cookie and user_id return false
	*/
	public function testDeleteCookieLoginWithInexistentUserIdAndTokenReturnFalse( )
	{
		$data = array(
					'mnm_user' => array( ),
					'mnm_login_cookie' => array( )
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->deleteCookieLogin( 1, 'my_token' );
		$this->assertFalse( $result, 'Delete an inexistent cookie login return false' );
	}

	/**
	 * Test delete all cookie logins for a inexistent user id, must return 0
	 */
	public function testDeleteAllCookiesLoginWithInexistentUserIdWillReturn0( )
	{
		$data = array(
					'mnm_user' => array( ),
					'mnm_login_cookie' => array( )
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->deleteAllCookiesLogin( 1 );
		$this->assertEquals( 0, $result, 'Delete all cookies for inexistent user id return 0' );
	}

	/**
	* Test delete all cookie logins for an existent user id without cookie login,
	* must return 0
	*/
	public function testDeleteAllCookiesLoginWithExistentUserIdWithoutCookiesWillReturn0( )
	{
		$data = array( 'mnm_user' => array(
											array(
												'user_id' => 1,
												'username' => 'Torcuato',
												'password' => 'Abc12345',
												'email' => 'torcuato@hotmail.com',
												'status' => 'ACTIVE'
											)
									),
						'mnm_login_cookie' => array( )
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->deleteAllCookiesLogin( 1 );
		$this->assertEquals( 0, $result, 'Delete all cookies for existent user id without cookies return 0' );
	}

	/**
	* Test delete all cookie logins for an existent user id without cookie login,
	* must return number of created cookies (2)
	*/
	public function testDeleteAllCookiesLoginWithExistentUserIdWithTwoCookiesWillReturnTwo( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'Torcuato',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										)
						),
						'mnm_login_cookie' => array(
										array(
											'user_id' => 1,
											'token' => 'my_token'
										),
										array(
											'user_id' => 1,
											'token' => 'my_token_2'
										)
						)
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->deleteAllCookiesLogin( 1 );
		$this->assertEquals( 2, $result, 'Delete all cookies for existent user id with 2 cookies return 2' );
	}

	/**
	 * Test add a cookie login for a valid user
	 */
	public function testAddCookieLoginWithUserValidAndTokenReturnTrue( )
	{
		$data = array( 'mnm_user' => array(
										array(
											'user_id' => 1,
											'username' => 'Torcuato',
											'password' => 'Abc12345',
											'email' => 'torcuato@hotmail.com',
											'status' => 'ACTIVE'
										)
						),
						'mnm_login_cookie' => array( )
		);

		$this->setDatabaseConnection( 'admin' );
		$this->insertData( $data );

		$result = $this->user_model->addCookieLogin( 1, '2378643278466' );
		$this->assertTrue( $result, 'Add a cookie login to an existent user return true' );
	}
}

?>
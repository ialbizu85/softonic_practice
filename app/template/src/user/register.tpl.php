{% extends "common/layout.tpl.php" %}

{% block javascript %}
<script src="{{ constant( 'URL_JS' ) }}/lib/form.js" type="text/javascript"></script>
<script src="{{ constant( 'URL_JS' ) }}/user/register.js" type="text/javascript"></script>
{% endblock javascript %}

{% block title %}
	Registrar nueva cuenta
{% endblock title %}

{% block content %}
		{% if ( message ) %}
        <h3 class="message"> {{ message }} </h3>
        {% endif %}

		<form method="post" name="registerForm" action="{{ constant( 'URL_DOMAIN' )}}/registro" id="registerForm" >
                <p>
                        <label for="username" >Nombre de usuario:</label>
                        <input type="text" name="username" id="username" />
						{% if error_list.username %}
						<label for="username" class="error">{{ error_list.username }}</label>
						{% endif %}
                </p>
                <p>
                        <label for="password" >Contraseña:</label>
                        <input type="password" name="password" id="password" />
						{% if error_list.password %}
						<label for="password" class="error">{{ error_list.password }}</label>
						{% endif %}
                </p>
				<p>
                        <label for="password_rep" >Repite contraseña:</label>
                        <input type="password" name="password_rep" id="password_rep" />
                </p>
				<p>
                        <label for="email" >E-mail:</label>
                        <input type="email" name="email" id="email" />
						{% if error_list.email %}
						<label for="email" class="error">{{ error_list.email }}</label>
						{% endif %}
                </p>

                <input type="submit" name="register" id="register" value="Registro" />
        </form>
{% endblock content %}
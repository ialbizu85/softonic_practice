{% extends "/common/profilelayout.tpl.php" %}

{% block news_user %}
	<a href="{{ constant( 'URL_DOMAIN' ) }}/perfil/{{ username }}/noticias">Noticias del usuario</a>
{% endblock news_user %}

{% block title %}
	Información del usuario
{% endblock title %}

{% block info %}
{% if not error_message %}

<h2 class="title">Ficha de usuario</h2>
<ul class="info">
		<li> <span>Nombre de usuario :</span> {{ user_profile_info[ 'username' ]|capitalize }} </li>
		<li> <span>Noticias meneadas :</span> {{ user_profile_info[ 'votes' ] }} </li>
		<li> <span>Noticias publicadas :</span> {{ user_profile_info[ 'published_news' ] }} </li>
		<li> <span>Meneos recibidos :</span> {{ user_profile_info[ 'received_votes' ] }} </li>
	
</ul>

{% if self_user %}
<p><a href="{{ constant( 'URL_DOMAIN' ) }}/usuario/modificar">Modificar contraseña</a></p>
<p><a href="{{ constant( 'URL_DOMAIN' ) }}/usuario/confirmar-baja">Baja de usuario</a></p>
{% endif %}

{% else %}
<ul>
	<li>
		{{ error_message }}
	</li>
</ul>
{% endif %}

{% endblock info %}

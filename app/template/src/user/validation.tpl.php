{% extends "common/layout.tpl.php" %}

{% block title %}
	Usuario registrado correctamente
{% endblock title %}

{% block content %}
<p>Un correo ha sido enviado a {{ email }} para solicitar confirmación.
	Para completar tu registro debes seguir las instrucciones.</p>

{% endblock content %}
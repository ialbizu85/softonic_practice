{% extends "common/layout.tpl.php" %}

{% block title %}
	Confirmación de baja de usuario
{% endblock title %}

{% block content %}
		<h3> {{ message }} </h3>

		<p><a href="{{ constant( 'URL_DOMAIN' ) }}/usuario/baja">Si</a></p>
		<p><a href="{{ constant( 'URL_DOMAIN' ) }}">No</a></p>

{% endblock content %}
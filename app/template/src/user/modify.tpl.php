{% extends "/common/layout.tpl.php" %}

{% block javascript %}
<script src="{{ constant( 'URL_JS' ) }}/lib/form.js" type="text/javascript"></script>
<script src="{{ constant( 'URL_JS' ) }}/user/modify.js" type="text/javascript"></script>
{% endblock javascript %}

{% block title %}
	Modificar datos de usuario
{% endblock title %}

{% block content %}

{% if not is_passwd_changed %}

<h2>Formulario de cambio de contraseña</h2>

<form action="{{ constant( 'URL_DOMAIN' ) }}/usuario/modificar" name="modifyForm" id="modifyForm" method="post">

	<p>
		<label for="old_user_passwd">
			Password antiguo:
		</label>
		<input type="password" id="old_user_passwd" name="old_passwd" />
		{% if error_list.old_passwd %}
			<label for="old_user_passwd" class="error">{{ error_list.old_passwd }}</label>
		{% endif %}
	</p>
	<p>
		<label for="new_passwd">
			Nuevo Password:
		</label>
		<input type="password" id="new_user_passwd" name="new_passwd" />
		{% if error_list.new_passwd %}
			<label for="new_user_passwd" class="error">{{ error_list.new_passwd }}</label>
		{% endif %}
	</p>
	<p>
		<label for="rep_new_passwd">
			Repita Nuevo Password:
		</label>
		<input type="password" id="rep_new_passwd" name="rep_new_passwd" />
	</p>

	<input id="modify_passwd" type='submit' name='modify_passwd' value='Modificar Contraseña' />
</form>

<p>{{ message }}</p>

{% else %}

<p>{{ message }}</p>

<a href="{{ constant( 'URL_DOMAIN' ) }}" id="return">Volver</a>

{% endif %}

{% endblock content %}

{% extends "common/layout.tpl.php" %}

{% block title %}
	Cambiar contraseña de la cuenta
{% endblock title %}

{% block content %}
        <h3 class="message"> {{ message }} </h3>

		{% if not is_changed %}
		<form method="post" name="changePasswdForm" action="{{ constant( 'URL_DOMAIN' )}}/usuario/cambiar-contrasena" id="changePasswdForm" >
                <p>
                        <label for="password" >Nueva contraseña:</label>
                        <input type="password" name="password" id="password" />
                </p>
                <p>
                        <label for="password_rep" >Repite contraseña:</label>
                        <input type="password" name="password_rep" id="password_rep" />
                </p>
                <input type="submit" name="change_passwd" id="change_passwd" value="Cambiar contraseña" />
        </form>
		{%  endif %}

		<p><a href="{{ constant( 'URL_DOMAIN' ) }}">Volver a portada</a></p>

{% endblock content %}
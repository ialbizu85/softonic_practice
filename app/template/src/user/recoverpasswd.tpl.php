{% extends "common/layout.tpl.php" %}

{% block javascript %}
<script src="{{ constant( 'URL_JS' ) }}/lib/form.js" type="text/javascript"></script>
<script src="{{ constant( 'URL_JS' ) }}/user/recoverpasswd.js" type="text/javascript"></script>
{% endblock javascript %}

{% block title %}
	Recuperar contraseña de tu cuenta
{% endblock title %}

{% block content %}
        <h3 class="message"> {{ message }} </h3>

		{% if not is_sent %}
		<form method="post" name="recoverPasswdForm" action="{{ constant( 'URL_DOMAIN' )}}/recuperar-contrasena" id="recoverPasswdForm" >
                <p>
                        <label for="username" >Nombre de usuario:</label>
                        <input type="text" name="username" id="username" />
						{% if error_list.username %}
						<label for="username" class="error">{{ error_list.username }}</label>
						{% endif %}
                </p>
				<p>
                        <label for="email" >E-mail:</label>
                        <input type="email" name="email" id="email" />
						{% if error_list.email %}
						<label for="email" class="error">{{ error_list.email }}</label>
						{% endif %}
                </p>

                <input type="submit" name="submit" id="submit" value="Enviar" />
        </form>
		{% endif %}

		<p><a href="{{ constant( 'URL_DOMAIN' ) }}">Volver a portada</a></p>

{% endblock content %}
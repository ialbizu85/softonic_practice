{% extends "common/layout.tpl.php" %}

{% block title %}
	Baja de usuario
{% endblock title %}

{% block content %}
		<h3> {{ message }} </h3>

		<form method="post" name="unsubscribeForm" action="{{ constant( 'URL_DOMAIN' ) }}/usuario/baja" id="unsubscribeForm" >
                <p>
                        <label for="password" >Por favor, introduzca su contraseña para darse de baja:</label>
                        <input type="password" name="password" id="password" />
                </p>
                <input type="submit" name="unsubscribe_submit" id="unsubscribe_submit" value="Enviar" />
        </form>

		<p>
			<a href="{{ constant( 'URL_DOMAIN' ) }}">Volver a portada</a>
		</p>

{% endblock content %}
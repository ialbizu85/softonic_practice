{% extends "common/layout.tpl.php" %}

{% block title %}
	Acceso al sistema
{% endblock title %}

{% block content %}
        {% if message %}
        <h3 class="message"> {{ message }} </h3>
        {% endif %}
		<form method="post" name="loginForm" action="{{ constant( 'URL_DOMAIN' ) }}/login" id="loginForm" >
                <p>
                        <label for="identifier" >Nombre de usuario o e-mail:</label>
                        <input type="text" name="identifier" id="identifier" />
                </p>
                <p>
                        <label for="password" >Contraseña:</label>
                        <input type="password" name="password" id="password" />
                </p>
				<p>
					<input type="checkbox" name="remember" id="remember" value="1" />
					<label for="remember">Recordar login</label>
				</p>
                <input type="submit" name="login" id="login" value="Login" />
        </form>
        <a href="{{ constant( 'URL_DOMAIN' ) }}/recuperar-contrasena">Recuperar contraseña</a> |
        <a href="{{ constant( 'URL_DOMAIN' ) }}/registro">Registro</a>

		{% if is_pending %}
        <p><a href="{{ constant( 'URL_DOMAIN' ) }}/reenvio-validacion">
			Pincha aqui para volver a recibir el e-mail de validación de cuenta</a></p>
        {% endif %}

{% endblock content %}
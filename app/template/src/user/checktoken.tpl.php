{% extends "common/layout.tpl.php" %}

{% block title %}
	Error en el cambio de contraseña
{% endblock title %}

{% block content %}
		<h3> {{ message }} </h3>

		<p><a href="{{ constant( 'URL_DOMAIN' ) }}">Volver a portada</a></p>

{% endblock content %}
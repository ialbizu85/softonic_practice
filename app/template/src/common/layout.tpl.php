<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

		<link href="{{ constant( 'URL_CSS' ) }}/reset.css" rel="stylesheet" type="text/css" />
		<link href="{{ constant( 'URL_CSS' ) }}/layout.css" rel="stylesheet" type="text/css" />
		<link href="{{ constant( 'URL_CSS' ) }}/news.css" rel="stylesheet" type="text/css" />
		<link href="{{ constant( 'URL_CSS' ) }}/paginator.css" rel="stylesheet" type="text/css" />
		<link href="{{ constant( 'URL_CSS' ) }}/profile.css" rel="stylesheet" type="text/css" />


		<script src="{{ constant( 'URL_JS' ) }}/lib/jquery-1.7.1.min.js" type="text/javascript"></script>
		{% block javascript %}
		{% endblock javascript %}

		<title>
			{% block title %}Listado de noticias{% endblock title %} - Meneame.net
		</title>
	</head>
	<body>
		<header>
			{% if layout.is_logged %}
				Hola, <a href="{{ constant( 'URL_DOMAIN' ) }}/perfil/{{ layout.username }}">{{ layout.username }}</a>
				| <a href="{{ constant( 'URL_DOMAIN' ) }}/logout">Desconectar</a>
			{% else %}
				<a href="{{ constant( 'URL_DOMAIN' ) }}/login">Acceso</a> |
				<a href="{{ constant( 'URL_DOMAIN' ) }}/registro">Registro</a>
			{% endif %}
			| <a href="{{ constant( 'URL_DOMAIN' ) }}/search">Buscar</a>
		</header>
		<nav id="top_nav">
			<ul>
				<li>
					<a href="{{ constant( 'URL_DOMAIN' ) }}">Portada</a>
				</li>
				<li>
					<a href="{{ constant( 'URL_DOMAIN' ) }}/populares">Populares</a>
				</li>
				<li>
					<a href="{{ constant( 'URL_DOMAIN' ) }}/pendientes">Pendientes</a>
				</li>
				<li>
					<a href="{{ constant( 'URL_DOMAIN' ) }}/noticias/subir/url">Subir noticia</a>
				</li>
			</ul>
		</nav>
		<section id="content">
			{% block content %}
			{% endblock content %}
		</section>
		<footer>
			<a href="http://www.w3.org/html/logo/">
				<img src="http://www.w3.org/html/logo/downloads/HTML5_Badge_32.png" alt="HTML5 Powered with Semantics" title="HTML5 Powered with Semantics">
			</a>

			<a href="http://jigsaw.w3.org/css-validator/check/referer">
				<img style="border:0;width:88px;height:31px"
					src="http://jigsaw.w3.org/css-validator/images/vcss"
					alt="¡CSS Válido!" />
			</a>
		</footer>
	</body>
</html>
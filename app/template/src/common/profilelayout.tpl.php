{% extends "/common/layout.tpl.php" %}

{% block content %}
<section>
	<nav id="profile_nav">
	<ul>
		<li>
			{% block profile_user %}Perfil de usuario{% endblock profile_user %}
		</li>
		<li>
			{% block news_user %}Noticias del usuario{% endblock news_user %}
		</li>
	</ul>
	</nav>
	<section>
		{% block info %}
		{% endblock info %}
	</section>
</section>
{% endblock content %}
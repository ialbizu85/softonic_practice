{% macro make( paginator ) %}
	<div class="paginator">
		{# Link to before page #}
		{% if paginator.total_pages > 0 %}
			{% if paginator.current_page > 1 %}
				{% if paginator.current_page == 2 %}
					<a class="page" href="{{ constant( 'URL_DOMAIN' ) }}{{ paginator.href }}">Anterior</a>
				{% else %}
					<a class="page" href="{{ constant( 'URL_DOMAIN' ) }}{{ paginator.href }}/{{ paginator.current_page - 1 }}">Anterior</a>
				{% endif %}
			{% else %}
				<p class="page">Anterior</p>
			{% endif %}

			{# Link to first page if needed #}
			{% if paginator.initial_page > 1 %}
				<a class="page" href="{{ constant( 'URL_DOMAIN' ) }}{{ paginator.href }}/">1</a> ...
			{% endif %}

			{% for page in paginator.initial_page..paginator.final_page %}
				{% if page != paginator.current_page %}
					{% if 1 != page %}
						<a class="page" href="{{ constant( 'URL_DOMAIN' ) }}{{ paginator.href }}/{{ page }}">{{ page }}</a>
					{% else %}
						<a class="page" href="{{ constant( 'URL_DOMAIN' ) }}{{ paginator.href }}">{{ page }}</a>
					{% endif %}
				{% else %}
					<p class="page current">{{ page }}</p>
				{% endif %}
			{% endfor %}


			{# Link to last page if needed #}
			{% if paginator.final_page < paginator.total_pages %}
				... <a class="page" href="{{ constant( 'URL_DOMAIN' ) }}{{ paginator.href }}/{{ paginator.total_pages }}">{{ paginator.total_pages }}</a>
			{% endif %}

			{# Link to next page #}
			{% if paginator.current_page < paginator.total_pages %}
				<a class="page" href="{{ constant( 'URL_DOMAIN' ) }}{{ paginator.href }}/{{ paginator.current_page+1 }}">Siguiente</a>
			{% else %}
				<p class="page">Siguiente</p>
			{% endif %}
		{% endif %}
	</div>
{% endmacro %}
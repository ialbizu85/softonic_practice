{% extends "common/layout.tpl.php" %}

{% block javascript %}
<script src="{{ constant( 'URL_JS' ) }}/lib/form.js" type="text/javascript"></script>
<script src="{{ constant( 'URL_JS' ) }}/news/url.js" type="text/javascript"></script>
{% endblock javascript %}

{% block title %}
	Validar dirección url
{% endblock title %}

{% block content %}
	<form method="post" name="urlForm" action="{{ constant( 'URL_DOMAIN' ) }}/noticias/subir/url" id="urlForm" >
		<p>
			<label for="news_url">URL de noticia:</label>
			<input type="text" name="news_url" id="news_url" value="http://" />
			{% if ( errors.url ) %}
				<label for="news_url" class="error"> {{ errors.url }} </label>
			{% endif %}
		</p>
		<input type="submit" name="next" id="next" value="Siguiente" />
	</form>
{% endblock content %}
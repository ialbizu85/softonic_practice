{% extends 'common/layout.tpl.php' %}

{% block javascript %}
<script src="{{ constant( 'URL_JS' ) }}/lib/form.js" type="text/javascript"></script>
<script src="{{ constant( 'URL_JS' ) }}/news/vote.js" type="text/javascript"></script>
<script src="{{ constant( 'URL_JS' ) }}/news/search.js" type="text/javascript"></script>
{% endblock javascript %}

{% block title %}
	Buscar noticias
{% endblock title %}


{% block content %}
<section id="busqueda">
	<form method="get" id="searchForm" action="{{ constant( 'URL_DOMAIN' ) }}/search" >
		<p>
			<label for="text">Inserte el texto que desea encontrar:</label>
			<input type="text" name="text" id="text" value="{{ text }}"/>
			<input type="submit" value="Buscar" />
			{% if errors %}
				{{ errors.text }}
			{% endif %}
		</p>
		<input type="radio" class="filter" name="filter" value="everything"
		{% if not filter or  filter == 'everything' %}
			checked="checked"
		{% endif %}
		/>todos
		<input type="radio" class="filter" name="filter" value="frontpage"
		{% if filter == 'frontpage' %}
			checked="checked"
		{% endif %}
		/>Portada
		<input type="radio" class="filter" name="filter" value="popular"
		{% if filter == 'popular' %}
			checked="checked"
		{% endif %}
		/>Populares
		<input type="radio" class="filter" name="filter" value="pending"
		{% if filter == 'pending' %}
			checked="checked"
		{% endif %}
		/>Pendientes
		<input type="radio" class="filter" name="filter" id="filter_user" value="user"
		{% if filter == 'user' %}
			checked="checked"
		{% endif %}
		/>Usuario
		<input type="text" id="username" name="username" value="{{ username }}"/>
		{% if errors %}
				{{ errors.username }}
		{% endif %}
	</form>
</section>

{% if news_list %}
<section id="news_list">
	{% for news in news_list %}
	<article class="news">
		<div class="votes">
			<div>
				<p class="num_votes">{{ news.votes }}</p>
				{% if layout.is_logged %}
					{% if news.is_voted %}
						<p class="vote">¡Chachi!</p>
					{% else %}
						<a href="#" class="vote" id="{{ news.news_id }}" title="Votar">Meneame</a>
					{% endif %}
				{% else %}
					<a href="{{ constant( 'URL_DOMAIN' ) }}/login" class="vote" title="Acceder al sistema">
						Acceso
					</a>
				{% endif %}
			</div>
		</div>
		<div class="news_info">
			<a class="title" href="{{ news.url }}" target="_blank">{{ news.title|raw }}</a>
			<div class="details">
				<p class="url">{{ news.url }}</p>
				<p class="author_date">por
					<a class="author" href="{{ constant( 'URL_DOMAIN' ) }}/perfil/{{ news.author }}/noticias">
						{{ news.author|raw }}
					</a>
					el {{ news.date }}
				</p>
			</div>
			<p class="summary">{{ news.summary|raw }}</p>
			<a href="{{ domain }}/noticias/{{ news.title_slug }}">Comentarios</a>
		</div>
	</article>
	{% endfor %}

	{% import 'common/paginator.tpl.php' as paginator_builder %}
	{{ paginator_builder.make( paginator ) }}
</section>
{% endif %}

{% endblock %}
{% extends 'common/profilelayout.tpl.php' %}

{% block javascript %}
<script src="{{ constant( 'URL_JS' ) }}/news/vote.js" type="text/javascript"></script>
{% endblock javascript %}

{% block profile_user %}
<a href="{{ constant( 'URL_DOMAIN' ) }}/perfil/{{ username }}">Perfil de usuario</a>
{% endblock profile_user %}

{% block title %}
	Noticias publicadas por "{{ username|raw }}"
{% endblock title %}

{% block info %}
<section id="news_list">
	{% if news_list is empty %}
		<p>El usuario no ha publicado ninguna noticia.</p>
	{% endif %}

	{% for news in news_list %}
	<article class="news">
		<div class="votes">
			<div>
				<p class="num_votes">{{ news.votes }}</p>
				{% if layout.is_logged %}
					{% if news.is_voted %}
						<p class="vote">¡Chachi!</p>
					{% else %}
						<a href="#" class="vote" id="{{ news.news_id }}" title="Votar">Meneame</a>
					{% endif %}
				{% else %}
					<a href="{{ constant( 'URL_DOMAIN' ) }}/login" class="vote"  title="Acceder al sistema">
						Acceso
					</a>
				{% endif %}
			</div>
		</div>
		<div class="news_info">
			<a class="title" href="{{ news.url }}">{{ news.title|raw }}</a>
			<div class="details">
				<p class="url">{{ news.url }}</p>
				<p class="author_date">por
					<a class="author" href="{{ constant( 'URL_DOMAIN' ) }}/perfil/{{ news.author }}/noticias">
						{{ news.author|raw }}
					</a>
					el {{ news.date }}
				</p>
			</div>
			<p class="summary">{{ news.summary|raw }}</p>
			<a href="{{ constant( 'URL_DOMAIN' ) }}/noticias/{{ news.title_slug }}">Comentarios</a>
		</div>
	</article>
	{% endfor %}

	{% import 'common/paginator.tpl.php' as paginator_builder %}
	{{ paginator_builder.make( paginator ) }}
</section>

{% endblock info %}
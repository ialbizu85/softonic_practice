{% extends 'common/layout.tpl.php' %}

{% block javascript %}
	<script src="{{ constant( 'URL_JS' ) }}/lib/form.js" type="text/javascript"></script>
	<script src="{{ constant( 'URL_JS' ) }}/news/vote.js" type="text/javascript"></script>
	<script src="{{ constant( 'URL_JS' ) }}/news/comments.js" type="text/javascript"></script>
{% endblock javascript %}

{% block title %}
	Comentarios de "{{ news.title|raw }}"
{% endblock title %}

{% block content %}

{% if ( errors.news ) %}
	<label class="error">{{ errors.noticia }}</label>
{% else %}
	<section class="news">
		<div class="votes">
			<div>
				<p class="num_votes">{{ news.votes }}</p>
				{% if layout.is_logged %}
					{% if news.is_voted %}
						<p class="vote">¡Chachi!</p>
					{% else %}
						<a href="#" class="vote" id="{{ news.news_id }}" title="Votar">Meneame</a>
					{% endif %}
				{% else %}
					<a href="{{ constant( 'URL_DOMAIN' ) }}/login" class="vote" alt="Acceder al sistema" title="Acceder al sistema">
						Acceso
					</a>
				{% endif %}
			</div>
		</div>
		<div class="news_info">
			<a class="title" href="{{ news.url }}" target="_blank">{{ news.title|raw }}</a>
			<div class="details">
				<p class="url">{{ news.url }}</p>
				<span>por </span>
				{% if not news.author %}
					anónimo
				{% else %}
					<a class="author" href="{{ constant( 'URL_DOMAIN' ) }}/perfil/{{ news.author }}/noticias">
						{{ news.author|raw }}
					</a>
				{% endif %}
				<span> el {{ news.date }}</span>
			</div>
			<p class="summary">{{ news.summary|raw }}</p>
			<a href="{{ constant( 'URL_DOMAIN' ) }}/noticias/{{ news.title_slug }}">Comentarios</a>
		</div>
	</section>

	{% if layout.is_logged %}
	<section>
		<h3 class="error">{{ error }}</h3>
		<form id="commentForm" method="post">
			<textarea id="comment" name="comment" cols="100" rows="10"></textarea>
			<input type="submit" id="send" value="Enviar" />
			{% if errors.comment %}
			<label for="comment" class="error"> {{ errors.comment }} </label>
			{% endif %}
			<input type="hidden" id="token" name="token" value="{{ token }}" />
		</form>
	</section>
	{% endif %}

	<section id="comments_list">
		{% for comment in comments %}
			<article class="comment" >
				<p class="content">{{ comment.content|raw }}</p>
				<p class="details">
					{{ comment.date }} por
					{% if not comment.username %}
						anónimo
					{% else %}
						<a href="{{ constant( 'URL_DOMAIN' ) }}/perfil/{{ comment.username }}">#{{ comment.username|raw }}</a>
					{% endif %}
				</p>
			</article>
		{% endfor %}

		{% if comments is empty %}
			No hay comentarios para esta noticia
		{% endif %}


		{% import 'common/paginator.tpl.php' as paginator_builder %}
		{{ paginator_builder.make( paginator ) }}
	</section>
{% endif %}

{% endblock content %}
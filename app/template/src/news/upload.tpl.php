{% extends "common/layout.tpl.php" %}

{% block javascript %}
<script src="{{ constant( 'URL_JS' ) }}/lib/form.js" type="text/javascript"></script>
<script src="{{ constant( 'URL_JS' ) }}/news/upload.js" type="text/javascript"></script>
{% endblock javascript %}

{% block title %}
	Publicar noticia
{% endblock title %}

{% block content %}
	{% if ( message ) %}
	<h3 class="message"> {{ message }} </h3>
	<a href="{{ constant( 'URL_DOMAIN' ) }}/noticias/{{ title_slug }}">Ir a noticia</a>
	{% else %}
	<form method="post" name="uploadForm" action="{{ constant( 'URL_DOMAIN' )}}/noticias/subir/publicar" id="uploadForm" >
		<p>
			<label for="title" >Titular de noticia:</label>
			<input type="text" name="title" id="title" />
			{% if ( errors.title ) %}
			<label for="title" class="error">{{ errors.title }}</label>
			{% endif %}
		</p>
		<p>
			<label for="summary" >Resumen de noticia:</label>
			<textarea name="summary" id="summary" rows="10" cols="50"></textarea>
			{% if ( errors.summary ) %}
				<label for="summary" class="error">{{ errors.summary }}</label>
			{% endif %}
		</p>
		<input type="submit" name="upload" id="upload" value="Subir" />
	</form>
	{% endif %}
{% endblock content %}
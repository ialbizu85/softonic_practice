{% extends 'common/layout.tpl.php' %}

{% block content %}
<div class="exception404">
    <h1>ERROR 404: PAGE NOT FOUND</h1>
    <p>La página {{ pagina }} al que pretende acceder no existe.</p>
</div>    
{% endblock content %}
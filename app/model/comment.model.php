<?php
/**
 * comment.model.php define CommentModel class
 *
 * @author meneame group
 */

/**
 * CommentModel class to work comment table from database
 */
class CommentModel extends Model
{
	/**
	 * Insert a new comment in database.
	 *
	 * @param int $news_id
	 * @param string $content
	 * @param int $author
	 * @return boolean
	 */
	public function insertComment( $news_id, $content, $author )
	{
		$sql = <<<SQL
INSERT INTO
	mnm_news_comment(
		news_id,
		author,
		content
		)
VALUES(
	?,
	?,
	?
)
SQL;

		$this->connectDb( 'admin' );

		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'iis', $news_id, $author, $content );
		$insert = $stmt->execute( );

		$stmt->close( );
		return $insert;
	}


	/**
	 * Get all news's comments of this page.
	 *
	 * @param int $news_id.
	 * @param int $page.
	 * @param int $element_per_page.
	 * @return array
	 */
	public function getNewsComments( $news_id, $page, $elements_per_page )
	{
		$comments_list	= array( );
		$lower_limit	= $elements_per_page * ( $page - 1 );

		$sql			= <<<SQL
SELECT
   nc.comment_id,
   nc.news_id,
   nc.content,
   nc.author,
   nc.create_at as date,
   u.username
FROM
   mnm_news_comment nc
   INNER JOIN mnm_user u ON nc.author = u.user_id
WHERE
   news_id = ?
ORDER BY
	date DESC
LIMIT
   ?,
   ?
SQL;

		$this->connectDb( 'admin' );

		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'iii', $news_id, $lower_limit, $elements_per_page );
		$stmt->execute( );
		$stmt->bind_result( $comment_id,
							$news_id,
							$content,
							$author,
							$date,
							$username
						);

		while ( $stmt->fetch( ) )
		{
			$comment[ 'comment_id' ]	= $comment_id;
			$comment[ 'news_id' ]		= $news_id;
			$comment[ 'content' ]		= $content;
			$comment[ 'author' ]		= $author;
			$comment[ 'date' ]			= $date;
			$comment[ 'username' ]		= $username;

			$comments_list[]			= $comment;
		}

		$stmt->close( );
		return $comments_list;
	}


	/**
	 * Get total comments number of a news
	 * 
	 * @param int $news_id.
	 * @return int
	 */
	public function getTotalNewsComments( $news_id )
	{
		$sql = <<<SQL
SELECT
   COUNT( * ) AS total
FROM
   mnm_news_comment
WHERE
   news_id = ?
SQL;

		$this->connectDb( 'admin' );

		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'i', $news_id );
		$stmt->execute( );
		$stmt->bind_result( $total );

		$stmt->fetch( );

		$stmt->close( );
		return $total;
	}
}

?>
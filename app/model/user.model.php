<?php
/**
 * user.model.php define UserModel class
 *
 * @author meneame group
 */

/**
 * UserModel class to work with user referred tables from database
 */
class UserModel extends Model
{
	/**
	 * Define user status in the database as active.
	 */
	const STATUS_ACTIVE = 'ACTIVE';

	/**
	 * Define user status in the database as pending.
	 */
	const STATUS_PENDING = 'PENDING';

	/**
	 * Define user status in the database as blocked.
	 */
	const STATUS_BLOCKED = 'BLOCKED';

	/**
	 * Check if user passed by param exists in the database.
	 *
	 * @param string $username
	 * @return boolean
	 */
	public function isRegistered( $username )
	{
		$this->connectDb( 'admin' );

		$sql = <<<SQL
SELECT
	COUNT(*)
FROM
	mnm_user
WHERE
	username = ?
SQL;
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 's', $username );
		$stmt->execute( );

		$stmt->bind_result( $result );

		$stmt->fetch( );
		$stmt->close( );

		return !!$result;
	}

	/**
	 * Insert new user in the database.
	 *
	 * @param string $username
	 * @param string $password
	 * @param string $email
	 * @return int
	 */
	public function setUser( $username, $password, $email )
	{
		$status = self::STATUS_PENDING;
		$this->connectDb( 'admin' );

		$sql = <<<SQL
INSERT INTO
	mnm_user(
		username,
		password,
		email,
		status
		)
VALUES(
	?,
	?,
	?,
	?
)
SQL;
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'ssss', $username, $password, $email, $status );
		$stmt->execute( );
		$user_id = $stmt->insert_id;
		$stmt->close( );

		return $user_id;
	}

	/**
	 * Get user info passing username or email as a parameter.
	 *
	 * @param string $credential
	 * @return boolean
	 */
	public function getUserByCredential( $credential )
	{
		$this->connectDb( 'admin' );

		$sql = <<<SQL
SELECT
   username,
   user_id,
   password,
   status
FROM
   mnm_user
WHERE
   username = ?
   OR email = ?
SQL;
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'ss', $credential, $credential );
		$stmt->bind_result( $user['username'], $user['user_id'], $user['password'], $user['status'] );

		$stmt->execute( );
		$stmt->fetch( );
		$stmt->close( );

		if  ( !$user['username'] )
		{
			return false;
		}

		return $user;
	}

	/**
	 * Update user password stored in the database.
	 *
	 * @param string $password
	 * @param int $user_id
	 * @return boolean
	 */
	public function setPasswd( $password, $user_id )
	{
		$this->connectDb( 'admin' );

		$sql = <<<SQL
UPDATE
   mnm_user
SET
   password = ?
WHERE
   user_id = ?
SQL;
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'si', $password, $user_id );

		$update = $stmt->execute( );
		$stmt->close( );

		return $update;
	}

	/**
	 * Retrieve password that matches with id passed as a parameter.
	 *
	 * @param int $user_id
	 * @return mixed
	 */
	public function getPasswd( $user_id )
	{
		$this->connectDb( 'admin' );

		$sql = <<<SQL
SELECT
   password
FROM
   mnm_user
WHERE
   user_id = ?
SQL;
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'i', $user_id );
		$stmt->bind_result( $password );

		$stmt->execute( );
		$stmt->fetch( );
		$stmt->close( );

		if ( !$password )
		{
			return false;
		}

		return $password;
	}

	/**
	 * Retrieve email that matches with id passed as a parameter.
	 *
	 * @param int $user_id
	 * @return mixed
	 */
	public function getMail( $user_id )
	{
		$this->connectDb( 'admin' );

		$sql = <<<SQL
SELECT
   email
FROM
   mnm_user
WHERE
   user_id = ?
SQL;
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'i', $user_id );
		$stmt->bind_result( $email );

		$stmt->execute( );
		$stmt->fetch( );
		$stmt->close( );

		if ( !$email )
		{
			return false;
		}

		return $email;
	}

	/**
	 * Retrieve user profile info from the database.
	 *
	 * @param string $username
	 * @return array
	 */
	public function getProfileInfoByUserName( $username )
	{
		$this->connectDb( 'admin' );
		$profile_info = array( );

		$sql = <<<SQL
SELECT
   username,
   votes,
   published_news,
   received_votes
FROM
   mnm_user
WHERE
   username = ?
SQL;
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 's', $username );
		$stmt->bind_result(
							$profile_info[ 'username' ],
							$profile_info[ 'votes' ],
							$profile_info[ 'published_news' ],
							$profile_info[ 'received_votes' ]
		);

		$stmt->execute( );
		$stmt->fetch( );
		$stmt->close( );

		if ( !$profile_info[ 'username' ] )
		{
			return false;
		}

		return $profile_info;
	}

	/**
	 * Retrieve user_id that matches with username passed as a parameter.
	 *
	 * @param string $username
	 * @return mixed
	 */
	public function getUserId( $username )
	{
		$this->connectDb( 'admin' );

		$sql = <<<SQL
SELECT
   user_id
FROM
   mnm_user
WHERE
   username = ?
SQL;
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 's', $username );
		$stmt->bind_result( $user_id );

		$stmt->execute( );
		$stmt->fetch( );
		$stmt->close( );

		if ( !$user_id )
		{
			return false;
		}

		return $user_id;
	}

	/**
	* Retrieve username that matches with user_id passed as a parameter.
	*
	* @param int $user_id
	* @return mixed
	*/
	public function getUsername( $user_id )
	{
		$this->connectDb( 'admin' );

		$sql = <<<SQL
SELECT
   username
FROM
   mnm_user
WHERE
   user_id = ?
SQL;
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'i', $user_id );
		$stmt->bind_result( $username );

		$stmt->execute( );
		$stmt->fetch( );
		$stmt->close( );

		if ( !$username )
		{
			return false;
		}
		
		return $username;
	}


	/**
	 * Update user status from pending to active.
	 *
	 * @param int $user_id
	 * @return boolean
	 */
	public function setValidUserStatus( $user_id )
	{
		$status = self::STATUS_ACTIVE;
		$this->connectDb( 'admin' );

		$sql = <<<SQL
UPDATE
   mnm_user
SET
   status = ?
WHERE
   user_id = ?
SQL;
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'si', $status, $user_id );

		$update = $stmt->execute( );
		$stmt->close( );

		return $update;
	}

	/**
	 * Check if email exists in the database.
	 *
	 * @param string $email
	 * @return boolean
	 */
	public function isRegisteredMail ( $email )
	{
		$this->connectDb( 'admin' );

		$sql = <<<SQL
SELECT
	COUNT(*)
FROM
	mnm_user
WHERE
	email = ?
SQL;
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 's', $email );
		$stmt->execute( );

		$stmt->bind_result( $result );

		$stmt->fetch( );
		$stmt->close( );

		return !!$result;
	}


	/**
	 * Check if a user exists in database
	 *
	 * @param integer $user_id
	 * @return boolean
	 */
	public function isValid( $user_id )
	{
		$exists = false;
		$sql	= <<<SQL
SELECT
	TRUE
FROM
   mnm_user
WHERE
   user_id = ?
SQL;

		$this->connectDb( 'admin' );

		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'i', $user_id );
		$stmt->bind_result( $exists );

		$stmt->execute( );
		$stmt->fetch( );
		$stmt->close( );

		return !!$exists;
	}
	
	/**
	 * Unsubscribe user from the database.
	 * 
	 * @param int $user_id
	 * @return boolean 
	 */
	public function unsubscribeUser( $user_id )
	{
		$status = self::STATUS_BLOCKED;
		$this->connectDb( 'admin' );

		$sql = <<<SQL
UPDATE
	mnm_user
SET
   username = null,
   email = null,
   password = null,
   status = ?
WHERE
   user_id = ?
SQL;
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'si', $status, $user_id );

		$stmt->execute( );

		$result = $stmt->affected_rows;

		$stmt->close( );

		return !!$result;
	}
	/**
	 * Delete a single cookie login from database.
	 * Return true if login is removed, false otherwise.
	 *
	 * @param integer 	$user_id
	 * @param string 	$token
	 * @return boolean
	 */
	public function deleteCookieLogin( $user_id, $token )
	{
		$this->connectDb( 'admin' );

		require CORE__CONFIG_DIR . '/rememberpass.config.php';
		$token = hash_hmac( 'sha1', $token, $_seed );

		$sql = <<<SQL
DELETE FROM
	mnm_login_cookie
WHERE
	user_id = ?
	AND token = ?
SQL;
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'is', $user_id, $token );

		$stmt->execute( );
		$is_erased = $stmt->affected_rows;
		$stmt->close( );

		return !!$is_erased;
	}

	/**
	 * Delete all cookies login from database.
	 * Return number of deleted cookies if user_id exists, 0 otherwise.
	 *
	 * @param integer $user_id
	 * @return boolean
	 */
	public function deleteAllCookiesLogin( $user_id )
	{
		$this->connectDb( 'admin' );

		$sql = <<<SQL
DELETE FROM
	mnm_login_cookie
WHERE
	user_id = ?
SQL;
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'i', $user_id );

		$stmt->execute( );
		$num_deleted_cookies = $stmt->affected_rows;
		$stmt->close( );

		return $num_deleted_cookies;
	}

	/**
	 * Add a new cookie login to a specified user
	 *
	 * @param integer	$user_id
	 * @param mixed		$token
	 * 
	 * @return boolean
	 */
	public function addCookieLogin( $user_id, $token )
	{
		$this->connectDb( 'admin' );

		require CORE__CONFIG_DIR . '/rememberpass.config.php';
		$token = hash_hmac( 'sha1', $token, $_seed );

		$sql = <<<SQL
INSERT IGNORE INTO
	mnm_login_cookie (
		`user_id`,
		`token`
	)
VALUES
	(
	   ?,
	   ?
	)
SQL;
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'is', $user_id, $token );

		$stmt->execute( );
		$is_inserted = $stmt->affected_rows;
		$stmt->close( );

		return !!$is_inserted;
	}
}

?>
<?php
/**
 * news.model.php Class to work with database news table.
 */

/**
 * Define NewsModel class
 */
class NewsModel extends Model
{
	/**
	 * Get a news by title slug from database.
	 *
	 * @param string $title_slug
	 * @return array
	 */
	public function getNewsByTitleSlug( $title_slug )
	{
		$sql = <<<SQL
SELECT
   n.news_id,
   n.user_id,
   n.title,
   n.title_slug,
   n.summary,
   n.url,
   n.create_at as date,
   n.votes,
   u.username as author
FROM
   mnm_news n
   INNER JOIN mnm_user u ON n.user_id = u.user_id
WHERE
   title_slug = ?
LIMIT
   1
SQL;

		$this->connectDb( 'admin' );

		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 's', $title_slug );
		$stmt->execute( );
		$stmt->bind_result( $news[ 'news_id' ],
							$news[ 'user_id' ],
							$news[ 'title' ],
							$news[ 'title_slug' ],
							$news[ 'summary' ],
							$news[ 'url' ],
							$date,
							$news[ 'votes' ],
							$news[ 'author' ]
						);

		$stmt->fetch( );

		$timestamp		= strtotime( $date );
		$news[ 'date' ]	= date( 'Y/m/d h:m:s', $timestamp );

		$stmt->close( );
		return $news;
	}


	/**
	 * Insert new news in database
	 *
	 * @param string $title
	 * @param string $title_slug
	 * @param string $summary
	 * @param string $url
	 * @param integer $user_id
	 * @return boolean
	 */
	public function insertNews( $title, $title_slug, $summary, $url, $user_id )
	{
		$url = $this->cleanUri( $url );
		$sql = <<<SQL
INSERT INTO
	mnm_news(
		user_id,
		title,
		title_slug,
		summary,
		url
	)
VALUES(
	?,
	?,
	?,
	?,
	?
)
SQL;

		$this->connectDb( 'admin' );

		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'issss', $user_id, $title, $title_slug, $summary, $url );
		$insert = $stmt->execute( );

		$stmt->close( );
		return $insert;
	}

	/**
	 * Insert a new vote to a notice
	 *
	 * @param integer $news_id
	 * @param integer $user_id
	 * @return boolean
	 */
	public function insertVote( $news_id, $user_id )
	{
		$sql = <<<SQL
INSERT IGNORE INTO
	mnm_news_vote (
		`news_id`,
		`user_id`
	)
VALUES (
	?,
	?
)
SQL;

		$this->connectDb( 'admin' );

		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'ii', $news_id, $user_id );
		$stmt->execute( );
		$affected_rows = $stmt->affected_rows;

		$stmt->close( );

		return !!$affected_rows;
	}


	/**
	 * Get news's votes number
	 *
	 * @param int $news_id
	 * @return int
	 */
	public function getNewsVotes( $news_id )
	{
		$votes 	= 0;
		$sql	= <<<SQL
SELECT
	votes
FROM
   mnm_news
WHERE
   news_id = ?
SQL;

		$this->connectDb( 'admin' );

		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'i', $news_id );
		$stmt->bind_result( $votes );

		$stmt->execute( );
		$stmt->fetch( );

		return $votes;;
	}

	/**
	 * Check if a news exists in database
	 *
	 * @param integer $news_id
	 * @return boolean
	 */
	public function isValid( $news_id )
	{
		$exists = false;
		$sql	= <<<SQL
SELECT
	TRUE
FROM
   mnm_news
WHERE
   news_id = ?
SQL;

		$this->connectDb( 'admin' );

		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'i', $news_id );
		$stmt->bind_result( $exists );

		$stmt->execute( );
		$stmt->fetch( );

		return !!$exists;
	}

	/**
	 * Check if url given is previously published
	 *
	 * @param string $url
	 * @return boolean
	 */
	public function isUrlRegistered( $url )
	{
		$exists = false;
		$url	= $this->cleanUri( $url );
		$sql	= <<<SQL
SELECT
	TRUE
FROM
   mnm_news
WHERE
   url = ?
SQL;

		$this->connectDb( 'admin' );

		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 's', $url );
		$stmt->bind_result( $exists );

		$stmt->execute( );
		$stmt->fetch( );

		return !!$exists;
	}

	/**
	 * Clean a given uri for remove anchors and last backslash
	 *
	 * @param string $url
	 * @return string
	 */
	public function cleanUri( $url )
	{
		$url 		= trim( $url );
		$anchor_pos = strpos( $url, '#' );

		if ( false !== $anchor_pos )
		{
			$url = substr( $url, 0, $anchor_pos );
		}

		$url = trim( $url, '/' );

		return $url;
	}

	/**
	 * Get news_list
	 *
	 * @param string $sql
	 * @param int $lower_limit
	 * @param int $interval_votes
	 * @return array
	 */
	private function getNewsList( $page, $elements_per_page, $date_limit, $interval_votes, $having, $text )
	{
		$news_list		= array( );
		$news			= array( );
		$lower_limit	= $elements_per_page * ( $page - 1 );

		$prepare_where = '';
		if ( $text != '' )
		{
			$prepare_where  =  'AND ( title LIKE ? OR summary LIKE ? )';
		}

		$sql			= <<<SQL
SELECT
	n.news_id,
	n.user_id,
	n.title,
	n.title_slug,
	n.url,
	n.summary,
	n.votes,
	n.create_at AS date,
	u.username AS author,
	COUNT( nv.news_id ) AS votes
FROM
	mnm_news n
	INNER JOIN mnm_user u ON n.user_id =  u.user_id
	LEFT JOIN mnm_news_vote nv ON nv.news_id = n.news_id
WHERE
	IFNULL( nv.create_at, NOW( ) ) > ?
	{$prepare_where}
GROUP BY
	n.news_id
HAVING
   {$having}
ORDER BY
	votes DESC
LIMIT
   ?,
   ?
SQL;

		$this->connectDb( 'admin' );
		$stmt	= $this->db->prepare( $sql );

		if ( $text != '' )
		{
			$text = $this->escapeRegexp( $text );
			$text = '%' . $text . '%';
			$stmt->bind_param( 'sssiii', $date_limit, $text, $text, $interval_votes, $lower_limit, $elements_per_page );
		}
		else
		{
			$stmt->bind_param( 'siii', $date_limit, $interval_votes, $lower_limit, $elements_per_page );
		}

		$stmt->execute( );
		$stmt->bind_result(
							$news_id,
							$user_id,
							$title,
							$title_slug,
							$url,
							$summary,
							$votes,
							$date,
							$author,
							$front_page_votes
						);

		while ( $stmt->fetch( ) )
		{
			$news[ 'news_id' ]			= $news_id;
			$news[ 'user_id' ]			= $user_id;
			$news[ 'title' ]			= $title;
			$news[ 'title_slug' ]		= $title_slug;
			$news[ 'url' ]				= $url;
			$news[ 'summary' ]			= $summary;
			$news[ 'votes' ]			= $votes;
			$timestamp					= strtotime( $date );
			$news[ 'date' ]				= date( 'Y/m/d h:m:s', $timestamp );
			$news[ 'author' ]			= $author;
			$news[ 'interval_votes' ]	= $front_page_votes;

			$news_list[]				= $news ;
		}

		$stmt->close( );

		return $news_list;
	}

	/**
	 * Get front page news
	 *
	 * @param integer $page
	 * @param integer $elements_per_page
	 * @param integer $day_votes
	 * @return array
	 */
	public function getFrontPageNews( $page, $elements_per_page, $text, $day_votes )
	{
		$date_limit	= $this->getSubDateMysql( 'P1D' );
		$having		= '? <= votes';

		$news_list = $this->getNewsList( $page , $elements_per_page , $date_limit, $day_votes, $having, $text );
		return $news_list;
	}


	/**
	 * Get most popular news
	 *
	 * @param integer $page
	 * @param integer $elements_per_page
	 * @param integer $month_votes
	 * @return array
	 */
	public function getPopularNews( $page, $elements_per_page, $text, $month_votes )
	{
		$date_limit	= $this->getSubDateMysql( 'P1M' );
		$having		= '? <= votes';

		$news_list = $this->getNewsList( $page , $elements_per_page , $date_limit, $month_votes, $having, $text );
		return $news_list;
	}


	/**
	 * Get pending news
	 *
	 * @param integer $page
	 * @param integer $elements_per_page
	 * @param integer $month_votes
	 * @return array
	 */
	public function getPendingNews( $page, $elements_per_page, $text, $month_votes )
	{
		$date_limit	= $this->getSubDateMysql( 'P1M' );
		$having		= '? > votes';

		$news_list = $this->getNewsList( $page , $elements_per_page , $date_limit, $month_votes, $having, $text );
		return $news_list;
	}


	/**
	 * Get user's news
	 *
	 * @param integer $page
	 * @param integer $elements_per_page
	 * @param string $username
	 * @return array
	 */
	public function getUserNews( $page, $elements_per_page, $text, $username )
	{
		$prepare_where = '';
		if ( $text != '' )
		{
			$prepare_where  =  'AND ( title LIKE ? OR summary LIKE ? )';
		}

		$lower_limit	= $elements_per_page * ( $page - 1 );
		$news_list		= array( );
		$news			= array( );
		$sql			= <<<SQL
SELECT
	n.news_id,
	n.user_id,
	n.title,
	n.title_slug,
	n.url,
	n.summary,
	n.votes,
	n.create_at as date,
	u.username as author
FROM
	mnm_news n
	INNER JOIN mnm_user u on n.user_id =  u.user_id
WHERE
	u.username = ?
	{$prepare_where}
ORDER BY
	date DESC
LIMIT
   ?,
   ?
SQL;

		$this->connectDb( 'admin' );
		$stmt = $this->db->prepare( $sql );

		if ( $text != '' )
		{
			$text = $this->escapeRegexp( $text );
			$text = '%' . $text . '%';
			$stmt->bind_param( 'sssii', $username, $text, $text, $lower_limit, $elements_per_page );
		}
		else
		{
			$stmt->bind_param( 'sii', $username, $lower_limit, $elements_per_page );
		}

		$stmt->execute( );
		$stmt->bind_result(
							$news_id,
							$user_id,
							$title,
							$title_slug,
							$url,
							$summary,
							$votes,
							$date,
							$author
		);

		while ( $stmt->fetch( ) )
		{
			$news[ 'news_id' ]		= $news_id;
			$news[ 'user_id' ]		= $user_id;
			$news[ 'title' ]		= $title;
			$news[ 'title_slug' ]	= $title_slug;
			$news[ 'url' ]			= $url;
			$news[ 'summary' ]		= $summary;
			$news[ 'votes' ]		= $votes;
			$timestamp				= strtotime( $date );
			$news[ 'date' ]			= date( 'Y/m/d h:m:s', $timestamp );
			$news[ 'author' ]		= $author;

			$news_list[]			= $news ;
		}

		$stmt->close( );

		return $news_list;
	}


	/**
	 * Get total users new.
	 *
	 * @param string $username
	 * @return int
	 */
	public function getTotalUserNews( $username, $text = '' )
	{
		$prepare_where = '';
		if ( $text != '' )
		{
			$prepare_where  =  'AND ( title LIKE ? OR summary LIKE ? )';
		}

		$total			= 0;
		$sql			= <<<SQL
SELECT
	COUNT( * ) AS total
FROM
	mnm_news n
	INNER JOIN mnm_user u on n.user_id =  u.user_id
WHERE
	u.username = ?
	{$prepare_where}
SQL;

		$this->connectDb( 'admin' );
		$stmt	= $this->db->prepare( $sql );
		if ( $text != '' )
		{
			$text = $this->escapeRegexp( $text );
			$text = '%' . $text . '%';
			$stmt->bind_param( 'sss', $username, $text, $text );
		}
		else
		{
			$stmt->bind_param( 's', $username);
		}

		$stmt->execute( );
		$stmt->bind_result( $total );
		$stmt->fetch( );
		$stmt->close( );

		return $total;
	}


	/**
	 * Get total front page news
	 *
	 * @param int $day_votes
	 * @return int
	 */
	public function getTotalFrontPageNews( $day_votes, $text = '' )
	{
		$date_limit	= $this->getSubDateMysql( 'P1D' );
		$having		= '? <= COUNT( nv.news_id )';

		$total = $this->getTotalNews( $date_limit, $day_votes, $having, $text );
		return $total;
	}


	/**
	 * Get total populars news.
	 *
	 * @param int $month_votes
	 * @return int
	 */
	public function getTotalPopularNews( $month_votes, $text = '' )
	{
		$date_limit	= $this->getSubDateMysql( 'P1M' );
		$having		= '? <= COUNT( nv.news_id )';

		$total = $this->getTotalNews( $date_limit, $month_votes, $having, $text );
		return $total;
	}


	/**
	 * Get total pendings news.
	 *
	 * @param int $month_votes
	 * @return int
	 */
	public function getTotalPendingNews( $month_votes, $text = '' )
	{
		$date_limit	= $this->getSubDateMysql( 'P1M' );
		$having		= '? > COUNT( nv.news_id )';

		$total = $this->getTotalNews( $date_limit, $month_votes, $having, $text );
		return $total;
	}

	/**
	 *
	 * @param string $sql
	 * @param int $date_limit
	 * @param int $month_votes
	 * @return int
	 */
	private function getTotalNews( $date_limit, $interval_votes, $having, $text )
	{
		$total = 0;
		$prepare_where = '';
		if ( $text != '' )
		{
			$prepare_where  =  'AND ( title LIKE ? OR summary LIKE ? )';
		}

		$sql = <<<SQL
SELECT
	COUNT( * )
FROM(
   SELECT
		COUNT( n.news_id )
	FROM
		mnm_news n
		LEFT JOIN mnm_news_vote nv ON nv.news_id = n.news_id
	WHERE
		( nv.create_at > ?
			OR n.votes = 0 )
		{$prepare_where}
	GROUP BY
		n.news_id
	HAVING
		{$having}
) t
SQL;
		$this->connectDb( 'admin' );
		$stmt = $this->db->prepare( $sql );
		if ( $text != '' )
		{
			$text = $this->escapeRegexp( $text );
			$text = '%' . $text . '%';
			$stmt->bind_param( 'issi', $text, $text, $date_limit, $interval_votes );
		}
		else
		{
			$stmt->bind_param( 'ii', $date_limit, $interval_votes );
		}
		$stmt->execute( );
		$stmt->bind_result( $total );
		$stmt->fetch( );
		$stmt->close( );

		return $total;
	}


	/**
	 * Return a list of news with a flag indicating if news was voted by
	 * provided user_id.
	 *
	 * @param integer $user_id
	 * @param array $news_ids
	 * @return array
	 */
	public function getVotedNews( $user_id, $news_ids )
	{
		$amount_news = sizeof( $news_ids );

		if ( 0 === $amount_news )
		{
			return array( );
		}

		$params	= implode(
						",",
						array_fill( 0, $amount_news, '?' )
		);

		$sql = <<<SQL
SELECT
	news_id
FROM
	mnm_news_vote
WHERE
	user_id = ?
	AND news_id IN ({$params})
SQL;

		$this->connectDb( 'admin' );
		$stmt		= $this->db->prepare( $sql );

		$params		= array_merge( array( $user_id ), $news_ids );

		$binding	= array_merge(
							array( str_repeat( 'i', $amount_news + 1 ) ),
							$params
		);

		foreach ( $binding AS $key => $val )
		{
			$tmp[$key] = &$binding[$key];
		}

		call_user_func_array(
							array(
								$stmt,
								'bind_param'
							),
							$tmp
		);

		$stmt->execute( );
		$stmt->bind_result( $news_id );

		$news_voted = array( );
		while ( $stmt->fetch( ) )
		{
			$news_voted[ $news_id ] = true;
		}

		$stmt->close ( );

		return $news_voted;
	}

	/**
	 * Chech if url is registered in database
	 *
	 * @param string $title_slug
	 */
	public function isRegisteredTitleSlug( $title_slug )
	{
		$sql	= <<<SQL
SELECT
	COUNT( * )
FROM
   mnm_news
WHERE
   title_slug = ?
SQL;

		$this->connectDb( 'admin' );
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 's', $title_slug );
		$stmt->execute( );
		$stmt->bind_result( $total );
		$stmt->fetch( );
		$stmt->close( );

		$is_registered = $total > 0;

		return $is_registered;
	}

	/**
	 * Get news that contains $text text.
	 *
	 * @param int $current_page
	 * @param int $elements_per_page
	 * @param string $text
	 * @return array
	 */
	public function searchNews( $page, $elements_per_page, $text )
	{
		$lower_limit	= $elements_per_page * ( $page - 1 );
		$news_list		= array( );
		$news			= array( );
		$text 			= $this->escapeRegexp( $text );
		$text			= '%' . $text . '%';
		$sql			= <<<SQL
SELECT
	n.news_id,
	n.user_id,
	n.title,
	n.title_slug,
	n.url,
	n.summary,
	n.votes,
	n.create_at as date,
	u.username as author
FROM
	mnm_news n
	INNER JOIN mnm_user u ON n.user_id = u.user_id
WHERE
	n.title LIKE ?
	OR n.summary LIKE ?
LIMIT
   ?,
   ?
SQL;

		$this->connectDb( 'admin' );
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'ssii', $text, $text, $lower_limit, $elements_per_page );
		$stmt->execute( );
		$stmt->bind_result(
							$news_id,
							$user_id,
							$title,
							$title_slug,
							$url,
							$summary,
							$votes,
							$date,
							$author
		);

		while ( $stmt->fetch( ) )
		{
			$news[ 'news_id' ]		= $news_id;
			$news[ 'user_id' ]		= $user_id;
			$news[ 'title' ]		= $title;
			$news[ 'title_slug' ]	= $title_slug;
			$news[ 'url' ]			= $url;
			$news[ 'summary' ]		= $summary;
			$news[ 'votes' ]		= $votes;
			$timestamp				= strtotime( $date );
			$news[ 'date' ]			= date( 'Y/m/d h:m:s', $timestamp );
			$news[ 'author' ]		= $author;

			$news_list[]			= $news ;
		}

		$stmt->close( );

		return $news_list;
	}


		/**
	 * Get news that contains $text text.
	 *
	 * @param int $current_page
	 * @param int $elements_per_page
	 * @param string $text
	 * @return array
	 */
	public function getTotalNewsByText( $text )
	{
		$text 			= $this->escapeRegexp( $text );
		$text			= '%' . $text . '%';
		$sql			= <<<SQL
SELECT
	COUNT( * ) AS total
FROM
	mnm_news
WHERE
	title LIKE ?
	OR summary LIKE ?
SQL;

		$this->connectDb( 'admin' );
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'ss', $text, $text );
		$stmt->execute( );
		$stmt->bind_result( $total );

		$stmt->fetch( );
		$stmt->close( );

		return $total;
	}

	/**
	 * From actualtime sub an interval and return
	 * the result of sub in mysql timestamp format
	 *
	 * @param string $sub_internval @see http://es2.php.net/manual/en/class.dateinterval.php
	 * @return integer
	 */
	protected function getSubDateMysql( $sub_interval )
	{
		$date			= new DateTime( );
		$date			= $date->sub( new DateInterval( $sub_interval ) );
		$date_timestamp = $date->getTimestamp( );
		$date_limit		= date( 'YmdHis', $date_timestamp );

		return $date_limit;
	}

	/**
	 * Escape regexp expressions from string
	 *
	 * @param string $string
	 * @return string
	 */
	protected function escapeRegexp( $string )
	{
		$string = strtr(
						$string,
						array(
							'%' => '\%',
							'_' => '\_'
						)
		);

		return $string;
	}
}

?>
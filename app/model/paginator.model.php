<?php
/**
 * paginator.model.php define Paginator class to work with paginators.
 *
 * @author meneame group
 */

/**
 * Paginator class
 */
class PaginatorModel extends Model
{
	/**
	 * Get pages range to show in paginator.
	 *
	 * @param int $current_page
	 * @param int $total_pages 
	 * @param int $page_per_range
	 */
	public function getPagesRange( $current_page, $total_pages, $page_per_range )
	{
		$range						= array( );
		$range[ 'current_page' ]	= $current_page;
		$range[ 'total_pages' ]		= $total_pages;

		$range[ 'initial_page' ]	= $current_page  - $page_per_range;
		$range[ 'final_page' ]		= $current_page + $page_per_range;
		
		if ( $range[ 'initial_page' ] <= 1 )
		{
			$range[ 'initial_page' ]	= 1;
			$range[ 'final_page' ]		= 1 + $page_per_range * 2;

			if ( $range[ 'final_page' ] > $total_pages )
			{
				$range[ 'final_page' ] = $total_pages;
			}
		}
		
		
		if ( $range[ 'final_page' ] > $total_pages )
		{
			$range[ 'initial_page' ]	= $total_pages - $page_per_range * 2;
			$range[ 'final_page' ]		= $total_pages;

			if ( $range[ 'initial_page' ] < 1 )
			{
				$range[ 'initial_page' ] = 1;
			}
		}

		return $range;
	}
	
	
	/**
	 * Calculate total pages to show
	 *
	 * @param int $total_elements
	 * @param int $elements_per_page
	 * @return int 
	 */
	public function getTotalPages( $total_elements, $elements_per_page )
	{
		$total_pages = ceil( $total_elements / $elements_per_page );

		return $total_pages;
	}
}

?>
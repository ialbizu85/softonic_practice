<?php
/**
 * token.model.php Class for generate tokens
 */

/**
 * Class for generate tokens
 *
 * @author jose
 */
class TokenModel extends Model
{
	const TOKEN_LENGTH = 16;

	/**
	 * Generate a random number
	 *
	 * @return integer
	 */
	public function generateNumeric( )
	{
		$token = rand( 1, 200 ) * time( );

		return $token;
	}

	/**
	 * Genetate a token trough the username and timestamp. The token must be unique.
	 *
	 * @param string $username
	 * @return string
	 */
	public  function generateAlphanumeric ( $username, $seed )
	{
		$token = md5 ( time( ) . $username . $seed );
		$token = substr( $token , 0, self::TOKEN_LENGTH );

		return $token;
	}
	
	/**
	 * Retrieve a token that matches with id passed as a parameter.
	 *
	 * @param int $user_id
	 * @return mixed
	 */
	public function getToken( $user_id )
	{
		$this->connectDb( 'admin' );

		$sql = <<<SQL
SELECT
   token
FROM
   mnm_user_token
WHERE
   user_id = ?
SQL;
		
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'i', $user_id );
		$stmt->bind_result( $token );

		$stmt->execute( );
		$stmt->fetch( );
		$stmt->close( );

		if ( !$token )
		{
			return false;
		}

		return $token;
	}
	
	/**
	 * Delete token from the database.
	 *
	 * @param int $user_id
	 * @return boolean
	 */
	public function deleteToken( $user_id )
	{
		$this->connectDb( 'admin' );

		$sql = <<<SQL
DELETE
FROM
   mnm_user_token
WHERE
   user_id = ?
SQL;
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'i', $user_id );

		$update = $stmt->execute( );
		$stmt->close( );

		return $update;
	}
	
	/**
	 * Insert Token for the user validation.
	 *
	 * @param string $token
	 * @param int $user_id
	 * @return boolean
	 */
	public function setToken( $token, $user_id )
	{
		$this->connectDb( 'admin' );

		$sql = <<<SQL
INSERT INTO
	mnm_user_token(
		user_id,
		token
		)
VALUES(
	?,
	?
)
SQL;
		$stmt = $this->db->prepare( $sql );
		$stmt->bind_param( 'is', $user_id, $token );

		$insert = $stmt->execute( );
		$stmt->close( );

		return $insert;
	}
}
?>
<?php
/*
 * search.ctrl.php
 *
 * @author meneame group
 */

/**
 * Define SearchNewsController class
 */
class SearchNewsController extends NewsNewsController
{
	/**
	 * FormValidator instance.
	 *
	 * @var FormValidator
	 */
	protected $validator;

	/**
	 * Text to search
	 *
	 * @var string
	 */
	protected $text;

	/**
	 * username to search
	 *
	 * @var string
	 */
	protected $username = '';

	/**
	 * search filter
	 *
	 * @var string
	 */
	protected $filter = '';

	/**
	 * FilterGet instance.
	 *
	 * @var FilterGet
	 */
	protected $get;

	/**
	 * Main method
	 */
	public function run( )
	{
		$this->get 			= FilterGet::getInstance( );
		$this->validator 	= new FormValidator( );

		if ( $this->get->keyExist( 'text' ) )
		{
			$this->text = $this->get->getText( 'text' );

			$is_valid	= $this->validate( );

			if ( $is_valid )
			{
				$news_list = $this->getNewsList( );

				$this->template->assign( 'news_list', $news_list );
				$this->template->assign( 'paginator', $this->paginator );
			}

			$errors = $this->validator->getErrors( );
			$this->template->assign( 'errors', $errors );
			$this->template->assign( 'text', $this->text );
			$this->template->assign( 'username', $this->username );
			$this->template->assign( 'filter', $this->filter );

		}

		$this->template->setTemplate( 'news/search' );
	}

	/**
	 * Validate form.
	 * Return true if is valid, false otherwise.
	 *
	 * @return boolean
	 */
	protected function validate( )
	{
		$this->validator->setField( 'text', $this->text )
						->required( 'Es obligatorio rellenar el campo.' )
						->minLength( 3, 'La palabra a buscar debe tener 3 caracteres como mínimo.' );

		if ( $this->get->keyExist( 'username' ) )
		{
			$this->validator->setField( 'username', $this->get->getText( 'username' ) )
							->required( 'Es obligatorio rellenar el campo.' );
		}

		return $this->validator->isFormValid( );
	}


	/**
	 * Return a list of news
	 *
	 * @return array
	 */
	protected function  getNewsList( )
	{
		$news_list		= array( );
		$href			= '/search';
		$username		= '';
		$interval_votes = 0;

		$arguments		= array(
							$this->paginator[ 'current_page' ],
							$this->elements_per_page,
							$this->text
		);

		$params			= array( );

		$this->filter 	= $this->get->getText( 'filter' );
		switch( $this->filter )
		{
			case 'frontpage':
				$arguments[]		= $this->day_votes;
				$params				= array( $this->day_votes );
				$total_method		= 'getTotalFrontPageNews';
				$get_method			= 'getFrontPageNews';
				break;

			case 'popular':
				$arguments[]		= $this->month_votes;
				$params				= array( $this->month_votes );
				$total_method		= 'getTotalPopularNews';
				$get_method			= 'getPopularNews';
				break;

			case 'pending':
				$arguments[]		= $this->month_votes;
				$params				= array( $this->month_votes );
				$total_method		= 'getTotalPendingNews';
				$get_method			= 'getPendingNews';
				break;

			case 'user':
				$this->username		= $this->get->getText( 'username' );
				$arguments[]		= $this->username;
				$params				= array( $this->get->getText( 'username' ) );
				$total_method		= 'getTotalUserNews';
				$get_method			= 'getUserNews';
				break;

			default:
				$total_method		= 'getTotalNewsByText';
				$get_method			= 'searchNews';

				break;
		}

		$params[] 			= $this->text;
		$this->paginator	= $this->paginate( 'NewsModel', $total_method, $params, $href );
		$arguments[ 0 ]		= $this->paginator[ 'current_page' ];
		$news_list			= $this->getData( 'NewsModel', $get_method ,$arguments );

		return $news_list;
	}

}

?>
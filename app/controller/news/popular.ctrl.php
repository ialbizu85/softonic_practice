<?php
/**
 * popular.ctrl.php Class PopularNewsControler.
 *
 * @author meneame group.
 */

/**
 * PopularNewsController Class. Load most popular news.
 */
class PopularNewsController extends NewsNewsController
{
	/**
	 * Get the most populars news list to show in template
	 */
	protected function getNewsList( )
	{
		$href				= '/populares';
		$this->paginator	= $this->paginate( 'NewsModel', 'getTotalPopularNews', array( $this->month_votes ), $href );
		$arguments			= array(
									$this->paginator[ 'current_page' ],
									$this->elements_per_page,
									'',
									$this->month_votes
		);
		$news_list			= $this->getData( 'NewsModel', 'getPopularNews', $arguments, $this->ttl );
		return $news_list;
	}
}

?>
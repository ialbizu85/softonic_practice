<?php
/**
 * url.ctrl.php Class UrlNewsControler to upload a news.
 *
 * @author meneame group.
 */

/**
 * UrlNewsController Class. Valid news url to upload.
 */
class UrlNewsController extends ControllerLogged
{
	/**
	 * Instance of FormValidator
	 *
	 * @var FormValidator
	 */
	private $validator;

	/**
	 * Main method
	 */
	public function run( )
	{
		$post				= FilterPost::getInstance( );
		$session			= FilterSession::getInstance( );
		$this->validator	= new FormValidator( );

	    if ( !$this->isLogged( ) )
	    {
		    $redirect = new Redirect( );
			$redirect->changeLocation( '/user/login' );
	    }

		if ( $post->keyExist( 'news_url' ) )
		{
			$url		= $post->getText( 'news_url' );
			$is_active	= $this->validate( $url );

			if ( $is_active )
			{
				$session->setText( 'news_url', $url );
				$redirect = new Redirect( );
				$redirect->changeLocation( '/noticias/subir/publicar' );
			}
			else
			{
				$errors	= $this->validator->getErrors( );
				$this->template->assign( 'errors', $errors );
			}
		}

		$this->template->setTemplate( 'news/url' );
	}

	/**
	 * Validate form
	 *
	 * @param formValidator $validator
	 * @param string $url
	 * @return boolean
	 */
	private function validate( $url )
	{
		$url_check	= new UrlCheck( );

		$this->validator->setField( 'url', $url )->callback(
														array(
															$url_check,
															'isActive'
														),
														'Esta URL no existe o no está activa. La url debe empezar por http:// o https://'
		);

		$url_registered = $this->getData( 'NewsModel', 'isUrlRegistered', array( $url ) );
		$this->validator->isFalse( $url_registered, 'Esta noticia ya ha sido publicada.' );

		$is_valid = $this->validator->isFormValid( );

		return $is_valid;
	}
}

?>
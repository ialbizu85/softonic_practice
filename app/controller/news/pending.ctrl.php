<?php
/**
 * pending.ctrl.php Class PendingNewsControler.
 *
 * @author meneame group.
 */

/**
 * PendingNewsController Class. Load most popular news.
 */
class PendingNewsController extends NewsNewsController
{
	/**
	 * Get pending news list to show in template
	 */
	protected function getNewsList( )
	{
		$href				= '/pendientes';
		$this->paginator	= $this->paginate( 'NewsModel', 'getTotalPendingNews', array( $this->month_votes ), $href );
		$arguments			= array(
									$this->paginator[ 'current_page' ],
									$this->elements_per_page,
									'',
									$this->month_votes
		);
		$news_list			= $this->getData( 'NewsModel', 'getPendingNews', $arguments, $this->ttl );
		return $news_list;
	}
}

?>
<?php
/**
 * controllerlist.php Class FrontpageNewsControler.
 *
 * @author meneame group.
 */

/**
 * FrontpageNewsController Class. Load news that are going to be showed in front page.
 */
abstract class NewsNewsController extends ControllerList
{
	/**
	 * Min votes to show in front page
	 *
	 * @var int
	 */
	protected $day_votes;

	/**
	 * Min votes to show in populars
	 *
	 * @var int
	 */
	protected $month_votes;

	/**
	 * Tpl to use
	 *
	 * @var string
	 */
	protected $tpl;

	/**
	 * Class construct
	 *
	 * @param View $view
	 */
	public function  __construct( View $view )
	{
		require CORE__CONFIG_DIR . '/votes.config.php';

		$this->day_votes			= $__day_votes;
		$this->month_votes			= $__month_votes;
		$this->tpl					= 'news/news';

		parent::__construct( $view );
	}

	/**
	 * Main method
	 */
	public function run( )
	{
		$user_id	= FilterSession::getInstance( )->getNumber( 'user_id', 0 );
		$news_list	= $this->getNewsList( );
		$news_list	= $this->addVotedFlag( $user_id, $news_list );

		$this->template->assign( 'user_id', $user_id );
		$this->template->assign( 'news_list', $news_list );
		$this->template->assign( 'paginator', $this->paginator );
		$this->template->setTemplate( $this->tpl );
	}

	/**
	 * Get news list to show in template
	 */
	abstract protected function getNewsList( );
}

?>
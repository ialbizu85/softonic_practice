<?php
/**
 * vote.ctrl.php Class for add news vote
 */

/**
 * Class for add news vote
 *
 * @author meneame group.
 */
class VoteNewsController extends Controller
{
	/**
	 * Store type request
	 * 
	 * @var mixed
	 */
	protected $ajax_request;

	/**
	 * Store user identifier
	 *
	 * @var integer
	 */
	protected $user_id;

	/**
	 * Store news identifier
	 *
	 * @var integer
	 */
	protected $news_id;

	/**
	 * Main method
	 */
	public function run( )
	{
		$this->ajax_request	= FilterServer::getInstance( )->getText( 'HTTP_X_REQUESTED_WITH', false );
		$this->user_id		= FilterSession::getInstance( )->getNumber( 'user_id', false );
		$is_vote			= false;
		$votes				= 0;

		$is_valid = $this->isParamsValid( );

		if ( $is_valid )
		{
			$arguments = array(
							$this->news_id,
							$this->user_id
			);

			$is_vote = $this->getData( 'NewsModel', 'insertVote', $arguments );
			if ( $is_vote )
			{
				$this->cache->deleteByGroup( 'NewsModel', 'insertVote' );
				$key = $this->cache->generateKey( 'UserModel', 'getProfileInfoByUserName', $arguments );
				$this->cache->delete( $key );

				$votes = $this->getData( 'NewsModel', 'getNewsVotes', array( $this->news_id ) );
			}
		}

		$this->template->assign( 'message', $votes );
		$this->template->setTemplate( 'common/json' );		
	}

	/**
	 * Check that given params are valid.
	 * Return true if it are valid, false otherwise.
	 *
	 * @return boolean
	 */
	protected function isParamsValid( )
	{
		if ( !$this->ajax_request || !$this->user_id )
		{
			return false;
		}

		$this->news_id = FilterGet::getInstance( )->getNumber( 'news_id', false );

		if ( !$this->news_id )
		{
			return false;
		}

		$news_valid = $this->getData( 'NewsModel', 'isValid', array( $this->news_id ) );

		if ( !$news_valid )
		{
			return false;
		}

		return true;
	}
}

?>
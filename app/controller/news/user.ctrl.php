<?php
/**
 * user.ctrl.php Class UserNewsControler.
 *
 * @author meneame group.
 */

/**
 * UserNewsController Class. Load news that are going to be showed in front page.
 */
class UserNewsController extends NewsNewsController
{
	/**
	 * Get user's news list to show in template
	 */
	protected function getNewsList( )
	{
		$username			= FilterUri::getInstance( )->getText( 'username' );

		$this->tpl			= 'news/usernews';
		$this->template->assign( 'username', $username );

		$href				= '/perfil/' . $username . '/noticias';
		$this->paginator	= $this->paginate( 'NewsModel', 'getTotalUserNews', array( $username ), $href );
		$arguments			= array(
								$this->paginator[ 'current_page' ],
								$this->elements_per_page,
								'',
								$username
		);
		$news_list			= $this->getData( 'NewsModel', 'getUserNews', $arguments, $this->ttl );
		return $news_list;
	}
}

?>
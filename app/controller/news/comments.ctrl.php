<?php
/**
 * comments.ctrl.php Administrate news's comments.
 *
 * @author meneame group
 */

/**
 * CommentsNewsController class to get and write news's comments.
 */
class CommentsNewsController extends ControllerList
{
	/**
	 * FormValidator instance.
	 *
	 * @var FormValidator
	 */
	protected $validator;

	/**
	 * Token for validate sended form
	 *
	 * @var integer
	 */
	protected $form_token;

	/**
	 * Array with news parameter from database
	 *
	 * @var array
	 */
	protected $news;

	/**
	 * Array with $_POST superglobal values.
	 *
	 * @var array
	 */
	protected $post;

	/**
	 * Main method
	 */
	public function run( )
	{
		$this->validator	= new FormValidator( );
		$this->post			= FilterPost::getInstance( );
		$title_slug			= FilterUri::getInstance( )->getText( 'title_slug' );
		$this->form_token	= $this->getToken( );
		$this->news			= $this->getData( 'NewsModel', 'getNewsByTitleSlug', array( $title_slug ) );
		$user_id			= FilterSession::getInstance( )->getNumber( 'user_id' );

		list( $this->news ) = $this->addVotedFlag( $user_id, array( $this->news ) );

		if ( $this->isLogged( ) && $this->post->keyExist( 'comment' ) )
		{
			$is_valid = $this->validate( );

			$this->insertComment( $is_valid );
			$this->form_token = $this->generateToken( );
		}

		$this->showPage( );
	}

	/**
	 * Return avaible token in session
	 *
	 * @return integer
	 */
	protected function getToken( )
	{
		$form_token = FilterSession::getInstance( )->getText( 'form_token', false );

		if ( !$form_token )
		{
			$form_token = $this->generateToken( );
		}

		return $form_token;
	}

	/**
	 * Generate a new token and add to session
	 *
	 * @return integer
	 */
	protected function generateToken( )
	{
		$form_token = $this->getData( 'TokenModel', 'generateNumeric', array( ) );
		FilterSession::getInstance( )->setNumber( 'form_token', $form_token );

		return $form_token;
	}

	/**
	 * Insert new comment.
	 *
	 * @param boolena $is_valid
	 */
	public function insertComment( $is_valid )
	{
		if ( $is_valid )
		{
			$user_id	= FilterSession::getInstance( )->getNumber( 'user_id' );
			$arguments	= array(
								$this->news[ 'news_id' ],
								$this->post->getText( 'comment' ),
								$user_id
			);

			$insert		= $this->getData( 'CommentModel', 'insertComment', $arguments );

			if ( !$insert )
			{
				$error = 'No se ha podido guardar correctamente el comentario.';
				$this->template->assign( 'error', $error );
			}

			$this->cache->deleteByGroup( 'CommentModel', 'insertComment' );
		}
	}

	/**
	 * Validate form
	 *
	 * @return boolean
	 */
	public function validate( )
	{
		$this->validator->setField( 'comment', $this->post->getText( 'comment' ) )
						->required( 'El campo de comentario no puede estar vacio.' );

		$this->validator->setField( 'token', $this->post->getText( 'token' ) )
						->required( )
						->stringEqual( $this->form_token );

		$is_valid = $this->validator->isFormValid( );

		return $is_valid;
	}

	/**
	 * show template
	 */
	public function showPage( )
	{
		$errors		= array( 'news' => 'Esta noticia no existe.' );
		$user_id	= FilterSession::getInstance( )->getNumber( 'user_id', 0 );
		$href		= '/noticias/' . $this->news[ 'title_slug' ];

		if ( !!$this->news[ 'news_id' ] )
		{
			$paginator				= $this->paginate( 'CommentModel', 'getTotalNewsComments', array( $this->news[ 'news_id' ] ), $href );
			$comments_arguments		= array(
											$this->news[ 'news_id' ],
											$paginator[ 'current_page' ],
											$this->elements_per_page
			);
			$comments				= $this->getData( 'CommentModel', 'getNewsComments', $comments_arguments, $this->ttl );
			$errors					= $this->validator->getErrors( );

			$this->template->assign( 'news', $this->news );
			$this->template->assign( 'comments', $comments );
			$this->template->assign( 'token', $this->form_token );
			$this->template->assign( 'paginator', $paginator );
		}

		$this->template->assign( 'errors', $errors );
		$this->template->assign( 'user_id', $user_id );
		$this->template->setTemplate( 'news/comments' );
	}
}

?>
<?php
/**
 * frontpage.ctrl.php Class FrontpageNewsControler.
 *
 * @author meneame group.
 */

/**
 * FrontpageNewsController Class. Load news that are going to be showed in front page.
 */
class FrontpageNewsController extends NewsNewsController
{
	/**
	 * Get front page's news list to show in template
	 */
	protected function getNewsList( )
	{
		$href				= '';
		$this->paginator	= $this->paginate( 'NewsModel', 'getTotalFrontPageNews', array( $this->day_votes ), $href );
		$arguments			= array(
									$this->paginator[ 'current_page' ],
									$this->elements_per_page,
									'',
									$this->day_votes
		);
		$news_list			= $this->getData( 'NewsModel', 'getFrontPageNews', $arguments, $this->ttl );
		return $news_list;
	}
}

?>
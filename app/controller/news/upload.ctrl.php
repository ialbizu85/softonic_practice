<?php
/**
 * upload.ctrl.php Class UploadNewsControler..
 *
 * @author meneame group.
 */

/**
 * UploadNewsController Class. To Store de news.
 */
class UploadNewsController extends ControllerLogged
{
	/**
	 * Instance of FormValidator
	 *
	 * @var FormValidator
	 */
	private $validator;

	/**
	 * Main method
	 */
	public function run( )
	{
		$this->validator	= new FormValidator( );
		$post				= FilterPost::getInstance( );
		$session			= FilterSession::getInstance( );

		if ( $post->keyExist( 'title' ) )
		{
			$title		= $post->getText( 'title' );
			$summary	= $post->getText( 'summary' );

			$slugger 	= new Slugger( );
			$title_slug = $slugger->slug( $title );

			$is_valid	= $this->validate( $title, $summary, $title_slug );

			if ( $is_valid )
			{
				$url		= $session->getText( 'news_url' );
				$user_id	= $session->getNumber( 'user_id' );

				$arguments	= array(
								$title,
								$title_slug,
								$summary,
								$url,
								$user_id
				);

				$insert		= $this->getData( 'NewsModel', 'insertNews', $arguments );

				//Romper cache
				$username	= $this->getData( 'UserModel', 'getUsername', array( $user_id ) );
				$key		= $this->cache->generateKey( 'UserModel', 'getProfileInfoByUserName', array( $username ) );
				$this->cache->delete( $key );
				$this->cache->deleteByGroup( 'NewsModel', 'insertNews' );

				$message = 'La noticia ha sido subida correctamente.';

				if ( !$insert )
				{
					$message = 'No se ha podido subier la noticia, intentelo de nuevo.';
				}
				
				$this->template->assign( 'title_slug', $title_slug );
				$this->template->assign( 'message', $message );
			}
		}

		$errors = $this->validator->getErrors( );

		$this->template->assign( 'errors', $errors );
		$this->template->setTemplate( 'news/upload' );
	}

	/**
	 * Validate form
	 *
	 * @param string $title
	 * @param string $summary
	 * @return boolean
	 */
	private function validate( $title, $summary, $title_slug )
	{
		$is_registered = $this->getData( 'NewsModel', 'isRegisteredTitleSlug' , array( $title_slug ) );
		
		$this->validator->setField( 'title', $title )
					->required( 'Debe introducir un titulo para la noticia.' )
					->maxLength( 120, 'El titulo debe contener un máximo de 120 carácteres.' )
					->isFalse( $is_registered, 'El título que desea añadir ya existe.' );

		$this->validator->setField( 'summary', $summary )
					->required( 'Es obligatorio escribir un resumen de noticia.' )
					->maxLength( 400, 'El resumen de la noticia no debe superar los 400 carácteres.' );

		$is_valid = $this->validator->isFormValid( );

		return $is_valid;
	}
}

?>
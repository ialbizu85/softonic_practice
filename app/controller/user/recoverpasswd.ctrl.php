<?php
/**
 * RecoverPasswd.ctrl.php Controller
 *
 * @author meneame group
 */

/**
 * RecoverPasswd controls the recovering password template.
 */
class RecoverpasswdUserController extends ControllerLayout
{
	/**
	 * Error message that is used where username field is not filled.
	 *
	 * @var string
	 */
	protected $error_empty_username;

	/**
	 * Error message that is used where user mail field is not filled.
	 *
	 * @var string
	 */
	protected $error_empty_message;

	/**
	 * Seed used to encrypt user password.
	 *
	 * @var string.
	 */
	protected $seed;

	/**
	 * Instance of FormValidator
	 *
	 * @var FormValidator
	 */
	protected $validator;

	/**
	 * Load the config file to have access to the spore.
	 *
	 * @param View $view
	 */
	public function __construct( View $view )
	{
		require CORE__CONFIG_DIR . '/userpass.config.php';

		$this->seed					= $__seed;
		$this->validator			= new FormValidator( );
		$this->send_mail_message	= 'Se ha enviado un e-mail a la dirección de ' .
									'correo indicada. Debe seguir los pasos para ' .
									'poder crear una nueva contraseña.';
		$this->error_empty_email	= 'El campo Email es obligatorio.';
		$this->error_empty_username	= 'El campo Username es obligatorio.';

		parent::__construct( $view );
	}

	/**
	 * Main method
	 */
	public function run( )
	{
		$form = FilterPost::getInstance( );

		if ( $form->keyExist( 'submit' ) )
		{
			$username		=	$form->getText( 'username' );
			$email			=	$form->getText( 'email' );
			$user_id		=	$this->getData( 'UserModel', 'getUserId', array( $username ) );

			FilterSession::getInstance( )->setNumber( 'id', $user_id );

			$is_form_valid	=	$this->checkForm ( $username, $user_id, $email );

			if ( $is_form_valid )
			{
				$token		=	$this->insertToken ( $user_id, $username );
				$mail		=	new Mail( );
				$mail->sendTokenNewPasswd( $email, $token );

				$this->template->assign( 'message', $this->send_mail_message );
				$this->template->assign( 'is_sent', true );
			}
			else
			{
				$error_list = $this->validator->getErrors( );

				$this->template->assign( 'message', 'Error: hay campos incorrectos' );
				$this->template->assign( 'error_list', $error_list );
			}
		}

		$this->template->setTemplate( 'user/recoverpasswd' );
	}

	/**
	 * Validates the recovery password form.
	 *
	 * @param string $username
	 * @param string $user_id
	 * @param string $email
	 * @return object
	 */
	protected function checkForm( $username, $user_id, $email )
	{
		$this->validator->setField( 'username', $username )
						->required( $this->error_empty_username )
						->callback( array( $this, 'isRegistered' ),
									'No existe ningún usuario con ese nombre',
									array( $username ) );

		$this->validator->setField( 'email' , $email )
						->required( $this->error_empty_email )
						->email( )
						->callback( array( $this, 'checkMail' ),
									'El nombre de usuario no corresponde con el e-mail indicado',
									array( $user_id ) );

		return $this->validator->isFormValid( );
	}

	/**
	 * Check if email introduced by user match with mail stored in the database.
	 *
	 * @param int $user_id
	 * @param string $email
	 * @return boolean
	 */
	public function checkMail( $user_id, $email )
	{
		$stored_email	= $this->getData( 'UserModel', 'getMail', array( $user_id ) );
		$encrypter		= new Encrypter( );
		$stored_email	= $encrypter->decrypt( $stored_email );
		$is_same		= ( $email === $stored_email );

		return $is_same;
	}

	/**
	 * Generate and insert a token in the database
	 *
	 * @param string $username
	 * @param string $user_id
	 * @return string $token
	 */
	protected function insertToken ( $user_id, $username )
	{
		$this->getData( 'TokenModel', 'deleteToken', array( $user_id ) );

		$arguments	=	array(
							$username,
							$this->seed
		);
		$token		=	$this->getData( 'TokenModel', 'generateAlphanumeric', $arguments );
		$enc_token	=	hash_hmac( 'sha1', $token, $this->seed );
		$arguments	=	array( $enc_token, $user_id );
		$this->getData( 'TokenModel',  'setToken' , $arguments );

		return $token;
	}

	/**
	 * Check if username already exists
	 *
	 * @param string $username
	 * @return boolean
	 */
	public function isRegistered ( $username )
	{
		$is_registered = $this->getData('UserModel', 'isRegistered', array( $username ) );

		return $is_registered;
	}

}

?>
<?php
/**
 * resendvalidation.ctrl.php Login Controller
 *
 * @author meneame group
 */

/**
 * Resend validation controller
 */
class ResendvalidationUserController extends ControllerLayout
{
	/**
	 * The seed for the hash algorithm
	 *
	 * @var string
	 */
	protected $seed;

	/**
	 * Load the config file to have access to the seed.
	 *
	 * @param View $view
	 */
	public function __construct( View $view )
	{
		require CORE__CONFIG_DIR . '/userpass.config.php';

		$this->seed = $__seed;

		parent::__construct( $view );
	}

	/**
	 * Main method
	 */
	public function run( )
	{
		$user_id	= FilterSession::getInstance( )->getNumber ( 'id' );
		$username	= FilterSession::getInstance( )->getText ( 'u_name' );

		if ( !$user_id || !$username )
		{
			$redirect = new Redirect( );
			$redirect->changeLocation( '/login' );
		}

		FilterSession::getInstance( )->setNumber ( 'id', '' );
		FilterSession::getInstance( )->setText ( 'u_name', '' );

		$token		=	$this->insertToken( $user_id, $username );

		$email		= $this->getData( 'UserModel', 'getMail',  array( $user_id ) );

		$encrypter	= new Encrypter( );
		$email		= $encrypter->decrypt( $email );

		$mail		= new Mail( );
		$mail->sendValidationMail ( $username, $email, $token );

		$this->template->assign( 'email', $email );
		$this->template->setTemplate( 'user/validation' );
	}

	/**
	 * Generate and insert a token in the database
	 *
	 * @param string $username
	 * @param string $user_id
	 * @return string $token
	 */
	protected function insertToken ( $user_id, $username )
	{
		$this->getData( 'TokenModel', 'deleteToken', array( $user_id ) );

		$arguments	=	array(
							$username,
							$this->seed
		);

		$token		=	$this->getData( 'TokenModel', 'generateAlphanumeric', $arguments );
		$enc_token	=	hash_hmac( 'sha1', $token, $this->seed );
		$arguments	=	array( $enc_token, $user_id );
		$this->getData( 'TokenModel',  'setToken' , $arguments );

		return $token;
	}

}

?>
<?php
/**
 * ChangePassword.ctrl.php Controller
 */

/**
 * ChangePassword controls the change of password
 *
 * @author meneame group
 *
 */
class ChangepasswordUserController extends ControllerLayout
{
	/**
	 * Min user password length.
	 * @var int
	 */
	protected $min_pass_len;
	/**
	 * Regular expression for validating the password.
	 *
	 * @var string.
	 */
	protected $passwd_regex;

	/**
	 * Error message used for giving feedback to the user when the password doesn't have
	 * the minimum required length.
	 *
	 * @var string.
	 */
	protected $min_pass_len_error;

	/**
	 * Error message for the password.
	 *
	 * @var string.
	 */
	protected $passwd_error;

	/**
	 * Error message for the password not repeated.
	 *
	 * @var string.
	 */
	protected $passwd_repeated;

	/**
	 * The seed for the hash algorithm.
	 *
	 * @var string.
	 */
	protected $seed;

	/**
	 * Instance of FormValidator
	 *
	 * @var FormValidator
	 */
	protected $validator;

	/**
	 * Set the default parameters for the controller.
	 *
	 * @param View $view
	 */
	public function __construct( View $view )
	{
		require CORE__CONFIG_DIR . '/userpass.config.php';

		$this->validator			= new FormValidator( );
		$this->seed					= $__seed;
		$this->passwd_regex			= $__passwd_regex;
		$this->passwd_error			= $__passwd_error_msg;
		$this->min_pass_len			= $__min_pass_len;
		$this->min_pass_len_error	= $__min_pass_len_error;
		$this->passwd_repeated		= $__passwd_repeated_error;

		parent::__construct( $view );
	}

	/**
	 * Main method
	 */
	public function run( )
	{
		$post		= FilterPost::getInstance( );
		$user_id	= FilterSession::getInstance( )->getNumber( 'id' );

		if ( !$user_id && $this->isLogged( ) )
		{
			$username = FilterSession::getInstance( )->getText ( 'username' );
			$redirect = new Redirect( );
			$redirect->changeLocation( '/perfil/' . $username );
		}
		else if ( !$user_id && !$this->isLogged( ) )
		{
			$redirect = new Redirect( );
			$redirect->changeLocation( '/login' );
		}

		if ( $post->keyExist( 'change_passwd' ) )
		{
		 	$password		= $post->getText( 'password' );
			$password_rep	= $post->getText( 'password_rep' );

			$is_form_valid	=	$this->checkForm ( $password, $password_rep );

			if ( $is_form_valid )
			{
				$enc_password	= hash_hmac( 'sha1', $password, $this->seed );
				$this->getData( 'UserModel', 'setPasswd', array( $enc_password, $user_id ) );

				$this->template->assign( 'message', 'Contraseña cambiada con éxito.' );
				$this->template->assign( 'is_changed', true );
			}
			else
			{
				$error_list = $this->validator->getErrors( );

				$this->template->assign( 'message', 'Error: hay campos incorrectos' );
				$this->template->assign( 'error_list', $error_list );
			}
		}

		$this->template->assign( 'user_id', $user_id );
		$this->template->setTemplate( 'user/changepassword' );
	}

	/**
	 * Check that the data inserted in the form is valid
	 *
	 * @param string $passwd
	 * @param string $passwd_rep
	 * @return boolean
	 */
	protected function checkForm ( $passwd, $passwd_rep )
	{
		$this->validator->setField( 'password', $passwd )
						->required( )
						->minLength( $this->min_pass_len, $this->min_pass_len_error )
						->regex( $this->passwd_regex, $this->passwd_error )
						->stringEqual( $passwd_rep, $this->passwd_repeated );

		return $this->validator->isFormValid( );
	}
}

?>
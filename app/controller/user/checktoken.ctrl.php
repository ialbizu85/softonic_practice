<?php
/**
 * CheckToken.ctrl.php Controller
 *
 * @author meneame group
 */

/**
 * CkeckToken controls the recovering password template.
 */
class ChecktokenUserController extends ControllerLayout
{
	/**
	 * Seed used to encrypt user password.
	 *
	 * @var string.
	 */
	protected $seed;

	/**
	 * Error message in case the token is invalid.
	 *
	 * @var string.
	 */
	protected $message;

	/**
	 * Load the config file to have access to the spore.
	 *
	 * @param View $view
	 */
	public function __construct( View $view )
	{
		require CORE__CONFIG_DIR . '/userpass.config.php';

		$this->seed			= $__seed;
		$this->message		= 'Ha sucedido un error durante el proceso de cambio ' .
							  'de password: token incorrecto. Por favor vuelva a intentarlo';

		parent::__construct( $view );
	}

	/**
	 * Main method
	 */
	public function run( )
	{
		$token		= FilterGet::getInstance( )->getText( 'token' );
		$user_id	= FilterSession::getInstance( )->getNumber( 'id' );
		$is_valid	= $this->isValidToken( $token, $user_id );

		if ( $is_valid )
		{
			$this->getData( 'TokenModel', 'deleteToken', array( $user_id ) );

			FilterSession::getInstance( )->setText( 'id', $user_id );
			$redirect = new Redirect( );
			$redirect->changeLocation( '/usuario/cambiar-contrasena' );

		}

		$this->template->assign( 'message', $this->message );
		$this->template->setTemplate( 'user/checktoken' );
	}

	/**
	 * Check if the received token matches the one stored in database
	 *
	 * @param string $token
	 * @param integer $user_id
	 * @return boolean
	 */
	protected function isValidToken ( $token, $user_id )
	{
		$stored_token	= $this->getData( 'TokenModel', 'getToken', array( $user_id ) );
		$token			= hash_hmac( 'sha1', $token, $this->seed );
		$is_valid		= ( $token === $stored_token );

		return $is_valid;
	}
}
?>
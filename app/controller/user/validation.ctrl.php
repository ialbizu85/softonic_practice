<?php
/**
 * validation.ctrl.php Login Controller
 *
 * @author meneame group
 */


/**
 * Validation controller
 */
class ValidationUserController extends ControllerLayout
{
	/**
	 * The seed for the hash algorithm
	 *
	 * @var string
	 */
	protected $seed;

	/**
	 * Load the config file to have access to the seed.
	 *
	 * @param View $view
	 */
	public function __construct( View $view )
	{
		require CORE__CONFIG_DIR . '/userpass.config.php';
		$this->seed = $__seed;

		parent::__construct( $view );
	}
	
	/**
	 * Main method
	 */
	public function run( )
	{
		$user_id	= FilterSession::getInstance( )->getNumber('id');
		$token		= FilterGet::getInstance( )->getText( 'token' );
		
		$validation_correct = 'Tu cuenta ha sido veríficada con éxito';
		
		$validation_error	= 'Hay un problema con la validación de tu cuenta.' .
							 'Por favor vuelve a intentarlo.';
		$stored_token		= $this->getData( 'TokenModel', 'getToken', array( $user_id ) );
		$enc_token			= hash_hmac( 'sha1', $token, $this->seed );
		
		if ( $stored_token === $enc_token )
		{
			$this->getData( 'UserModel', 'setValidUserStatus', array( $user_id ) );
			$this->getData( 'TokenModel', 'deleteToken', array( $user_id ) );
			$this->template->assign( 'message', $validation_correct );
		}
		else
		{
			$this->template->assign( 'message', $validation_error );
		}
		$this->template->setTemplate( 'user/confirmvalidation' );
	}

}
?>
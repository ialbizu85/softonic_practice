<?php
/**
 * register.ctrl.php Register Controller
 *
 * @author meneame group
 */

/**
 * Register controller
 */
class RegisterUserController extends ControllerLayout
{
	/**
	 * Message to inform that the passwords don´t match
	 *
	 * @var string
	 */
	protected $passwd_repeated;

	/**
	 * Regular expression for validating the password
	 *
	 * @var string
	 */
	protected $passwd_regex;

	/**
	 * Error message for the password
	 *
	 * @var string
	 */
	protected $passwd_error;

	/**
	 * Regular expression for validating the username
	 *
	 * @var string
	 */
	protected $username_regex;

	/**
	 * Error message for the username
	 *
	 * @var string
	 */
	protected $username_error;

	/**
	 * Error message if the username already exists
	 *
	 * @var string
	 */
	protected $username_exist;

	/**
	 * Error message if the username already exists
	 *
	 * @var string
	 */
	protected $email_exist;

	/**
	 * The template that will be setted
	 *
	 * @var string
	 */
	protected $tpl =  'user/register';

	/**
	 * The seed for the hash algorithm
	 *
	 * @var string
	 */
	protected $seed;

	/**
	 * Instance of FormValidator
	 *
	 * @var FormValidator
	 */
	protected $validator;

	/**
	 * Load the config file to have access to the seed.
	 *
	 * @param View $view
	 */
	public function __construct( View $view )
	{
		require CORE__CONFIG_DIR . '/userpass.config.php';

		$this->seed				= $__seed;
		$this->validator		= new FormValidator( );
		$this->passwd_regex		= $__passwd_regex;
		$this->passwd_error		= $__passwd_error_msg;
		$this->username_error	= 'Username debe tener entre 3 y 6 carácteres alfanuméricos';
		$this->username_regex	= '/^([a-zA-Z0-9]){3,15}$/';
		$this->passwd_repeated	= 'Las contraseñas no coinciden';
		$this->username_exist	= 'Ya existe un usuario con ese username';
		$this->email_exist		= 'La dirección de correo ya está registrada';

		parent::__construct( $view );
	}

	/**
	 * Main method
	 */
	public function run( )
	{
		$form = FilterPost::getInstance( );

		if ( $form->keyExist( 'register' ) )
		{
			$username		=	$form->getText( 'username' );
			$password		=	$form->getText( 'password' );
			$password_rep	=	$form->getText( 'password_rep' );
			$email			=	$form->getText( 'email' );

			$is_form_valid	= $this->checkForm ( $username, $password, $password_rep, $email );

			if ( $is_form_valid )
			{
				$encrypter	=	new Encrypter( );
				$enc_email	=	$encrypter->encrypt( $email );
				$user_id	=	$this->insertUser( $username, $password, $enc_email );
				$token		=	$this->insertToken( $user_id, $username );

				$mail		= new Mail( );
				$mail->sendValidationMail ( $username, $email, $token );

				FilterSession::getInstance( )->setNumber( 'id', $user_id );

				$this->template->assign( 'email', $email );
				$this->tpl = 'user/validation';
			}
			else
			{
				$error_list = $this->validator->getErrors( );

				$this->template->assign( 'message', 'Error: hay campos incorrectos' );
				$this->template->assign( 'error_list', $error_list );
			}
		}

		$this->template->setTemplate( $this->tpl );
	}

	/**
	 * Check that the data inserted in the form is valid
	 *
	 * @param string $username
	 * @param string $passwd
	 * @param string $passwd_rep
	 * @param string $email
	 * @return boolean
	 */
	protected function checkForm ( $username, $passwd, $passwd_rep, $email )
	{
		$this->validator->setField( 'username', $username )
						->required( )
						->rangeLength( 3, 15 )
						->regex ( $this->username_regex, $this->username_error )
						->callback( array($this, 'isNotRegistered'),
								$this->username_exist );

		$this->validator->setField( 'email', $email )
						->required( )
						->email( )
						->callback( array($this, 'isNotRegisteredMail'),
								$this->email_exist );

		$this->validator->setField( 'password', $passwd )
						->required( )
						->minLength( 6 )
						->regex( $this->passwd_regex, $this->passwd_error )
						->stringEqual( $passwd_rep, $this->passwd_repeated );

		return $this->validator->isFormValid( );
	}

	/**
	 * Insert a user in the database and generate the token for his validation
	 *
	 * @param string $username
	 * @param string $passwd
	 * @param string $email
	 * @return string $user_id
	 */
	protected function insertUser ( $username, $passwd, $email )
	{
		$passwd		= hash_hmac( 'sha1', $passwd, $this->seed );
		$arguments	=	array(
							$username,
							$passwd,
							$email
		);
		$user_id	=	$this->getData( 'UserModel',  'setUser' , $arguments );

		return $user_id;
	}

	/**
	 * Generate an insert a token in the database
	 *
	 * @param string $user_id
	 * @param string $username
	 * @return string $token
	 */
	protected function insertToken ( $user_id, $username )
	{
		$arguments	=	array(
							$username,
							$this->seed
		);
		$token		=	$this->getData( 'TokenModel', 'generateAlphanumeric', $arguments );
		$enc_token	=	hash_hmac( 'sha1', $token, $this->seed );
		$arguments	=	array(
							$enc_token,
							$user_id
		);

		$this->getData( 'TokenModel',  'setToken' , $arguments );

		return $token;
	}

	/**
	 * Return if a username already exist in the database
	 *
	 * @param string $username
	 * @return boolean
	 */
	public function isNotRegistered ( $username )
	{
		$is_registered = $this->getData( 'UserModel',  'isRegistered' , array( $username ) );

		return !$is_registered;
	}

	/**
	 * Return if a email already exist in the database
	 *
	 * @param string $email
	 * @return boolean
	 */
	public function isNotRegisteredMail ( $email )
	{
		$encrypter		= new Encrypter( );
		$email			= $encrypter->encrypt( $email );
		$is_registered	= $this->getData( 'UserModel',  'isRegisteredMail' , array( $email ) );

		return !$is_registered;
	}
}

?>
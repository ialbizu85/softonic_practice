<?php
/**
 * Unsusbscribe.ctrl.php Defines UserModify controller.
 *
 * @author meneame group.
 */

/**
 *  Unsubscribe allows user to delete his account.
 */
class UnsubscribeUserController extends ControllerLogged
{
	/**
	 * Store the template name assigned by the controller.
	 *
	 * @var string
	 */
	protected $tpl = 'user/unsubscribe';


	/**
	 * The seed for the hash algorithm
	 *
	 * @var string
	 */
	protected $seed;

	/**
	 * Load the config file to have access to the seed.
	 *
	 * @param View $view
	 */
	public function __construct( View $view )
	{
		require CORE__CONFIG_DIR . '/userpass.config.php';

		$this->seed = $__seed;

		parent::__construct( $view );
	}

	/**
	 * Main method
	 *
	 * @see Controller::run( )
	 */
	public function run( )
	{
		$post = FilterPost::getInstance( );
		
		if ( $post->keyExist( 'unsubscribe_submit' ) )
		{
			$user_id			= FilterSession::getInstance( )->getNumber( 'user_id' );
			$password			=	hash_hmac( 'sha1', $post->getText( 'password' ), $this->seed );
			$stored_password	= $this->getData( 'UserModel', 'getPasswd', array( $user_id ) ); 
			
			if ( $password === $stored_password )
			{
				$this->getData( 'UserModel', 'unsubscribeUser', array( $user_id ) );

				$redirect	= new Redirect( );
				$redirect->changeLocation( '/logout' );
			}
			
			else
			{
				$this->template->assign( 'message', 'Error: Ha habido un problema al dar de baja al usuario.' .
										' Por favor, compruebe la contraseña proporcionada' );
			}
		}

		$this->template->setTemplate( $this->tpl );
	}
}
?>
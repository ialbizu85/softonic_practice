<?php
/**
 * login.ctrl.php Login Controller
 *
 * @author meneame group
 */

/**
 * Login controller
 */
class LoginUserController extends ControllerLayout
{
	/**
	 * The seed for the hash algorithm
	 *
	 * @var string
	 */
	protected $seed;

	/**
	 * Load the config file to have access to the seed.
	 *
	 * @param View $view
	 */
	public function __construct( View $view )
	{
		require CORE__CONFIG_DIR . '/userpass.config.php';

		$this->seed = $__seed;

		parent::__construct( $view );
	}


	/**
	 * Main method
	 */
	public function run( )
	{
		$post = FilterPost::getInstance( );

		if ( $post->keyExist( 'login' ) )
		{
			$password	=	hash_hmac( 'sha1', $post->getText( 'password' ), $this->seed );
			$identifier	=	$post->getText( 'identifier' );
			$is_email	=	strpos( $identifier, '@' );

			if ( $is_email )
			{
				$encrypter	=	new Encrypter( );
				$identifier	=	$encrypter->encrypt( $identifier );
			}

			$user			=	$this->getData( 'UserModel', 'getUserByCredential', array( $identifier ) );
			$is_registered	=	$user && ( $user['password'] == $password );

			if (  $is_registered && ( $user['status'] == 'ACTIVE' ) )
			{
				$username	=	$user['username'];
				$user_id	=	$user['user_id'];

				FilterSession::getInstance( )->setNumber( 'user_id', $user_id );
				FilterSession::getInstance( )->setText( 'username', $username );

				if ( $post->getNumber( 'remember', 0 ) )
				{
					$this->storeCookieLogin( $user_id );
				}

				$redirect			= new Redirect( );
				$redirect->changeLocation( '/' );
			}
			else if (  $is_registered && ( $user['status'] == 'PENDING' ) )
			{
				FilterSession::getInstance( )->setNumber( 'id', $user['user_id'] );
				FilterSession::getInstance( )->setText( 'u_name', $user['username'] );

				$this->template->assign( 'message', 'Error: Cuenta aun no activada.' );
				$this->template->assign( 'is_pending', true );
			}
			else
			{
				$this->template->assign( 'message', 'Error: Nombre de usuario o contraseña incorrectos.' );
			}

		}

		$this->template->setTemplate( 'user/login' );
	}
}

?>
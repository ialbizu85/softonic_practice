<?php
/**
 * Profile.ctrl.php Controller
 */

/**
 *  Profile class assigns user profile info to the template.
 *
 * @author meneame group
 *
 */
class ProfileUserController extends ControllerLayout
{
	/**
	 * Store the template name assigned by the controller.
	 *
	 * @var string
	 */
	protected $tpl = 'user/profile';

	/**
	 * Constructor method.
	 *
	 * @param View $view
	 */
	public function  __construct( View $view )
	{
		require CORE__CONFIG_DIR . '/cache.config.php';

		$this->ttl = $__ttl_profile;

		parent::__construct( $view );
	}

	/**
	 * Main method
	 *
	 * @see Controller::run( )
	 */
	public function run( )
	{
		$username = FilterUri::getInstance( )->getText( 'username' );

		if ( !$username )
		{
			$this->noAuthorized( );
		}

		$is_registered_user = $this->getData( 'UserModel', 'isRegistered', array( $username ) );

		if ( $is_registered_user )
		{
			$user_profile_info	= $this->getData( 'UserModel', 'getProfileInfoByUserName', array( $username ), $this->ttl );
			$this->template->assign( 'user_profile_info', $user_profile_info );

			$session_user = FilterSession::getInstance( )->getText( 'username' );
			
			if ( $session_user === $username )
			{
				$this->template->assign( 'self_user', true );
			}
		}
		else
		{
			$this->template->assign( 'error_message', 'No existe el usuario solicitado' );
		}

		$this->template->assign( 'username', $username );
		$this->template->setTemplate( $this->tpl );
	}
}
?>
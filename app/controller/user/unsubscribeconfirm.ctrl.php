<?php
/**
 * Unsubscribeconfirm.ctrl.php Defines Unsubscribeconfirm controller.
 *
 * @author meneame group.
 */

/**
 *  Unsubscribeconfirm controls set the template for the unsubscribe confirmation message.
 */
class UnsubscribeconfirmUserController extends ControllerLogged
{
	/**
	 * Store the template name assigned by the controller.
	 *
	 * @var string
	 */
	protected $tpl = 'user/unsubscribeconfirm';

	/**
	 * Main method
	 *
	 * @see Controller::run( )
	 */
	public function run( )
	{
		$message = '¿Está seguro que desea darse de baja?';

		$this->template->assign( 'message', $message );
		$this->template->setTemplate( $this->tpl );
	}
}
?>
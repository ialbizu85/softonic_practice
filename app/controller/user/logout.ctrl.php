<?php
/**
 * logout.ctrl.php Logout Controller
 *
 * @author meneame group
 */

/**
 * Logout controller
 */
class LogoutUserController extends ControllerLayout
{
	/**
	 * Main method
	 */
	public function run( )
	{
		FilterSession::getInstance( )->destroy( );

		$this->deleteRememberCookie( );

		$redirect = new Redirect( );
		$redirect->temporaryRedirect( '/' );
	}

	/**
	 * Delete active remember cookie for logout
	 */
	protected function deleteRememberCookie( )
	{
		require CORE__CONFIG_DIR . '/rememberpass.config.php';

		$cookie = FilterCookie::getInstance( )->getText( $_cookie_login, false );

		if ( $cookie )
		{
			$parts	= explode( ' ', $cookie );

			if ( 2 === sizeof( $parts ) )
			{
				list( $user_id, $token ) = $parts;
				
				$this->getData(
							'UserModel',
							'deleteCookieLogin',
							array(
								$user_id,
								$token
							)
				);

				FilterCookie::getInstance( )->setText(
													$_cookie_login,
													'',
													$expire = time( ) - $_expire_time,
													$path = null,
													$domain = null ,
													$secure = false,
													$httponly = true
				);
			}
		}
	}
}

?>
<?php
/**
 * UserModify.ctrl.php Defines UserModify controller.
 *
 * @author meneame group.
 */

/**
 *  UserModify controls the modification of user password by the user.
 */
class ModifyUserController extends ControllerLogged
{
	/**
	 * Error message that is used where username field is not filled.
	 */
	const ERROR_EMPTY_OLD_PASSWD = 'El campo contraseña antigua no puede estar vacio.';

	/**
	 * Error message that is used where user mail field is not filled.
	 */
	const ERROR_EMPTY_NEW_PASSWD = 'El campo nueva contraseña no puede estar vacio.';

	/**
	 * Store the template name assigned by the controller.
	 *
	 * @var string
	 */
	protected $tpl = 'user/modify';

	/**
	 * Spore used to encrypt user password.
	 *
	 * @var string.
	 */
	protected $seed;

	/**
	 * Min user password length.
	 *
	 * @var int
	 */
	protected $min_pass_len;

	/**
	 * Error message used for giving feedback to the user when the password doesn't have
	 * the minimum required length.
	 *
	 * @var string.
	 */
	protected $min_pass_len_error;

	/**
	 * Regular expression used for check if password have the correct format.
	 *
	 * @var string
	 */
	protected $passwd_regex;

	/**
	 * Error message for the password not repeated.
	 *
	 * @var string.
	 */
	protected $passwd_repeated;

	/**
	 * Error message used for giving some information to the user if password doesn't
	 * have the correct format.
	 *
	 * @var string
	 */
	protected $passwd_error_msg;

	/**
	 * Store the confirmation message that is showed in the view.
	 *
	 * @var string
	 */
	protected $message = '';

	/**
	 * Load the config file to have access to the seed and to the other config settings.
	 *
	 * @param View $view
	 */
	public function __construct( View $view )
	{
		require CORE__CONFIG_DIR . '/userpass.config.php';

		$this->seed				= $__seed;
		$this->min_pass_len		= $__min_pass_len;
		$this->passwd_regex		= $__passwd_regex;
		$this->passwd_error_msg	= $__passwd_error_msg;
		$this->min_pass_len_error	= $__min_pass_len_error;
		$this->passwd_repeated		= $__passwd_repeated_error;

		parent::__construct( $view );
	}

	/**
	 * Main method
	 *
	 * @see Controller::run( )
	 */
	public function run( )
	{
		$filter_post = FilterPost::getInstance( );

		if ( $filter_post->keyExist( 'modify_passwd' ) )
		{
			$is_valid			= false;

			$filter_session		= FilterSession::getInstance( );
			$user_id			= $filter_session->getNumber( 'user_id' );

			$old_passwd			= $filter_post->getText( 'old_passwd' );
			$new_passwd			= $filter_post->getText( 'new_passwd' );
			$rep_new_passwd		= $filter_post->getText( 'rep_new_passwd' );

			$form_validator		= $this->validateForm( $user_id, $old_passwd, $new_passwd, $rep_new_passwd );
			$is_valid			= $form_validator->isFormValid( );

			if ( $is_valid )
			{
				$is_passwd_changed = $this->setPasswd( $new_passwd, $user_id );
				$this->template->assign( 'is_passwd_changed', $is_passwd_changed );
			}
			else
			{
				$error_list		= $form_validator->getErrors( );
				$this->template->assign( 'error_list', $error_list );
			}
		}

		$this->template->assign( 'message', $this->message );
		$this->template->setTemplate( $this->tpl );
	}

	/**
	 * Try to store the new user password in the database and set the result message
	 * of the operation.
	 *
	 * @param string $new_passwd
	 * @param int $user_id
	 */
	protected function setPasswd( $new_passwd, $user_id )
	{
		$is_passwd_changed	= false;
		$new_passwd_encrypt = hash_hmac( 'sha1', $new_passwd, $this->seed );
		$status				= $this->getData( 'UserModel', 'setPasswd', array(
																			$new_passwd_encrypt,
																			$user_id
																			)
		);
		$this->message = 'No ha sido posible cambiar su contraseña.'
						. ' Por favor, intentelo de nuevo mas tarde.';

		if ( $status )
		{
			$is_passwd_changed	= true;
			$this->message		= 'Contraseña actualizada correctamente!';
		}

		return $is_passwd_changed;
	}

	/**
	 * Check if password stored in the database matches with the old password
	 * introduced by user.
	 *
	 * @param int $user_id
	 * @param string $form_old_passwd
	 * @return boolean
	 */
	public function checkPasswd( $user_id, $form_old_passwd )
	{
		$bd_passwd			= $this->getData( 'UserModel', 'getPasswd', array( $user_id ) );
		$form_old_passwd	= hash_hmac( 'sha1', $form_old_passwd, $this->seed );
		$is_same			= ( $bd_passwd === $form_old_passwd );

		return $is_same;
	}

	/**
	 * Validates the modify password form.
	 *
	 * @return boolean
	 */
	protected function validateForm( $user_id, $old_passwd, $new_passwd, $rep_new_passwd )
	{
		$form_validator = new FormValidator;

		$form_validator->setField( 'old_passwd', $old_passwd )
					->required( self::ERROR_EMPTY_OLD_PASSWD )
					->minLength( $this->min_pass_len, $this->min_pass_len_error )
					->regex( $this->passwd_regex, $this->passwd_error_msg )
					->callback( array(
									$this,
									'checkPasswd'
								),
								'La contraseña antigua introducida no se corresponde'
								. ' con la contraseña guardada en la base de datos!',
								array( $user_id )
		);

		$form_validator->setField( 'new_passwd', $new_passwd )
					->required( self::ERROR_EMPTY_NEW_PASSWD )
					->minLength( $this->min_pass_len, $this->min_pass_len_error )
					->regex( $this->passwd_regex, $this->passwd_error_msg )
					->stringEqual( $rep_new_passwd, $this->passwd_repeated );

		return $form_validator;
	}
}
?>
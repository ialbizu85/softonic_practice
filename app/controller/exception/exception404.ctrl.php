<?php
/**
 * execption404.ctrl.php define Exception404 controller 
 * 
 * @author meneame group
 */

/**
 * Class Exception404ExceptionController
 */
class Exception404ExceptionController extends Controller
{
    /**
     * Main method
     */
    public function run( )
    {
		header( 'HTTP/1.0 404 Not Found' );
		$pagina = FilterServer::getInstance( )->getText( 'REQUEST_URI' );
		$this->template->assign( 'pagina', $pagina );
    	$this->template->setTemplate( 'exception/exception404' );
    }
}

?>
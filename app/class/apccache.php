<?php
/**
 * apccache.php Class implements cache funtionalities with apc
 *
 * @author meneame group.
 */

/**
 * Class ApcCache to work with caches
 */
class ApcCache implements CacheAdapterInterface
{
	/**
	 * Store information.
	 *
	 * @param string $key. Key of the value
	 * @param mixed $value. Value to store
	 * @param int $ttl. Lifetime of the value in cache.
	 */
	public function store( $key, $value, $ttl )
	{
		apc_store( $key , $value, $ttl );
	}


	/**
	 * Return value of the key from cache.
	 *
	 * @param string $key
	 * @param mixed $default
	 * @return mixed
	 */
	public function fetch( $key, $default = false )
	{
		$value = apc_fetch( $key, $success );

		if ( $success )
		{
			return $value;
		}

		return $default;
	}


	/**
	 * Delete value from cache.
	 *
	 * @param string $key
	 */
	public function delete( $key )
	{
		apc_delete( $key );
	}
}

?>
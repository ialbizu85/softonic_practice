<?php
/**
 * mail.php Class for mail
 *
 * @author meneame group
 */

/**
 * Class mail
 */
class Mail
{
	/**
	 * The headers sended with de e-mails. Used to identify the sender of the e-mail.
	 *
	 * @var string.
	 */
	protected $headers = 'From: no-reply@meneame.project';
	
	/**
	 * Send an e-mail to the user with the new password.
	 * 
	 * @param string $user_mail E-mail address of the destinatary.
	 * @param string $new_user_passwd The new password selected by the user.
	 * @return boolean status Check if the mail has been sended successfully.
	 */
	public function sendRecoverPasswdMail( $user_mail, $new_user_passwd )
	{
		$subject = '[Meneame Project] Recuperación de contraseña';
		
		$message =  'Tu contraseña ha sido cambiada con éxito. Tu nueva ' .
					'contraseña es ' . $new_user_passwd;
		
		$status = mail( $user_mail, $subject, $message, $this->headers );
		
		return $status;
	}
	
	/**
	 * Send an e-mail to the user to validate the newly created account.
	 * 
	 * @param string $username Username of the destinatary user.
	 * @param string $user_mail E-mail address of the destinatary user.
	 * @param string $token Token used to validate the password.
	 * @return boolean status Check if the mail has been sended successfully.
	 */
	public function sendValidationMail( $username, $user_mail, $token )
	{
		$subject	= '[Meneame Project] Validación de cuenta';
		
		$message	=  'Te has registrado en meneame project con el nombre de usuario ' .
				$username . '. Para completar tu registro tienes confirmar tu ' . 
				'electrónico pinchando en el siguiente enlace o copiándolo en la ' .
				'barra de direcciones: ' . URL_DOMAIN . '/usuario/validar?token=' . $token;
		
		$status		= mail( $user_mail, $subject, $message, $this->headers );
		
		return $status;
	}
	
	/**
	 * Send an e-mail to the user to validate the newly created account.
	 * 
	 * @param type $user_mail E-mail address of the destinatary user .
	 * @param type $token Token used to validate the password.
	 * @return boolean status Check if the mail has been sended successfully.
	 */
	public function sendTokenNewPasswd( $user_mail, $token )
	{
		$subject	= '[Meneame Project] Recuperación de contraseña';
		
		$message	=  'Has solicitado recuperar tu contraseña. ' .
				'Para poder crear una nueva contraseña tienes que pinchar en el ' .
				'siguiente enlace o copiarlo en la barra de direcciones: ' . 
				URL_DOMAIN . '/usuario/checktoken?token=' . $token;
		
		$status		= mail( $user_mail, $subject, $message, $this->headers );
		
		return $status;
	}
	
}

?>
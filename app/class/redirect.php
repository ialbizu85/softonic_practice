<?php
/**
 * redirect.php Class for redirect client.
 *
 * @author meneame group.
 */

/**
 * Class redirect to work with redirect functionalities..
 */
class Redirect
{
	/**
	 * Realize a 301 Code for redirect client user to an other url
	 * and cache this redirect.
	 *
	 * @param string $url.
	 */
	public function permanentRedirect( $url )
	{
		header( 'HTTP/1.1 301 Moved Permanently' );
		$this->changeLocation( $url );
	}

	/**
	* Realize a 307 Code for redirect client user to an other url.
	*
	* @param string $url.
	*/
	public function temporaryRedirect( $url )
	{
		header( 'HTTP/1.1 307 Temporary Redirect' );
		$this->changeLocation( $url );
	}

	/**
	 * Redirect client to an other location.
	 *
	 * @param string $url.
	 */
	public function changeLocation( $url )
	{
		header( 'Location: ' . URL_DOMAIN . $url );
		die( );
	}
}

?>
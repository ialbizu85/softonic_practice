<?php
/**
 * slugger.php Class for create slugs from strings.
 *
 * @author meneame group.
 */

/**
 * Class for create slugs from strings.
 *
 * @author meneame group.
 */
class Slugger {

	/**
	 * Given a string, return string slugged for be passed by uri.
	 *
	 * @param string $chain.
	 * @return string.
	 */
	public function slug( $chain )
	{
		$slug_chain = iconv( 'UTF-8', 'ASCII//TRANSLIT', $chain );
		$slug_chain = preg_replace( '/[^a-zA-Z0-9]/', '-' , $slug_chain );
		$slug_chain = preg_replace( '/--+/', '-' , $slug_chain );
		$slug_chain = trim( $slug_chain, '-' );

		return $slug_chain;
	}
}

?>
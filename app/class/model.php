<?php
/**
 * Model structure
 *
 * @author meneame group.
 */

/**
 * Class Model
 * 
 */
class Model
{
	/**
	 * DB resource
	 * 
	 * @var resource
	 */
	private $db;
	
	/**
	 * Create a connections to a specific profile
	 * 
	 * @param string $profile DB profile
	 */
	public function connectDb( $profile )
	{
		$this->db = MysqlDatabase::getInstance( $profile );
	}
	
	/**
	 * Not available magic methods
	 * 
	 * @param string $name
	 * @return mixed 
	 */
	public function __get($name)
	{
		return $this->db;
	}
	
	/**
	 * Not available magic methods
	 * 
	 * @param string $name
	 * @param mixed $value 
	 */
	public function __set($name, $value) 
	{
		throw new Exception( 'The property ' . $name . ' does not exist or it\'s not available' );
	}
}

?>
<?php
/**
 * MySQL Database access
 *
 * @author meneame group.
 */

/**
 * Class Database.
 *
 * Core Database class.
 */
class MysqlDatabase extends Mysqli implements DatabaseInterface
{
	/**
	 * Contains instance of database object.
	 *
	 * @var object $instance
	 */
	private static $instance;

	/**
	 * Construct method for initializing methods and variables.
	 *
	 * @param string $profile Name of the profile configuration to load.
	 * @param string $charset Charset name.
	 */
	private function __construct( $profile, $charset = 'utf-8' )
	{
		require CORE__CONFIG_DIR . '/db.config.php';
		if ( !isset( $_profile_db[ $profile ] ) )
		{
			throw new MysqlDatabaseException( 'The profile "' . $profile . '" does not exists');
		}

		parent::__construct( $_profile_db[ $profile ]['host']
				, $_profile_db[ $profile ]['user']
				, $_profile_db[ $profile ]['password']
				, $_profile_db[ $profile ]['dbname']
		);

		$error = $this->connect_error;
		if ( $error )
		{
			throw new MysqlDatabaseException( 'Could not connect to database: '
				. $this->connect_errno . '-' . $error );
		}

		mysqli_report( MYSQLI_REPORT_ERROR );
		$this->setCharset( $charset );
	}

	/**
	 * Gets a singleton object from the database class.
	 *
	 * @param string $profile Name of the profile configuration to load.
	 * @return bool Returns true if everything is ok, false otherwise.
	 */
	public static function getInstance( $profile )
	{
		if ( empty( self::$instance[ $profile ] ) )
		{
			self::$instance[ $profile ] = new self( $profile );
		}

		return self::$instance[ $profile ];
	}

	/**
	 * Makes a query to the database, and if it returns a resultset, fetches it and returns an array.
	 *
	 * @param string $sql Sql statement to query.
	 * @return mixed Returns true if no resulset was returned from the query.
	 * If a resultset was returned, returns the fetched resultset array. If
	 * something went wrong, returns false.
	 */
	public function query( $sql )
	{
		$result_set = parent::query( $sql );
		if ( !$result_set ) return false;

		$result	= array( );
		
		if ( 0 == $this->field_count )
		{
			return true;
		}
		
		while ( $array = $result_set->fetch_assoc( ) )
		{
			$result[] = $array;
		}

		return $result;
	}


	/**
	* Sets the client connection charset.
	*
	* @param string $charset Charset.
	* @return bool Returns true if everything went ok, false otherwise.
	*/
	public function setCharset( $charset )
	{
		parent::set_charset( $charset );
	}

	/**
	* Ends the current database connection.
	*
	* @return void
	*/
	public function __destruct( )
	{
		parent::close( );
	}
}

/**
 * MysqlDatabaseException class
 *
 * Database Exception class for the MysqlDatabase class
 */
class MysqlDatabaseException extends Exception
{

}

?>
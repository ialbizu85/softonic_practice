<?php
/**
 * filterget.php Class to work with $_GET global variable.
 *
 * @author meneame group
 */

/**
 * Class to work with $_GET global variable.
 *
 * @version 1.0
 * @author meneame group.
 * @package Filter.
 * @subpackage FilterGet.
 */
class FilterGet extends Filter
{
	/**
	 * Instance of the object.
	 *
	 * @static
	 * @access private.
	 * @var FilterGet.
	 */
	private static $instance = null;
	
	/**
	 * Initialize the arrays to clean and the allowed tags.
	 * 
	 * @return FilterGet.
	 */
	private function __construct( )
	{
		$this->data = $_GET;
		
		$_GET		= array( );
	}
	
	/**
	 * Return an unique instance each time that we acces to the $_GET variable.
	 * 
	 * @static
	 * @return FilterGet.
	 */
	public static function getInstance( )
	{
		if ( null === self::$instance )
		{
			self::$instance = new self( );
		}
		
		return self::$instance;
	}
}

?>
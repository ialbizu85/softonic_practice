<?php
/**
 * autoload.php Contains autoload definition.
 *
 * @author meneame group.
 */

/**
 * Allow load automatically Controllers, Models and Classes.
 *
 * @param string $class_name
 *
 * Example of valid parameters:
 *		IndexHomeController
 *		UserModel
 *		Dispatcher
 */
function framework_autoload( $class_name )
{
	preg_match( '/^([A-Z][a-z0-9]+)([A-Z][a-z0-9]+)?(Controller|Model|Interface)/', $class_name, $matches );

	$objet_type = ( isset( $matches[ 3 ] ) ) ? $matches[ 3 ] : '';
	switch ( $objet_type )
	{
		case 'Controller':
			$filename = CORE__CONTROLLER_PATH . '/' . strtolower( $matches[ 2 ] )  . '/' . strtolower( $matches[ 1 ] ) . '.ctrl.php';
			break;

		case 'Model':
			$filename = CORE__MODEL_PATH . '/' . strtolower( $matches[ 1 ] ) . '.model.php';
			break;

		case 'Interface':
			$filename = CORE__CLASSES_DIR . '/' . strtolower( $matches[ 1 ] ) . strtolower( $matches[ 2 ] ) . '.iface.php';
			break;

		default:
			$filename = CORE__CLASSES_DIR . '/' . strtolower( $class_name ) . '.php';
			if ( !file_exists( $filename ) )
			{
				return ;
			}
	}

	require_once $filename;
}

?>
<?php
/**
 * list.ctrl.php Class ListNewsControler.
 *
 * @author meneame group.
 */

/**
 * ListNewsController Class. Define list controller
 */
abstract class ControllerList extends ControllerLayout
{
	/**
	 * Elements to show in a page.
	 *
	 * @var int
	 */
	protected $elements_per_page;

	/**
	 * Page to show in paginator before and after of current page.
	 *
	 * @var int
	 */
	protected $page_per_range;

	/**
	 * Page range to show in paginator of template.
	 *
	 * @var array
	 */
	protected $paginator;

	/**
	 * Tpl to use
	 *
	 * @var string
	 */
	protected $tpl;

	/**
	 * Cache ttl value
	 *
	 * @var int
	 */
	protected $ttl;

	/**
	 * Class construct
	 *
	 * @param View $view
	 */
	public function  __construct( View $view )
	{
		require CORE__CONFIG_DIR . '/paginator.config.php';
		require CORE__CONFIG_DIR . '/cache.config.php';

		$this->elements_per_page	= $__elements_per_page;
		$this->page_per_range		= $__page_per_range;
		$this->ttl					= $__ttl_news_list;
		parent::__construct( $view );
	}

	/**
	 * Get parameter from paginator model.
	 * 
	 * @return array
	 */
	protected function paginate( $model, $method, $params, $href )
	{
		$paginator				= array( );
		$current_page			= FilterUri::getInstance( )->getNumber( 'page', false );
		$news_number			= $this->getData( $model, $method, $params );

		$arguments				= array(
									$news_number,
									$this->elements_per_page
		);
		$total_pages			= $this->getData( 'PaginatorModel', 'getTotalPages', $arguments );

		$this->checkPage( $current_page, $total_pages, $href );

		if ( !$current_page )
		{
			$current_page = 1;
		}

		$paginator_arguments	= array(
										$current_page,
										$total_pages,
										$this->page_per_range,
		);
		$paginator				= $this->getData( 'PaginatorModel', 'getPagesRange', $paginator_arguments );
		$paginator[ 'href' ]	= $href;

		return $paginator;
	}

	/**
	 * Check if comment page exists
	 * 
	 * @param int $current_page
	 * @param int $total_pages
	 * @param string $href
	 * @return boolean
	 */
	protected function checkPage( $current_page, $total_pages, $href )
	{
		$redirect	= new Redirect( );

		if ( false !== $current_page )
		{
			if ( $current_page <= 1 )
			{
				$redirect->permanentRedirect( $href );
			}

			if ( $current_page > $total_pages )
			{
				$redirect->temporaryRedirect( $href . '/' . $total_pages );
			}
		}

		return true;
	}

	/**
	 * Add field is voted to news_list.
	 * Store if user voted each news.
	 *
	 * @param array $news_list
	 * @return array
	 */
	protected function addVotedFlag( $user_id, $news_list )
	{
		$news_ids = array( );
		foreach ( $news_list AS $value )
		{
			$news_ids[] = $value[ 'news_id' ];
		}

		$voted_news = $this->getData(
									'NewsModel',
									'getVotedNews',
									array(
										$user_id,
										$news_ids
									)
		);

		foreach ( $news_list AS $key => $value )
		{
			$is_voted = false;
			if ( array_key_exists( $value[ 'news_id' ] , $voted_news ) )
			{
				$is_voted = true;
			}

			$news_list[ $key ][ 'is_voted' ] = $is_voted;
		}

		return $news_list;
	}
}

?>
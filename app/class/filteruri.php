<?php
/**
 * filteruri.php Provides filter functionalities over URI data
 *
 * @author meneame group
 */

/**
 * Provides filter functionalities over URI data
 */
class FilterUri extends Filter
{
	/**
	 * Current instance
	 * @var FilterUri
	 */
	private static $_instance = null;

	/**
	 * Private construct for Singleton
	 */
	private function __construct( $param )
	{
		$this->data = $param;
	}

	/**
	 * Private clone for Singleton
	 */
	private function __clone( )
	{
	}

	/**
	 * Returns current instance
	 * 
	 * @param array $params
	 * @return FilterUri
	 */
	public static function getInstance( $params = array( ) )
	{
		if ( null === self::$_instance )
		{
			self::$_instance = new self( $params );
		}
		return self::$_instance;
	}

}

?>
<?php
/**
 * filtersession.php Class to work with $_SESSION global variable.
 *
 * @author meneame group
 */

/**
 * Class to work with $_SESSION global variable.
 *
 * @version 1.0
 * @author meneame group.
 * @package Filter.
 * @subpackage FilterSession.
 */
class FilterSession extends Filter
{
	/**
 	 * Instance of the object.
	 *
	 * @static
	 * @access private.
	 * @var FilterSession.
	 */
	private static $instance = null;

	/**
	 * Initialize the arrays and the allowed tags to clean.
	 *
	 * @return FilterSession.
	 */
	private function __construct( )
	{
            session_start( );
            $this->data = $_SESSION;

            $_SESSION	= array( );
	}

	/**
	 * Load the data array into the superglobal $_SESSION.
	 */
	public function  __destruct( )
	{
		$_SESSION = $this->data;
	}


	/**
	 * Return an unique instance each time that we try to acces to $_SESSION variable.
	 *
	 * @static
	 * @return FilterSession.
	 */
	public static function getInstance( )
	{
		if ( null === self::$instance )
		{
			self::$instance = new self( );
		}

		return self::$instance;
	}


	/**
	 * Write a value with the corresponding key.
	 *
	 * @param string $key.
	 * @param string $value.
	 */
	public function setText( $key, $value )
	{
		if ( is_string( $value ) )
		{
			$this->data[ $key ] = $value;
		}
	}


	/**
	 * Write a numeric value with corresponding key.
	 * 
	 * @param string $key.
	 * @param int $value.
	 */
	public function setNumber( $key, $value )
	{
		if ( is_numeric( $value ) )
		{
			$this->data[ $key ] = $value;
		}
	}

	/**
	 * Delete all session information
	 */
	public function destroy( )
	{
		$this->data = array( );
	}
}

?>
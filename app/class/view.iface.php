<?php
/**
 * viewengine.iface.php
 */

/**
 * Interface for template engines in view layer
 *
 * @author meneame group.
 */
interface ViewInterface
{
	/**
	 * Render fiven template setting all properties and return
	 * formatted result.
	 *
	 * @param string $template_path
	 * @param array $properties
	 *
	 * @return string
	 */
	public function render( $template_path, $properties );
}
<?php
/**
 * encrypter.php Class for mail.
 *
 * @author meneame group.
 */

/**
 * Class encrypter.
 */
class Encrypter
{
	/**
	 * The seed for the encrypter algorithm.
	 *
	 * @var string
	 */
	protected $key = 'cQiox(8<GHL';
	
	/**
	 * The cipher to encrypt/decrypt.
	 *
	 * @var mixed
	 */
	protected $cipher;

	/**
	 * The iv for the encryption algorithm.
	 *
	 * @var string.
	 */
	protected $iv = '9a9df186';

	/**
	 * Select the blowfish encrypter algorithm in mode cbc and generate the iv
	 * through the key.
	 */
	public function  __construct( )
	{
		$this->cipher = mcrypt_module_open( MCRYPT_BLOWFISH, '', 'cbc', '' );
	}

	/**
	 * Encrypt the input string with the selected algorithm.
	 *
	 * @param string $string The string to encrypt.
	 * @return string The string encrypted.
	 */
	public function encrypt ( $string )
	{
		if ( empty ( $string ) )
		{
			return false;
		}

		mcrypt_generic_init( $this->cipher, $this->key, $this->iv );
		$encrypted = mcrypt_generic( $this->cipher, $string );

		mcrypt_generic_deinit( $this->cipher );
		$encrypted = chop ( base64_encode ( $encrypted ) );
		
		return $encrypted;
	}

	/**
	 * Decrypt the encrypted string with the selected algorithm.
	 *
	 * @param string $string The string to decrypt.
	 * @return string The string decrypted.
	 */
	public function decrypt ( $string )
	{
		if ( empty ( $string ) )
		{
			return false;
		}

		mcrypt_generic_init ( $this->cipher, $this->key, $this->iv );
		$decrypted = mdecrypt_generic ( $this->cipher, base64_decode ( $string ) );
		
		mcrypt_generic_deinit ( $this->cipher );
		$decrypted = chop ( $decrypted );

		return $decrypted;
	}
}

?>
<?php
/**
 * controller.php is main controller class.
 *
 * @author meneame group.
 */

/**
 * Controller class is  the main controller.
 */
abstract class Controller
{
	/**
	 * Instance of specific Template.
	 *
	 * @var View
	 */
	protected $template;

	/**
	 * Cache instance.
	 *
	 * @var CacheStrategyInterface
	 */
	protected $cache = null;

	/**
	 * Functionalities over Uri.
	 *
	 * @var FilterUri
	 */
	protected $uri;

	/**
	 * Default cache ttl value.
	 *
	 * @var int.
	 */
	protected $ttl = 0;

     /**
	 * Initialize the constructor.
	 *
	 * @param View $view
	 */
	public function __construct( View $view )
	{
		$this->template = $view;
		$this->uri		= FilterUri::getInstance( );

		if ( !$this->isLogged( ) )
		{
			$this->tryCookieLogin( );
		}
	}

	/**
	 * Render template.
	 */
	public function render( )
	{
		$this->template->render( );
	}

	/**
	 * Main method.
	 */
	abstract public function run( );

	/**
	 * Invoke a method from a specific model and get data and seve it in cache.
	 *
	 * @param string $model name
	 * @param string $method name
	 * @param array $arguments of method
	 * @return mixed data result
	 */
	protected function getData( $model, $method, $arguments, $ttl = -1 )
	{
		$cache_obj = CacheGetData::getInstance( );

		if ( 0 <= $ttl )
		{
			$key = $this->cache->generateKey( $model, $method, $arguments );

			//Check in cache.
			$result = $this->cache->fetch( $key );
			
			if ( !$result )
			{
				//Execute the sentence.
				$result	= $cache_obj->get( $model, $method, $arguments );

				//Store in cache.
				$this->cache->store( $key, $result, $ttl );
			}

			return $result;
		}

		//Execute the sentence.
		return $cache_obj->get( $model, $method, $arguments );
	}


	/**
	 * Set cache a CacheStrategyInterface instance.
	 *
	 * @param CacheStrategyInterface $cache.
	 */
	public function setCache( CacheStrategyInterface $cache )
	{
		$this->cache = $cache;
	}

	/**
	 * Throw an exception because user isn´t authorized.
	 */
	protected function noAuthorized( )
	{
		throw new Exception404( 'Not Found' );
	}


	/**
	 * Check if user is logged.
	 *
	 * @return boolean
	 */
	protected function isLogged( )
	{
		return FilterSession::getInstance( )->keyExist( 'user_id' );
	}

	/**
	 * Try do a login through cookie login
	 */
	private function tryCookieLogin( )
	{
		require CORE__CONFIG_DIR . '/rememberpass.config.php';

		$cookie = FilterCookie::getInstance( );
		$token 	= $cookie->getText( $_cookie_login, false );

		if ( $token )
		{
			$cookie->removeCookie( $_cookie_login );
			$parts = explode( ' ', $token );

			if ( 2 != sizeof( $parts ) )
			{
				return false;
			}

			list( $user_id, $token ) 	= $parts;
			$is_deleted 				= $this->deleteCookieLogin( $user_id, $token );

			if ( $is_deleted )
			{
				$username = $this->getData( 'UserModel', 'getUsername', array( $user_id ) );
				FilterSession::getInstance( )->setText( 'user_id', $user_id );
				FilterSession::getInstance( )->setText( 'username', $username );

				$this->storeCookieLogin( $user_id );

				return true;
			}

			$this->getData( 'UserModel', 'deleteAllCookiesLogin', array( $user_id ) );
		}
	}

	/**
	 * Sened a cookie too client and register it to database
	 *
	 * @param integer $user_id
	 */
	protected function storeCookieLogin( $user_id )
	{
		require CORE__CONFIG_DIR . '/rememberpass.config.php';
		$token = $this->getData( 'TokenModel', 'generateNumeric', array( ) );

		FilterCookie::getInstance( )->setText(
											$_cookie_login,
											$user_id . ' ' . $token,
											$expire = time( ) + $_expire_time,
											$path = null,
											$domain = null ,
											$secure = false,
											$httponly = true
		);

		$this->getData( 'UserModel', 'addCookieLogin', array( $user_id, $token ) );
	}

	/**
	 * Delete cookie login and return if it was successful (true), false
	 * otherwise
	 *
	 * @param integer 	$user_id
	 * @param string 	$token
	 * @return boolean
	 */
	private function deleteCookieLogin( $user_id, $token )
	{
		$is_deleted = $this->getData(
									'UserModel',
									'deleteCookieLogin',
									array(
										$user_id,
										$token
									)
		);

		return $is_deleted;
	}
}

?>
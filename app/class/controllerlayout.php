<?php
/**
 * controller.php class test that user is loged.
 *
 * @author meneame group.
 */

/**
 * Controller class test that user is loged.
 */
abstract class ControllerLayout extends Controller
{
	/**
	 * Initialize controller.
	 *
	 * @param View $view.
	 */
	public function  __construct( View $view )
	{
		parent::__construct( $view );

		$this->setDataLayout( );
	}

	/**
	 * Set data for current layout.
	 */
	protected function setDataLayout( )
	{
		$session			= FilterSession::getInstance( );

		$user[ 'user_id' ]	= $session->getText( 'user_id', false );
		$user[ 'username' ]	= $session->getText( 'username', false );

		if ( $user[ 'user_id' ] )
		{
			$user[ 'is_logged' ] = true;
			
			$this->template->assign( 'layout', $user );
		}
	}
}

?>
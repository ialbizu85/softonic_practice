<?php
/**
 * cache.iface.php interface for cache functions.
 * 
 * @author meneame group.
 */

/**
 * Cache interface to define cache functions.
 */
interface CacheStrategyInterface
{
	/**
	 * Store value with its key in cache.
	 *
	 * @param string $value.
	 * @param string $key. Cache key
	 * @param int $ttl.
	 */
	public function store( $key, $value, $ttl );

	/**
	 * Return value of the key from cache.
	 *
	 * @param string $key. Cache key.
	 */
	public function fetch( $key );

	/**
	 * Delete value from cache.
	 *
	 * @param string $key. Cache key.
	 */
	public function delete( $key );
	
	/**
	 * Delete all key from group with a relationship with [ $model ][ $method ].
	 *
	 * @param string $model.
	 * @param string $method.
	 */
	public function deleteByGroup( $model, $method );
}

?>
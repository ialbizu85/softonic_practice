<?php
/**
 * cachegetdata.php class that saves data from controller getData method.
 *
 * @author meneame group.
 */

/**
 * Class CacheGetData. Store getData data.
 */
class CacheGetData
{
	/**
	 * It saves an Instance.
	 *
	 * @var objet CacheGetData.
	 */
	private static  $instance = null;

	/**
	 * Storage to getData data.
	 *
	 * @var array saves getData info
	 */
	protected $cache_data = array( );

	/**
	 * Private construct for singleton
	 */
	private function  __construct( ) { }

	/**
	 * Private clone for singletone
	 */
	private function  __clone( ) { }

	/**
	 * get instance of the object.
	 *
	 * @return object CacheGetData
	 */
	public static function  getInstance( )
	{
		if ( null === self::$instance )
		{
			self::$instance = new self;
		}
		return self::$instance;
	}

	/**
	 * Checks if exist key
	 *
	 * @param string $model name.
	 * @param string $method name.
	 * @param array $arguments of method.
	 * @return mixed if it exists return array result else return NULL.
	 */
	public function get( $model, $method, $arguments = array( ) )
	{
		$get_data_key = $model . $method;

		$get_data_key = $get_data_key . serialize( $arguments );
		
		if ( !array_key_exists( $get_data_key , $this->cache_data ) )
		{
			$object = new $model( );
			$result = call_user_func_array(
											array(
												$object,
												$method
											),
											$arguments
			);
                        
			$this->set(
						$model,
						$method,
						$arguments,
						$result
			);
		}

		return $this->cache_data[ $get_data_key ];
	}

	/**
	 * set data in cache_data.
	 *
	 * @param string $model name.
	 * @param dtring $method name.
	 * @param array $arguments of method.
	 * @param mixed $result data will be saved.
	 */
	protected function set( $model, $method, $arguments, $result )
	{
		$get_data_key                       = $model . $method;
		$get_data_key                       = $get_data_key . serialize( $arguments );
		$this->cache_data[ $get_data_key ]  = $result;
	}
}

?>
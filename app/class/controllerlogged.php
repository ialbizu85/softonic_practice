<?php
/**
 * controller.php class test that user is loged.
 *
 * @author meneame group.
 */

/**
 * Controller class test that user is loged.
 */
abstract class ControllerLogged extends ControllerLayout
{
	/**
	 * Initialize object.
	 */
	public function __construct( View $view )
	{
		if ( !$this->isLogged( ) )
 		{
 			$redirect = new Redirect( );
			$redirect->temporaryRedirect( '/login' );
 		}

		parent::__construct( $view );
	}
}

?>
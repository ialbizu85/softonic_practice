<?php
/**
 * filtercookie.php Class for work with superglobal $_COOKIE
 */

/**
 * Class for work with superglobal $_COOKIE
 *
 * @author jose
 */
class FilterCookie extends Filter
{
	/**
 	 * Unique instance
 	 *
	 * @var FilterCookie
	 */
	private static $instance = null;

	/**
	 * Initializate object
	 *
	 * @return FilterCookie
	 */
	private function __construct( )
	{
		$this->data = $_COOKIE;

		$_COOKIE = array( );
	}

	/**
	 * Return a instance of FilterCookie
	 *
	 * @return FilterCookie
	 */
	public static function getInstance( )
	{
		if ( null === self::$instance )
		{
			self::$instance = new self( );
		}

		return self::$instance;
	}


	/**
	 * Set a new text for cookie
	 *
	 * @param string $key
	 * @param string $value
	 */
	public function setText( $key, $value, $expire = 0, $path = '/', $domain = URL_DOMAIN , $secure = false, $httponly = false )
	{
		if ( !empty( $key ) && is_string( $value ) )
		{
			setcookie( $key, $value, $expire, $path, $domain, $secure, $httponly );
			$this->data[ $key ] = $value;
		}
	}


	/**
	 * Set a new number for cookie
	 *
	 * @param string $key
	 * @param int $value
	 */
	public function setNumber( $key, $value, $expire = 0, $path = '/', $domain = URL_DOMAIN , $secure = false, $httponly = false )
	{
		if ( !empty( $key ) && is_numeric( $value ) )
		{
			setcookie( $key, $value, $expire, $path, $domain, $secure, $httponly );
			$this->data[ $key ] = $value;
		}
	}

	/**
	 * Delete cookie by given key
	 *
	 * @param string $key
	 */
	public function removeCookie( $key )
	{
		if ( $key )
		{
			setcookie( $key, '', time( )-3600 , '/' , '' , 0 );
			return true;
		}

		return false;
	}

	/**
	 * Remove all avaible cookies
	 */
	public function removeAllCookies( )
	{
		foreach ( $this->data AS $key => $value )
		{
			setcookie( $key, '', time( )-3600 , '/' , '' , 0 );
		}
	}
}

?>
<?PHP
/**
 * parseurl.php Check the url an gives the correct controller.
 */

/**
 * ParseUrl is invoked from Dispatcher class and it manages the current request.
 */
class ParseUrl
{
	
	/**
	 * Store URL config file data.
	 *
	 * @var array.
	 */
	private $url_data;

	/**
	 * Controller to execute.
	 *
	 * @var string.
	 */
	private $controller = '';
	
	/**
	 * Store current instance.
	 *
	 * @var ParseUrl.
	 */
	private static $_instance = null;

	/**
	 * Initializes the main data.
	 *
	 * Private for Singleton.
	 */
	private function __construct( )
    {
		$this->loadUrlConfig( );
		$this->getControllerByRequestUri( );
    }
    
    /**
     * Private for Singleton.
     */
    private function __clone( ){}
    
    /**
     * Returns the current instance.
	 *
     * @return ParseUrl.
     */
    public static function getInstance( )
    {
    	if ( null === self::$_instance )
    	{
    		self::$_instance = new self;
    	}
    	return self::$_instance;
    }

	/**
	 * Get the controller for the current request.
	 * 
	 * @return string.
	 */
	public function getController( )
	{
		if ( !$this->controller ) throw new Exception404 ( );

		return $this->controller;
	}
	

	/**
	 * Load URL to the private property..
	 */
    protected function loadUrlConfig( )
    {
        require_once CORE__CONFIG_DIR . '/url.config.php';
		$this->url_data = $__core_url_config;
    }

	/**
	 * Process the request to get the main controller.
	 * Needs to be called before getController method.
	 */
	protected function getControllerByRequestUri( )
	{
		$request_uri	= FilterServer::getInstance( )->getText( 'REQUEST_URI' );
		$request_uri	= substr( $request_uri, 1 );
		$request_uri	= $this->cleanRequestUri( $request_uri );

		foreach ( $this->url_data as $url_config )
		{
			$url_config_match = $this->escapeRequestUri( $url_config[ 'request_uri' ] );

			if ( preg_match( "/^{$url_config_match}$/i", $request_uri, $matches ) )
			{
				if ( array_key_exists( 'params', $url_config ) )
				{
					$keys = $url_config[ 'params' ];
					$this->parseMatches2UriParams( $keys, $matches );
				}
					
				$this->controller = $url_config[ 'controller' ];
				break;
			}
		}
	}
	
	/**
	 * Look for a query string in the request uri, and if found, erases it.
	 * 
	 * @return string $request_uri.
	 */
	private function cleanRequestUri( $request_uri )
	{
		$request_uri	= str_replace( '?' . FilterServer::getInstance( )->getText( 'QUERY_STRING' ), '', $request_uri );
		return $request_uri;
	}
	
	/**
	 * Escape \ for the preg_match
	 * 
	 * @return string $request_uri
	 */
	private function escapeRequestUri ( $request_uri )
	{
		$request_uri	= str_replace( '/', '\/', $request_uri );
		return $request_uri;
	}
	
	/**
	 * Store $matches into params parsed with $keys.
	 *
	 * @param String $keys.
	 * @param array $matches.
	 */
	private function parseMatches2UriParams ( $keys, array $matches )
	{
		$keys		= explode( ':', $keys );
		$matches	= array_slice( $matches, 1 );
		$match_num	= count( $matches );
		$params = array( );

		for ( $i = 0; $i < $match_num; $i++ )
		{
			if ( $keys[ $i ] )
			{
				$params[$keys[$i]] = $matches[ $i ];
			}
		}
		FilterUri::getInstance( $params );
	}
}


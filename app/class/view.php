<?php
/**
 * view.php View template system.
 *
 * @author meneame group.
 */

/**
 * Manage templates and vars.
 */
class View
{
	/**
	 * Template dir and name.
	 *
	 * @var string.
	 * Valid example:
	 * 'home/index.tpl.php'
	 */
	protected $template_name = '';

	/**
	 * Template extension.
	 *
	 * @var string.
	 */
	protected $template_extension = '.tpl.php';

	/**
	 * Store all assigns.
	 *
	 * @var array.
	 */
	protected $properties = array( );

	/**
	 * Render the template if this var is TRUE.
	 *
	 * @var boolean.
	 */
	protected $render_template = true;

	/**
	 * Engine for render templates.
	 *
	 * @var ViewInterface.
	 */
	protected $engine;

	/**
	 * Initialice view class.
	 *
	 * @param ViewInterface $engine.
	 */
	public function __construct( ViewInterface $engine )
	{
		$this->engine = $engine;
	}

	/**
	 * Set the current template.
	 *
	 * @param string $_template_name.
	 */
	public function setTemplate( $_template_name )
	{
		$this->template_name = $_template_name;
	}

	/**
	 * Assign a value to a specific property.
	 *
	 * @param string $property.
	 * @param mixed $value.
	 */
	public function assign( $property, $value )
	{
		if ( empty( $property ) )
		{
			throw new Exception( 'Invalid property assigned.' );
		}

		$this->properties[ $property ] = $value;
	}

	/**
	 * Print the template if needed.
	 *
	 * @return boolean.
	 */
	public function render( )
	{
		if ( !$this->render_template )
		{
			return false;
		}

		if ( empty ( $this->template_name ) )
		{
			throw new Exception ( 'Invalid template' );
		}

		$file_path = $this->template_name . $this->template_extension;
		echo $this->engine->render( $file_path, $this->properties );

		return true;
	}

	/**
	 * Allow to show the rendered template.
	 *
	 * @param boolean $boolean.
	 */
	public function setRender( $boolean )
	{
		$this->render_template = !!$boolean;
	}

	/**
	 * Get properties from the template.
	 *
	 * @param string $name.
	 * @return mixed.
	 */
	public function __get( $name )
	{
		return $this->properties[ $name ];
	}

	/**
	 * Checks if the property is setted.
	 *
	 * @param string $key the key to be searched.
	 * @return string false	if not exist, the key if it exist.
	 */
	public function __isset( $key )
	{
		return array_key_exists( $key, $this->properties );
	}
}

?>
<?php
/**
 * urlcheck.php Class for test urls.
 *
 * @author meneame group.
 *
 */

/**
 * Class for check if an url is active and return 200 code.
 *
 * @author meneame group.
 *
 */
class UrlCheck
{
	/**
	 * Check if given url return a 200 code.
	 *
	 * @param string $url.
	 */
	public function isActive( $url )
	{
		$url_valid = filter_var( $url, FILTER_VALIDATE_URL );

		if ( !$url_valid )
		{
			return false;
		}

		$ch = curl_init( );

		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_NOBODY, TRUE );
		curl_exec( $ch );

		$httpCode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
		$is_ok = $httpCode == 200;

		return $is_ok;
	}
}

?>
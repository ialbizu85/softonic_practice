<?php
/**
 * cache.php Define cache class.
 *
 * @author meneame group
 */

/**
 * Class cache to administrate cache.
 */
class Cache implements CacheStrategyInterface
{

	/**
	 * Memcache instance
	 *
	 * @var CacheAdapterInterface.
	 */
	private $cache;

	/**
	 * Cache keys group list
	 *
	 * @var array
	 */
	private $cache_key_list;

	/**
	 * Relation between group to delete cache
	 *
	 * @var array
	 */
	private $delete_cache_data;

	/**
	 * Cache key ttl value.
	 *
	 * @var int
	 */
	private $ttl_cache_key_list;

	/**
	 * Cache key list value.
	 *
	 * @var array
	 */
	private $cache_data;

	/**
	 * Application id
	 * 
	 * @var string
	 */
	private $app_id;

	/**
	 * Create an instance.
	 * Load cache conf and store cache key list in cache
	 */
	public function  __construct( )
	{
		$this->loadCacheConfig( );
	}

	/**
	 * Load CACHE to the private property.
	 */
    protected function loadCacheConfig( )
    {
        require CORE__CONFIG_DIR . '/cache.config.php';
		$this->cache_key_list		= $__core_cache_config;
		$this->delete_cache_data	= $__core_delete_cache_config;
		$this->cache_data			= $__cache_key_list;
		$this->ttl_cache_key_list	= $__ttl_cache_key_list;
		$this->app_id				= $__app_id;
		$this->cache				= new $__cache_type( );
    }

	/**
	 * Store information.
	 * 
	 * @param string $key. Key of the value
	 * @param mixed $value
	 * @param int $ttl
	 */
	public function store( $key, $value, $ttl)
	{
		$this->cache->store( $key, $value, $ttl );

		//Obtener el grupo correspondiente a la key
		$group		= $this->getKeyGroup( $key );

		//Obtener lista de keys del cache
		$key_list	= $this->cache->fetch( $this->cache_data, array( ) );

		//Existe el grupo
		if ( array_key_exists( $group, $key_list ) )
		{
			$key_list[ $group ][] = $key;
		}
		//No existe el grupo
		else
		{
			$key_list = array( $group => array( $key ) );
		}

		//Almacenar la lista de grupos de keys en cache.
		$this->cache->store( $this->cache_data, $key_list, $this->ttl_cache_key_list );
	}

	/**
	 * Return value of the key from cache
	 * 
	 * @param string $key. Cache key
	 */
	public function fetch( $key )
	{
		$this->cache->fetch( $key );
	}

	/**
	 * Delete all values belong to group from cache
	 * 
	 * @param string $model
	 * @param string $method
	 */
	public function deleteByGroup( $model, $method )
	{
		//Obtener grupo de keys a eliminar
		$group_list	= $this->delete_cache_data[ $model ][ $method ];

		//Obtener lista de keys
		$key_list = $this->cache->fetch( $this->cache_data );

		//Eliminar todos los valores equivalentes a las keys
		foreach ( $group_list as $group )
		{
			if ( array_key_exists( $group, $key_list ) )
			{
				foreach ( $key_list[ $group ] as $key )
				{
					$this->cache->delete( $key );
				}

				$key_list[ $group ] = array( );
			}
		}


		$this->cache->store( $this->cache_data, $key_list, $this->ttl_cache_key_list );
	}

	/**
	 * Delete a value from cache by key.
	 * 
	 * @param string $key
	 */
	public function delete( $key )
	{
		$this->cache->delete( $key );

		//Obtener lista de keys
		$key_list	= $this->cache->fetch( $this->cache_data );
		$group 		= $this->getKeyGroup( $key );
		
		unset( $key_list[ $group ][ $key ] );
	}


	/**
	 * Generete a cache key.
	 * 
	 * @param string $model
	 * @param string $method
	 * @param array $arguments
	 * @return string
	 */
	public function generateKey( $model, $method, $arguments )
	{
		$key = $this->app_id . '-' . $model . '-' . $method . '-' . serialize( $arguments );
		return $key;
	}

	/**
	 * Return key´s tag to find the group of the key
	 * 
	 * @param string $key
	 * @return string
	 */
	private function getKeyGroup( $key )
	{
		list( $app_id, $model, $method, $value ) = explode( '-', $key );
		return $this->cache_key_list[ $model ][ $method ];
	}


}

?>
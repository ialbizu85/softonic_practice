<?php
/**
 * cacheadapter.iface.php Define cache adapter interface class.
 *
 * @author meneame group.
 */

/**
 * Class cacheadapter to administrate to give the structure of cache class.
 */
interface CacheAdapterInterface {

	/**
	 * Store value with its key in cache.
	 *
	 * @param string $value.
	 * @param string $key. Cache key
	 * @param int $ttl.
	 */
	public function store( $key, $value, $ttl );

	/**
	 * Return value of the key from cache.
	 *
	 * @param string $key
	 * @return mixed
	 */
	public function fetch( $key, $default = false );

	/**
	 * Delete value from cache.
	 *
	 * @param string $key. Cache key
	 */
	public function delete( $key );
}

?>
<?PHP
/**
 * filter.php Class to work with global variables.
 *
 * @author meneame group
 */

/**
 * Class to work with global variables.
 *
 * @abstract
 * @version 1.0
 * @author meneame group.
 * @package Filter.
 */
abstract class Filter
{
	/**
	 * Content of the global variables.
	 *
	 * @access protected.
	 * @var array.
	 */
	protected $data = array( );

	/**
	 * Sanitize the values of a determined field.
	 *
	 * @param mixed $value Value to sanitize.
	 * @see addslashes( )
	 * @see htmlentities( )
	 * @return string
	 */
	private function sanitize( $value )
	{
		return trim( addslashes( htmlentities( $value, ENT_QUOTES, 'utf-8' ) ) );
	}

	/**
	 * Return filtered value.
	 *
	 * @param string $key Key of the array.
	 * @param mixed $default_value Default value to show y the key doesn't exists.
	 * @param string $allowed_html HTML tags allowed.
	 * @use Filter::sanitize( ).
	 * @see array_key_exists( ).
	 * @see strip_tags( ).
	 * @return mixed.
	 */
	public function getText( $key, $default_value = null, $allowed_html = '' )
	{
		if ( array_key_exists( $key, $this->data ) )
		{
			$value = strip_tags( $this->data[ $key ], $allowed_html );
			return $this->sanitize( $value );
		}

		return $default_value;
	}

	/**
	 * Return the numeric value.
	 *
	 * @param string $key Array key.
	 * @param mixed $default_value Default value to show y the key doesn't exists.
	 * @see array_key_exists( ).
	 * @see is_numeric( ).
	 * @return mixed.
	 */
	public function getNumber( $key, $default_value = null )
	{
		if ( array_key_exists( $key, $this->data )
				&& is_numeric( $this->data[ $key ] ) )
		{
			return $this->data[ $key ];
		}

		return $default_value;
	}

	/**
	 * Returns an array.
	 *
	 * @param string $key. Array key.
	 * @param mixed $default_value. Default value to show y the key doesn't exists.
	 * @param string $allowed_html. HTML tags allowed.
	 * @return mixed.
	 */
	public function getTextArray( $key, $default_value = null, $allowed_html = '' )
	{
		$list = array( );

		if ( array_key_exists( $key, $this->data ) )
		{
			$list = $this->data[ $key ];
			foreach ( $list as $clave => $value )
			{
				$value			= strip_tags( $value, $allowed_html );
				$list[ $clave ] = $this->sanitize( $value );
			}

			return $list;
		}

		return $default_value;
	}


     /**
	 * Return an array with numeric values.
	 *
	 * @param string $key. Array key.
	 * @param mixed $default_value. Default value to show y the key doesn't exists.
	 * @return mixed
	 */
	public function getNumericArray( $key, $default_value = null )
	{
		if ( array_key_exists( $key, $this->data ) )
		{
			foreach ( $this->data[ $key ] as $value )
			{
				if ( !is_numeric( $value ) )
				{
					return $default_value;
				}
			}
			return $this->data[ $key ];
		}
		return $default_value;
	}

	/**
	 * Check if exists key in $data.
	 *
	 * @param string $key Key to check.
	 * @return boolean.
	 */
	public function keyExist ( $key )
	{
		return array_key_exists( $key, $this->data );
	}
}

?>
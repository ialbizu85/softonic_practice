<?php
/**
 * memcachedcache.php Class implements cache funtionalities with memcache.
 * 
 * @author meneame group.
 */

/**
 * Class MemcachedCache to work with caches.
 */
class MemcachedCache implements CacheAdapterInterface
{
	/**
	 * Memcache instance.
	 * 
	 * @var Memcache.
	 */
	private $memcache;

	
	/**
	 * Create an instance.
	 * Load cache conf and store cache key list in cache.
	 */
	public function  __construct( ) 
	{
		require CORE__CONFIG_DIR . '/cache.config.php';
		$this->memcache = new Memcache( );
		$this->memcache->addserver( $__memcached_host, $__memcached_port );
	}


	/**
	 * Store information.
	 *
	 * @param string $key. Key of the value.
	 * @param mixed $value.
	 */
	public function store( $key, $value, $ttl )
	{
		$this->memcache->set( $key, $value, false, $ttl );
	}
	

	/**
	 * Return value of the key from cache.
	 *
	 * @param string $key.
	 * @return mixed.
	 */
	public function fetch( $key, $default = false )
	{
		$value = $this->memcache->get( $key );

		if ( false === $value )
		{
			return $default;
		}

		return $value;
	}


	/**
	 * Delete value from cache.
	 *
	 * @param string $key.
	 */
	public function delete( $key )
	{
		$this->memcache->delete( $key );
	}
}

?>
<?php
/**
 * filterserver.php Class to work with $_SERVER global variable.
 *
 * @author meneame group
 */

/**
 * Class to work with $_SERVER global variable.
 *
 * @version 1.0
 * @author meneame group.
 * @package Filter.
 * @subpackage FilterServer.
 */
class FilterServer extends Filter
{
	/**
	 * Instance of the object.
	 *
	 * @static
	 * @access private.
	 * @var FilterServer.
	 */
	private static $instance = null;
	
	/**
	 * Initialize the arrays and the allowed tags to clean.
	 *
	 * @return FilterServer.
	 */
	private function __construct( )
	{
		$this->data = $_SERVER;
		
		$_SERVER = array( );
	}
	
	/**
	 * Return an unique instance each time that we try to acces to $_SERVER variable.
	 *
	 * @static
	 * @return FilterServer.
	 */
	public static function getInstance( )
	{
		if ( null === self::$instance )
		{
			self::$instance = new self( );
		}
		
		return self::$instance;
	}
}

?>
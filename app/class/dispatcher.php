<?php
/**
 * dispatcher.class.php manager the current request. Everything is processed by this class.
 *
 * @author meneame group.
 */
require_once CORE__CLASSES_DIR . '/autoload.php';

/**
 * The dispatcher is the class that's called from the main script. Is like the "main" function in C.
 *
 * @package Core
 * @subpackage Core
 */
class Dispatcher
{
	/**
	 * Store the main controller.
	 * 
	 * @var string
	 */
	protected $controller_name = '';

	/**
	 * Initialice the Dispatcher class.
	 */
	public function __construct( )
	{
		spl_autoload_register( 'framework_autoload' );
	}

	/**
	 * The most important method, here inside all the main code flux and decissions are done.
	 */
	public function executeInstance( )
	{
		try
		{
			$this->getMainController2Execute( );
			$engine		= new TwigView( );
			$view		= new View( $engine );
			$controller = new $this->controller_name( $view );
			$controller->setCache( new Cache( ) );
			$controller->run( );
			$controller->render( );
		}
		catch ( Exception404 $e )
		{
			$e->executeException( );
		}
		catch ( Exception $e )
		{
			echo $e->getMessage( );
		}
	}

	/**
	 * It gets the controller to execute depending of the entry.
	 */
	protected function getMainController2Execute( )
	{
		$parser					= ParseUrl::getInstance( );
		$this->controller_name	= $parser->getController( );
	}
}

?>
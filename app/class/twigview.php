<?php
/**
 * twigview.php Class for manage templates in twig engine.
 */

/**
 * Class for manage templates in twig engine.
 *
 * @author meneame group.
 */
class TwigView implements ViewInterface
{
	/**
	 * Twig Runtime.
	 *
	 * @var Twig_Environment.
	 */
	protected $twig;

	/**
	 * Initialice class and configure twig.
	 */
	public function __construct( )
	{
		$this->configure( );
	}

	/**
	 * Configure a new instance of Twig.
	 */
	public function configure( )
	{
		require CORE__CONFIG_DIR . '/twig.config.php';

		require_once $twig_installation_path . '/Autoloader.php';
		Twig_Autoloader::register( );

		$loader 	= new Twig_Loader_Filesystem( $twig_template_path );
		$this->twig = new Twig_Environment(
										$loader,
										array(
												'cache' => $twig_cache_path,
												'debug' => $twig_debug
										)
		);
	}

	/**
	 * Render fiven template setting all properties and return
	 * formatted result.
	 *
	 * @param string $template_path.
	 * @param array $properties.
	 *
	 * @return string.
	 */
	public function render( $template_path, $properties )
	{
		return $this->twig->render( $template_path, $properties );
	}
}

?>
<?php
/**
 * filterpost.php Class to work with $_POST global variable.
 *
 * @author meneame group
 */

/**
 * Class to work with $_POST global variable.
 * 
 * @version 1.0
 * @author meneame group.
 * @package Filter.
 * @subpackage FilterPost.
 */
class FilterPost extends Filter
{
	/**
 	 * Instance of the object.
	 *
	 * @static
	 * @access private.
	 * @var FilterPost.
	 */
	private static $instance = null;
	
	/**
	 * Initialize the arrays and the allowed tags to clean.
	 * 
	 * @return FilterPost.
	 */
	private function __construct( )
	{
		$this->data = $_POST;
		
		$_POST		= array( );
	}
	
	/**
	 * Return an unique instance each time that we try to acces to $_POST variable.
	 * 
	 * @static
	 * @return FilterPost.
	 */
	public static function getInstance( )
	{
		if ( null === self::$instance )
		{
			self::$instance = new self( );
		}
		
		return self::$instance;
	}
}

?>
<?php
/**
 * error404.php Class to manage 404 error.
 * 
 * @author meneame group.
 */

/**
 * Definición de la clase Exception404.
 */
class Exception404 extends Exception 
{
	/**
	 * Execute exception
	 */
    public function executeException( )
    {
        $this->controller_name	= 'Exception404ExceptionController';
		$engine					= new TwigView( );
        $view					= new View( $engine );
        $controller				= new $this->controller_name( $view );
        $controller->run( );
        $controller->render( );
    }
}

?>
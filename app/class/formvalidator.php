<?php
/**
 * FormValidator.php Class for check forms easily
 *
 * @author meneame group.
 */

/**
 * FormValidator allow to user validate each
 * field of form and get possible errors easily.
 *
 * Default charset to use is 'utf-8', see setCharset( );

 * @author meneame group.
 */
class FormValidator
{
	/**
	 * Name of current charset for use in validators
	 *
	 * @var string
	 */
	protected $charset;

	/**
	 * Store all information abut fields
	 *
	 * @var array
	 */
	protected $fields;

	/**
	 * Indicate position of current field
	 *
	 * @var integer
	 */
	protected $current_field;

	/**
	 * Say if form is valid (true) or an error has occurred (false)
	 *
	 * @var boolean
	 */
	protected $is_form_valid;

	/**
	 * Say if the current field checked is valid (true) or not (false)
	 *
	 * @var boolean
	 */
	protected $is_valid;

	/**
	 * Error message default for checker "required"
	 *
	 * @see FormValidator::required
	 */
	const REQUIRED_ERROR_MESSAGE = 'El campo "%s" no puede estar vacio.';

	/**
	 * Error message default for checker "email"
	 *
	 * @see FormValidator::email
	 */
	const EMAIL_ERROR_MESSAGE = 'El campo "%s" debe contener un correo electrónico válido.';

	/**
	 * Error message default for checker "minLength"
	 *
	 * @see FormValidator::minLength
	 */
	const MIN_LENGTH_ERROR_MESSAGE = 'El campo "%s" debe tener al menos %d carácteres.';

	/**
	 * Error message default for checker "maxLength"
	 *
	 * @see FormValidator::maxLength
	 */
	const MAX_LENGTH_ERROR_MESSAGE = 'El campo "%s" debe contener menos de %d carácteres.';

	/**
	 * Error message default for checker "rangeLength"
	 *
	 * @see FormValidator::rangeLength
	 */
	const RANGE_LENGTH_ERROR_MESSAGE = 'El campo "%s" debe contener entre %d y %d carácteres.';

	/**
	 * Error message default for checker "stringEqual"
	 *
	 * @see FormValidator::stringEqual
	 */
	const STRING_EUQAL_ERROR_MESSAGE = 'Los campos no contienen el mismo "%s"';

	/**
	 * Error message default for checker "minNumber"
	 *
	 * @see FormValidator::minNumber
	 */
	const MIN_NUMBER_ERROR_MESSAGE = 'El número del campo "%s" debe ser mayor que %d.';

	/**
	 * Error message default for checker "maxNumber"
	 *
	 * @see FormValidator::maxNumber
	 */
	const MAX_NUMBER_ERROR_MESSAGE = 'El número del campo "%s" debe ser menor que %d.';

	/**
	 * Error message default for checker "rangeNumber"
	 *
	 * @see FormValidator::rangeNumber
	 */
	const RANGE_NUMBER_ERROR_MESSAGE = 'El número del campo "%s" debe estar %d y %d.';

	/**
	 * Error message default for checker "isNumeric"
	 *
	 * @see FormValidator::isNumeric
	 */
	const IS_NUMERIC_ERROR_MESSAGE = 'El campo "%s" debe contener un número.';

	/**
	 * Error message default for checker "isFloat"
	 *
	 * @see FormValidator::isFloat
	 */
	const IS_FLOAT_ERROR_MESSAGE = 'El campo "%s" debe contener un número decimal.';

	/**
	 * Error message default for checker "isInteger"
	 *
	 * @see FormValidator::isInteger
	 */
	const IS_INTEGER_ERROR_MESSAGE = 'El campo "%s" debe contener un número entero.';

	/**
	 * Error message default for checker "inOptions"
	 *
	 * @see FormValidator::inOptions
	 */
	const IN_OPTIONS_ERROR_MESSAGE = 'No se ha seleccionado un valor válido del campo "%s".';

	/**
	 * Error message default for checker "inOptions"
	 *
	 * @see FormValidator::inOptions
	 */
	const ALL_IN_OPTIONS_ERROR_MESSAGE = 'Se han seleccionado valores inválidos del campo "%s".';

	/**
	 * Initializate object with defaults
	 */
	public function __construct( )
	{
		$this->is_form_valid	= true;
		$this->charset			= 'utf-8';
		$this->current_field	= -1;
	}

	/**
	 * Set a new charset for validators
	 *
	 * @param string $charset
	 * @return boolean
	 */
	public function setCharset( $charset )
	{
		$old_charset = mb_internal_encoding ( );

		if ( mb_internal_encoding ( $charset ) )
		{
			$this->charset = $charset;
			mb_internal_encoding ( $old_charset );

			return true;
		}

		return false;
	}

	/**
	 * Set a new current field for check
	 *
	 * @param string	$name
	 * @param mixed		$value
	 *
	 * @return FormValidator
	 */
	public function setField( $name, $value )
	{
		$field_data = array(
						'name'	=> $name,
						'value' => $value,
						'error' => false
		);

		$this->fields[ ++$this->current_field ] = $field_data;

		$this->is_valid = true;

		return $this;
	}

	/**
	 * If current field is valid return true, false otherwise.
	 *
	 * @return boolean
	 */
	public function isValid( )
	{
		return $this->is_valid;
	}

	/**
	 * If form is valid return true, false otherwise.
	 *
	 * @return boolean
	 */
	public function isFormValid( )
	{
		return $this->is_form_valid;
	}

	/**
	 * Return all messages for each field.
	 * If field don't has error message is false.
	 *
	 * Array example:
	 *   'field1' => 'Error message',
	 *   'field2' => false
	 *
	 * @return array
	 */
	public function getErrors( )
	{
		$errors = array( );

		for ( $i=0; $i<=$this->current_field; ++$i )
		{
			$field_name				= $this->fields[$i]['name'];
			$field_error			= $this->fields[$i]['error'];

			$errors[ $field_name ]	= $field_error;
		}

		return $errors;
	}

	/**
	 * Execute a callback with $extraparams and $filter_value
	 * and check if callback test is passed.
	 *
	 * Examples:
	 *
	 * basic:
	 *	$form_validator->callback( 'is_numeric', 'Should be numeric' );
	 *
	 * extended:
	 *  $form_validator->callback(
	 *							array(
	 *								$this,
	 *								'check'),
	 *							'Some error',
	 *							array('param1')
	 * );
	 *
	 * $this->check( $param1, $filter_value );
	 *
	 * @param mixed		$callback
	 * @param string	$message	Error message
	 * @param array		$params		This params are passed firstly than $filter_Value
	 *
	 * @return	FormValidator
	 */
	public function callback( $callback, $message, $params = array( ) )
	{
		if ( $this->is_valid )
		{
			$field_value	= $this->getCurrentFieldValue( );
			$params[]		= $field_value;

			$is_valid = call_user_func_array( $callback, $params );

			if ( !$is_valid )
			{
				$this->setErrorCurrentField( $message );
			}
		}

		return $this;
	}


	/**
	 * Check if current field match with regex param
	 *
	 * @param string $pattern Regular expression
	 * @param string $message Error message for show if check fails
	 * @return FormValidator
	 */
	public function regex( $pattern, $message )
	{
		if ( $this->is_valid )
		{
			$field_value	= $this->getCurrentFieldValue( );

			$matches		= preg_match ( $pattern, $field_value );
			$is_valid		= ( $matches > 0 );

			if ( !$is_valid )
			{
				$this->setErrorCurrentField( $message );
			}
		}

		return $this;
	}

	/**
	 * Only checks if flag is true.
	 * It's useful for external checks.
	 *
	 * @param	boolean		$is_valid
	 * @param	string		$message	Error message for show if check fails
	 * @return	FormValidator
	 */
	public function isTrue( $is_valid, $message )
	{
		if ( $this->is_valid && !$is_valid )
		{
			$this->setErrorCurrentField( $message );
		}

		return $this;
	}

	/**
	 * Only checks if flag is false.
	 * It's useful for external checks.
	 *
	 * @param	boolean		$is_valid
	 * @param	string		$message	Error message for show if check fails
	 * @return	FormValidator
	 */
	public function isFalse( $is_valid, $message )
	{
		if ( $this->is_valid && $is_valid )
		{
				$this->setErrorCurrentField( $message );
		}

		return $this;
	}


	/**
	 * Check if current field is in options passed by params.
	 *
	 * @param mixed		$options	List of valid options separed by ':' or array
	 * @param string	$delimiter	Default delimiter for options ':'
	 * @param string	$message	Error message for show if check fails
	 * @return FormValidator
	 */
	public function inOptions( $options, $delimiter = ':', $message = self::IN_OPTIONS_ERROR_MESSAGE )
	{
		if ( $this->is_valid )
		{
			$field_value = $this->getCurrentFieldValue( );

			if ( !is_array( $options ) )
			{
				$options = explode( $delimiter, $options );
			}
			$is_valid = in_array( $field_value, $options );

			if ( !$is_valid )
			{
				$message = sprintf( $message, $this->getCurrentFieldName( ) );
				$this->setErrorCurrentField( $message );
			}
		}

		return $this;
	}

	/**
	 * Check if all current field values are in options passed by params.
	 *
	 * @param mixed		$options	List of valid options separed by ':' or array
	 * @param string	$delimiter	Default delimiter for options ':'
	 * @param string	$message	Error message for show if check fails
	 * @return FormValidator
	 */
	public function allInOptions( $options, $delimiter = ':', $message = self::ALL_IN_OPTIONS_ERROR_MESSAGE )
	{
		if ( $this->is_valid )
		{
			$field_value = $this->getCurrentFieldValue( );

			if ( !is_array( $options ) )
			{
				$options = explode( $delimiter, $options );
			}

			$is_valid = true;
			foreach ( $field_value AS $value )
			{
				if ( !in_array( $value, $options ) )
				{
					$is_valid = false;
					break;
				}
			}

			if ( !$is_valid )
			{
				$message = sprintf( $message, $this->getCurrentFieldName( ) );
				$this->setErrorCurrentField( $message );
			}
		}

		return $this;
	}

	/**
	 * Check if current field is a number
	 *
	 * @param string $message Error message for show if check fails
	 * @return FormValidator
	 */
	public function isNumeric( $message = self::IS_NUMERIC_ERROR_MESSAGE )
	{
		if ( $this->is_valid )
		{
			$field_value	= $this->getCurrentFieldValue( );

			$is_valid		= is_numeric( $field_value );

			if ( !$is_valid )
			{
				$message = sprintf( $message, $this->getCurrentFieldName( ) );
				$this->setErrorCurrentField( $message );
			}
		}

		return $this;
	}

	/**
	 * Check if current field is a float
	 *
	 * @param string $message Error message for show if check fails
	 * @return FormValidator
	 */
	public function isFloat( $message = self::IS_FLOAT_ERROR_MESSAGE )
	{
		if ( $this->is_valid )
		{
			$field_value	= $this->getCurrentFieldValue( );

			$is_valid		= is_float( $field_value );

			if ( !$is_valid )
			{
				$message = sprintf( $message, $this->getCurrentFieldName( ) );
				$this->setErrorCurrentField( $message );
			}
		}

		return $this;
	}

	/**
	 * Check if current field is an integer
	 *
	 * @param string $message Error message for show if check fails
	 * @return FormValidator
	 */
	public function isInteger( $message = self::IS_INTEGER_ERROR_MESSAGE )
	{
		if ( $this->is_valid )
		{
			$field_value	= $this->getCurrentFieldValue( );

			$is_valid		= is_integer( $field_value );

			if ( !$is_valid )
			{
				$message = sprintf( $message, $this->getCurrentFieldName( ) );
				$this->setErrorCurrentField( $message );
			}
		}

		return $this;
	}

	/**
	 * Check if current field is filled
	 *
	 * @see FormValidator::setField
	 *
	 * @param	string $message Error message for show if check fails
	 * @return	FormValidator
	 */
	public function required( $message = self::REQUIRED_ERROR_MESSAGE )
	{
		if ( $this->is_valid )
		{
			$field_value	= $this->getCurrentFieldValue( );
			$is_valid		= !empty( $field_value );

			if ( !$is_valid )
			{
				$message = sprintf( $message, $this->getCurrentFieldName( ) );
				$this->setErrorCurrentField( $message );
			}
		}

		return $this;
	}

	/**
	 * Check if field is an email
	 *
	 * @param string $message Error message for show if check fails
	 * @return FormValidator
	 */
	public function email( $message = self::EMAIL_ERROR_MESSAGE )
	{
		if ( $this->is_valid )
		{
			$field_value	= $this->getCurrentFieldValue( );
			$is_valid		= filter_var( $field_value, FILTER_VALIDATE_EMAIL );

			if ( !$is_valid )
			{
				$message = sprintf( $message, $this->getCurrentFieldName( ) );
				$this->setErrorCurrentField( $message );
			}
		}

		return $this;
	}

	/**
	 * Check if string arg is has $min_length minimun of characters
	 *
	 * @param integer	$min_length Minim length for string
	 * @param string	$message	Error message for show if check fails
	 * @return FormValidator
	 */
	public function minLength( $min_length, $message = self::MIN_LENGTH_ERROR_MESSAGE )
	{
		if ( $this->is_valid )
		{
			$field_value	= $this->getCurrentFieldValue( );

			$string_length	= mb_strlen( $field_value, $this->charset );
			$is_valid		= ( $string_length >= $min_length );

			if ( !$is_valid )
			{
				$message = sprintf( $message, $this->getCurrentFieldName( ), $min_length );
				$this->setErrorCurrentField( $message );
			}
		}

		return $this;
	}

	/**
	 * Check if string arg is has $max_length maximun of characters
	 *
	 * @param integer	$max_length Maximun length for string
	 * @param string	$message	Error message for show if check fails
	 * @return FormValidator
	 */
	public function maxLength( $max_length, $message = self::MAX_LENGTH_ERROR_MESSAGE )
	{
		if ( $this->is_valid )
		{
			$field_value	= $this->getCurrentFieldValue( );

			$string_length	= mb_strlen( $field_value, $this->charset );
			$is_valid		= ( $string_length <= $max_length );

			if ( !$is_valid )
			{
				$message = sprintf( $message, $this->getCurrentFieldName( ), $max_length );
				$this->setErrorCurrentField( $message );
			}
		}

		return $this;
	}

	/**
	 * Check if number of characters are between $min_length and $max_length
	 * both inclusive
	 *
	 * @param integer	$max_length Maximun length for string
	 * @param integer	$min_length Minimun length for string
	 * @param string	$message	Error message for show if check fails
	 *
	 * @return FormValidator
	 */
	public function rangeLength( $min_length, $max_length, $message = self::RANGE_LENGTH_ERROR_MESSAGE )
	{
		if ( $this->is_valid )
		{
			$field_value	= $this->getCurrentFieldValue( );

			$string_length	= mb_strlen( $field_value, $this->charset );

			$is_valid		= ( $string_length >= $min_length );
			$is_valid		&= ( $string_length <= $max_length );

			if ( !$is_valid )
			{
				$message = sprintf( $message, $this->getCurrentFieldName( ), $min_length, $max_length );
				$this->setErrorCurrentField( $message );
			}
		}

		return $this;
	}

	/**
	 * Check if given string is equal to field.
	 *
	 * @param string $string_compare
	 * @param string $message			Error message for show if check fails
	 * @return FormValidator
	 */
	public function stringEqual( $string_compare, $message = self::STRING_EUQAL_ERROR_MESSAGE )
	{
		if ( $this->is_valid )
		{
			$field_value	= $this->getCurrentFieldValue( );

			$is_valid		= ( strcmp( $field_value, $string_compare ) === 0 );

			if ( !$is_valid )
			{
				$message = sprintf( $message, $this->getCurrentFieldName( ) );
				$this->setErrorCurrentField( $message );
			}
		}

		return $this;
	}

	/**
	 * Check if number arg is greather or equal than $min_number
	 *
	 * @param integer	$min_number Minim number
	 * @param string	$message	Error message for show if check fails
	 * @return FormValidator
	 */
	public function minNumber( $min_number, $message = self::MIN_NUMBER_ERROR_MESSAGE )
	{
		if ( $this->is_valid )
		{
			$field_value	= $this->getCurrentFieldValue( );

			$is_valid		= ( $field_value >= $min_number );

			if ( !$is_valid )
			{
				$message = sprintf( $message, $this->getCurrentFieldName( ), $min_number );
				$this->setErrorCurrentField( $message );
			}
		}

		return $this;
	}

	/**
	 * Check if number arg is less or equal than $max_number
	 *
	 * @param integer	$max_number Maximun number
	 * @param string	$message	Error message for show if check fails
	 * @return FormValidator
	 */
	public function maxNumber( $max_number, $message = self::MAX_NUMBER_ERROR_MESSAGE )
	{
		if ( $this->is_valid )
		{
			$field_value	= $this->getCurrentFieldValue( );

			$is_valid		= ( $field_value <= $max_number );

			if ( !$is_valid )
			{
				$message = sprintf( $message, $this->getCurrentFieldName( ), $max_number );
				$this->setErrorCurrentField( $message );
			}
		}

		return $this;
	}

	/**
	 * Check if number are between $min_number and $max_number
	 * both inclusive
	 *
	 * @param integer	$max_number Maximun number
	 * @param integer	$min_number Minimun number
	 * @param string	$message	Error message for show if check fails
	 *
	 * @return FormValidator
	 */
	public function rangeNumber( $min_number, $max_number, $message = self::RANGE_NUMBER_ERROR_MESSAGE )
	{
		if ( $this->is_valid )
		{
			$field_value	= $this->getCurrentFieldValue( );

			$is_valid		= ( $field_value >= $min_number );
			$is_valid		&= ( $field_value <= $max_number );

			if ( !$is_valid )
			{
				$message = sprintf( $message, $this->getCurrentFieldName( ), $min_number, $max_number );
				$this->setErrorCurrentField( $message );
			}
		}

		return $this;
	}

	/**
	 * Return value of current field
	 *
	 * @return mixed
	 */
	protected function getCurrentFieldValue( )
	{
		return $this->fields[ $this->current_field ][ 'value' ];
	}

	/**
	 * Return name of current field
	 *
	 * @return mixed
	 */
	protected function getCurrentFieldName( )
	{
		return $this->fields[ $this->current_field ][ 'name' ];
	}

	/**
	 * Set an error for current field.
	 *
	 * @param string $message Error message to be setted
	 */
	protected function setErrorCurrentField( $message )
	{
		$this->is_valid			= false;
		$this->is_form_valid	= false;

		$this->fields[ $this->current_field ][ 'error' ] = $message;
	}

}

?>
<?php

/**
 * Name of cookie for login
 *
 * @var string
 */
$_cookie_login 	= 'A3rCsd_12d90';

/**
 * Seed for hash cookie login in database
 *
 * @var string
 */
$_seed			= '0439u2mc93qu293ceu2389cne23892';

/**
 * Time to expire is three months (7776000 seconds)
 *
 * @var integer
 */
$_expire_time	= 7776000;

?>
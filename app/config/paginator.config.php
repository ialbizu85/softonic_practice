<?php
/**
 * Define all elemets to paginate every lists.
 *
 * @author meneame group
 */

//Elements to show in a page.
$__elements_per_page	= 2;
//Range of page to show in paginator before and after of current page.
$__page_per_range		= 2;

?>
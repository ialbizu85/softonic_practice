<?php
/**
 * twig.config.php Configuration for twig environment
 *
 * @author meneame group
 */

$twig_installation_path	= CORE__VENDOR_DIR . '/Twig';
$twig_template_path		= CORE__TEMPLATE_PATH;
$twig_cache_path		= CORE__CACHE_TEMPLATE_PATH;

/**
 * If setted true, remove cache on all request, else use always cache.
 *
 * @var boolean
 */
$twig_debug				= true;

?>
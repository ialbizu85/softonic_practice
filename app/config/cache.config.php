<?php
/**
 * cache.config.php Configuration of cache system
 *
 * @author meneame group
 */

$__ttl_comments_list		= 1200;
$__ttl_profile				= 1200;
$__ttl_news_list			= 1200;
$__ttl_cache_key_list		= 0;


$__cache_key_list			= 'meneame_cache_key_list';
$__app_id					= 'meneame';

/**
 * Cache we use in framework
 */
$__cache_type = 'ApcCache';

/**
 * Memcached configuration
 */
$__memcached_host = 'localhost';
$__memcached_port = '11211';


/**
 * Configuration example for key
 */
$__core_cache_config = array( );
$__core_cache_config[ 'CommentModel' ]	= array( 'getNewsComments' => 'comment_list' );
$__core_cache_config[ 'UserModel' ]		= array( 'getProfileInfoByUserName' => 'profile' );
$__core_cache_config[ 'NewsModel' ]		= array(
												'getFrontPageNews' => 'frontpage_list',
												'getPopularNews' => 'popular_list',
												'getPendingNews' => 'pending_list',
												'getUserNews' => 'user_list'
);


/**
 * Configuration example for tags
 */

$__core_delete_cache_config = array( );
$__core_delete_cache_config[ 'CommentModel' ]	= array( 'insertComment' => array( 'comment_list' ) );
$__core_delete_cache_config[ 'NewsModel' ]		= array(
														'insertVote' => array(
																			'frontpage_list',
																			'popular_list',
																			'pending_list',
																			'user_list'
														),
														'insertNews' => array( 'pending_list' )
);

?>
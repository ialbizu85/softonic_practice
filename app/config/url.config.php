<?php
/**
 * url.config.php Defines all available URLs.
 *
 * @author meneame group
 */

/**
 * URL container
 */
$__core_url_config = array( );

$__url_config['request_uri']    = '([0-9]+)?'; // Regex.
$__url_config['params']			= 'page';
$__url_config['controller']     = 'FrontpageNewsController';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'recuperar-contrasena'; // Regex.
$__url_config['controller']     = 'RecoverpasswdUserController';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'reenvio-validacion'; // Regex.
$__url_config['controller']     = 'ResendvalidationUserController';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'usuario/modificar'; // Regex.
$__url_config['controller']     = 'ModifyUserController';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'noticias/subir/url'; // Regex.
$__url_config['controller']     = 'UrlNewsController';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'perfil/([a-zA-Z0-9]+)'; // Regex.
$__url_config['params']			= 'username';
$__url_config['controller']     = 'ProfileUserController';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'perfil/([a-zA-Z0-9]+)/noticias(/([0-9]+))?'; // Regex.
$__url_config['params']			= 'username::page';
$__url_config['controller']     = 'UserNewsController';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'login'; // Regex.
$__url_config['controller']     = 'LoginUserController';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'logout'; // Regex.
$__url_config['controller']     = 'LogoutUserController';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'registro'; // Regex.
$__url_config['controller']     = 'RegisterUserController';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'usuario/checktoken'; // Regex.
$__url_config['controller']     = 'ChecktokenUserController';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'usuario/cambiar-contrasena'; // Regex.
$__url_config['controller']     = 'ChangepasswordUserController';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'usuario/validar'; // Regex.
$__url_config['controller']     = 'ValidationUserController';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'usuario/baja'; // Regex.
$__url_config['controller']     = 'UnsubscribeUserController';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'usuario/confirmar-baja'; // Regex.
$__url_config['controller']     = 'UnsubscribeconfirmUserController';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'noticias/subir/publicar'; // Regex.
$__url_config['controller']     = 'UploadNewsController';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'noticias/votar'; // Regex.
$__url_config['controller']     = 'VoteNewsController';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'populares(/([0-9]+))?'; // Regex.
$__url_config['params']			= ':page';
$__url_config['controller']     = 'PopularNewsController';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'pendientes(/([0-9]+))?'; // Regex.
$__url_config['params']			= ':page';
$__url_config['controller']     = 'PendingNewsController';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'noticias/([-a-zA-Z0-9]+)(/([0-9]+))?'; // Regex.
$__url_config['controller']     = 'CommentsNewsController';
$__url_config['params']			= 'title_slug::page';
$__core_url_config[]            = $__url_config;

$__url_config['request_uri']    = 'search(/([0-9]))?'; // Regex.
$__url_config['controller']     = 'SearchNewsController';
$__url_config['params']			= ':page';
$__core_url_config[]            = $__url_config;

?>
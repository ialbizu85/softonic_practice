<?php
/*
 * defines.config.php
 *
 * Constant file container
 */

define( 'URL_DOMAIN'				, 'http://meneame.ion.internship' );
define( 'URL_CSS'					, URL_DOMAIN . '/css' );
define( 'URL_JS'					, URL_DOMAIN . '/js' );

define( 'CORE__APP_PATH'			, CORE__BASE_DIR . '/app' );
define( 'CORE__CONTROLLER_PATH'		, CORE__APP_PATH . '/controller' );
define( 'CORE__MODEL_PATH'			, CORE__APP_PATH . '/model' );
define( 'CORE__TEMPLATE_PATH'		, CORE__APP_PATH . '/template/src' );
define( 'CORE__CACHE_TEMPLATE_PATH'	, CORE__APP_PATH . '/template/cache' );
define( 'CORE__VENDOR_DIR'			, CORE__APP_PATH . '/vendor' );

?>
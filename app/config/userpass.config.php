<?php
/**
 * userpass.config.php Configuration of user information
 */

/**
 * This is the seed for the encrypter algorithms.
 */
$__seed	= 'aJ025beHoo9';

/**
 * Minimum length for user password.
 */
$__min_pass_len	= 6;

/**
 * Maximum length for user password.
 */
$__max_pass_len = 12;

/*
 * Regular expression used for check if password have the correct format.
 */
$__passwd_regex = '/^((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,})$/';

/**
 * Error message used for giving feedback to the user when the password doesn't have
 * the minimum required length.
 */
$__min_pass_len_error = 'La contraseña debe tener un mínimo de 6 caracteres.';

/*
 * Error message used for giving some information to the user if password doesn't
 * have the correct format.
 */
$__passwd_error_msg = 'La contraseña debe contener al menos 6 carácteres, incluyendo' .
					' al menos una letra mayúscula, una letra minúscula y un caracter.';

/**
 * Error message for the password not repeated.
 */
$__passwd_repeated_error = 'Las contraseñas no coinciden';
?>
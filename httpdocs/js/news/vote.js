$( document ).ready( function(){
						$( '.vote' ).click( vote );
					} );

/**
 * Vote a news and get news's votes number.
 */
function vote( e ){
	if( parseInt( e.target.getAttribute( 'id' ) ) == e.target.getAttribute( 'id' ) )
	{
		e.preventDefault();

		var news_id = e.target.getAttribute( 'id' );

		$.ajax({
			url: 'http://' + location.host + '/noticias/votar',
			type: 'GET',
			data: 'news_id=' + news_id,
			dataType: 'json',
			success: function( votes ) {
				var div			= e.target.parentNode;
				var num_votes	= div.childNodes[1];
				var link		= div.childNodes[3];

				div.removeChild( link );
				
				var paragraph		= document.createElement( 'p' );
				paragraph.className = 'vote';
				
				var text			= document.createTextNode( '¡Chachi!' );
				paragraph.appendChild( text );
				
				div.appendChild( paragraph );

				num_votes.firstChild.nodeValue = votes;
			}
		});
	}
}


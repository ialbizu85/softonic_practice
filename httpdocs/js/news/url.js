/*
 * url.js Form validation for news/url.tpl.php
 */

$( document ).ready( function( ) {
	$( document.getElementById( 'urlForm' ) ).bind( 'submit', function( e ) {
		var regex 	= /^https?:\/\/.+$/;
		var url 	= document.getElementById( 'news_url' ).value;
		
		if ( !regex.test( url ))
		{
			e.preventDefault( );
			$( '.error' ).remove( );
			
			addInputError( 'news_url', 'La url debe empezar por http:// o https://');
		}
	});
});
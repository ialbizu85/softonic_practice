/* 
 * comments.js Form validation for news/comments.tpl.php
 */

$( document ).ready( function( ) {
	$( document.getElementById( 'commentForm' ) ).bind( 'submit', function( e ) {
		var comment = document.getElementById( 'comment' ).value;

		$( '.error' ).remove( );

		if ( 0 === comment.length )
		{
			e.preventDefault( );
			addInputError( 'comment', 'El campo de comentario no puede estar vacio.' );
		}
	});
});
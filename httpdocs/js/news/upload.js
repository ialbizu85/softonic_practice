/*
 * upload.js Form validation for news/upload.tpl.php
 */

$( document ).ready( function( ) {
	$( document.getElementById( 'uploadForm' ) ).bind( 'submit', function( e ) {
		var send_form	= true;
		var title 		= document.getElementById( 'title' ).value;
		var summary 	= document.getElementById( 'summary' ).value;
		
		$( '.error' ).remove( );
		
		if ( 120 < title.length )
		{
			send_form = false;
			addInputError( 'title', 'El titulo debe contener un máximo de 120 carácteres.' );
		}
		else if ( 0 === title.length )
		{
			send_form = false;
			addInputError( 'title', 'Debe introducir un titulo para la noticia.' );
		}
		
		if ( 400 < summary.length )
		{
			send_form = false;
			addInputError( 'summary', 'El resumen de la noticia no debe superar los 400 carácteres.' );
		}
		else if ( 0 === summary.length )
		{
			send_form = false;
			addInputError( 'summary', 'Es obligatorio escribir un resumen de noticia.' );
		}
			
		
		if ( !send_form )
		{
			e.preventDefault( );
		}
	});
});
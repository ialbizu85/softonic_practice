/*
 * search.js Form validation for news/search.tpl.php
 */

$( document ).ready( function( ) {
	$( document.getElementById( 'searchForm' ) ).bind( 'submit', function( e ) {
		var send_form	= true;
		var text 		= document.getElementById( 'text' ).value.trim();
		var username 	= document.getElementById( 'username' ).value.trim();

		$( '.error' ).remove( );

		if ( 3 > text.length )
		{
			send_form = false;
			addInputError( 'text', 'El texto a buscar debe contener un mínimo de 3 carácteres.' );
		}

		var search_by_user = document.getElementById( 'filter_user' );

		if( search_by_user.checked )
		{
			if ( 0 == username.length )
			{
				send_form = false;
				addInputError( 'username', 'El usuario por el que se desea buscar debe contener un valor.' );
			}
		}
		
		if ( !send_form )
		{
			e.preventDefault( );
		}
	});

	//Efectos al cambiar el filtro
	$( '.filter' ).bind( 'change', function( e ) {
		var filter 		= e.target.value;
		var username 	= document.getElementById( 'username' );

		if( filter == 'user' ){
			username.disabled = false;
		}
		else{
			username.disabled = true;
			username.value		= '';
		}
	});

	//Al cargar la página habilitar o desabilitar el username textbox
	var search_by_user 	= document.getElementById( 'filter_user' );
	var username 		= document.getElementById( 'username' );

	if( username.value != '' )
	{
		username.disabled = false;
	}
	else{
		username.disabled	= true;
	}

});



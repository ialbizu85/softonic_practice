/*
 * modify.js Form validation for user/modify.tpl.php
 */

$( document ).ready( function( ) {
	$( document.getElementById( 'modifyForm' ) ).bind( 'submit', function( e ) {
		var send_form			= true;
		var old_password		= document.getElementById( 'old_user_passwd' ).value;
		var new_password		= document.getElementById( 'new_user_passwd' ).value;
		var rep_new_password	= document.getElementById( 'rep_new_passwd' ).value;
		var password_regex		= /^((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,})$/;

		$( '.error' ).remove( );

		if ( 0 === old_password.length ){
			send_form = false;
			addInputError( 'old_user_passwd', 'El campo contraseña antigua no puede estar vacio.' );
		}
		else if( !password_regex.test( old_password ) ){
			send_form = false;
			addInputError( 'old_user_passwd', 'El password debe contener al menos' +
											' 6 carácteres, incluyendo al menos una' +
											' letra mayúscula, una letra minúscula y un caracter.' );
		}

		if ( 0 === new_password.length ){
			send_form = false;
			addInputError( 'new_user_passwd', 'El campo nueva contraseña no puede estar vacio.' );
		}
		else if( !password_regex.test( new_password ) ){
			send_form = false;
			addInputError( 'new_user_passwd', 'El password debe contener al menos' +
											' 6 carácteres, incluyendo al menos una' +
											' letra mayúscula, una letra minúscula y un caracter.' );
		}

		if( new_password !== rep_new_password ){
			send_form = false;
			addInputError( 'new_user_passwd', 'Las contraseñas no coinciden' );
		}

		if ( !send_form )
		{
			e.preventDefault( );
		}
	})
});



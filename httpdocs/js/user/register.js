/*
 * register.js Form validation for user/register.tpl.php
 */

$( document ).ready( function( ) {
	$( document.getElementById( 'registerForm' ) ).bind( 'submit', function( e ) {
		
		var send_form		= true;
		var username		= document.getElementById( 'username' ).value;
		var password		= document.getElementById( 'password' ).value;
		var password_rep	= document.getElementById( 'password_rep' ).value;
		var email			= document.getElementById( 'email' ).value;
		var username_regex	= /^(([a-zA-Z0-9]){3,15})$/;
		var password_regex	= /^((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,})$/;
		var email_regex		= /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;

		$( '.error' ).remove( );

		if ( 0 === username.length )
		{
			send_form = false;
			addInputError( 'username', 'El campo username no puede estar vacio.' );
		}

		else if ( !username_regex.test( username ) )
		{
			send_form = false;
			addInputError( 'username', 'Username debe contener entre 3 y 15 carácteres alfanuméricos.' );
		}

		if ( 0 === password.length )
		{
			send_form = false;
			addInputError( 'password', 'El campo password no puede estar vacio.' );
		}

		else if ( !password_regex.test ( password ) )
		{
			send_form = false;
			addInputError( 'password', 'El password debe contener al menos 6 carácteres, incluyendo al menos una letra mayúscula, una letra minúscula y un caracter.' );
		}

		else if ( password !== password_rep )
		{
			send_form = false;
			addInputError( 'password', 'Las contraseñas no coinciden' );
		}

		if ( 0 === email.length )
		{
			send_form = false;
			addInputError( 'email', 'El campo e-mail no puede estar vacio.' );
		}

		else if ( !email_regex.test ( email ) )
		{
			send_form = false;
			addInputError( 'email', 'Debe introducir un email válido.' );
		}

		if ( !send_form )
		{
			e.preventDefault( );
		}
	});
});
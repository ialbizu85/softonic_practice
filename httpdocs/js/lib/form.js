/**
 * This function add a new label to a input
 * that show to user the error.
 *  
 * @param input_id
 * @param message
 */
function addInputError( input_id, message )
{
	var el = document.createElement( 'label' );
	el.setAttribute( 'class', 'error' );
	el.setAttribute( 'for', input_id );
	
	var text = document.createTextNode( message );
	el.appendChild( text );
	
	var paragraph = document.getElementById( input_id ).parentNode;
	paragraph.appendChild( el );
}
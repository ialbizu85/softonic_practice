<?php

//Para mostrar todos los errores
error_reporting(E_ALL|E_NOTICE);

//reconfigurar un parametro del php.init
ini_set( 'html_errors', 1 );
ini_set( 'display_errors', 1 );
/**
 * index.php Entry point to the the Internship Framwork.
 *
 */

define( 'CORE__BASE_DIR'        , realpath( __DIR__ . '/../' ) );
define( 'CORE__CLASSES_DIR'		, CORE__BASE_DIR . '/app/class' );
define( 'CORE__CONFIG_DIR'      , CORE__BASE_DIR . '/app/config' );

require_once CORE__CONFIG_DIR . '/defines.config.php';
require_once CORE__CLASSES_DIR . '/dispatcher.php';

$dispatcher = new Dispatcher( );
$dispatcher->executeInstance( );

?>
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';
 
DROP SCHEMA IF EXISTS `meneame` ;
CREATE SCHEMA IF NOT EXISTS `meneame` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci ;
USE `meneame` ;
 
-- -----------------------------------------------------
-- Table `meneame`.`mnm_user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `meneame`.`mnm_user` ;
 
CREATE  TABLE IF NOT EXISTS `meneame`.`mnm_user` (
  `user_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `username` VARCHAR(255) NOT NULL ,
  `password` CHAR(64) CHARACTER SET 'ascii' COLLATE 'ascii_general_ci' NOT NULL ,
  `email` VARCHAR(255) NOT NULL ,
  `status` ENUM('ACTIVE', 'PENDING') NOT NULL DEFAULT 'PENDING' ,
  `votes` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `published_news` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `received_votes` INT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`user_id`) ,
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) ,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) )
ENGINE = InnoDB;
 
 
-- -----------------------------------------------------
-- Table `meneame`.`mnm_news`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `meneame`.`mnm_news` ;
 
CREATE  TABLE IF NOT EXISTS `meneame`.`mnm_news` (
  `news_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `user_id` INT UNSIGNED NOT NULL ,
  `title` VARCHAR(255) NOT NULL ,
  `title_slug` VARCHAR(255) NOT NULL ,
  `summary` TEXT NOT NULL ,
  `url` VARCHAR(255) NOT NULL ,
  `create_at` TIMESTAMP NOT NULL DEFAULT NOW() ,
  `votes` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`news_id`) ,
  INDEX `fk_mnm_news_mnm_user` (`user_id` ASC) ,
  CONSTRAINT `fk_mnm_news_mnm_user`
    FOREIGN KEY (`user_id` )
    REFERENCES `meneame`.`mnm_user` (`user_id` )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;
 
 
-- -----------------------------------------------------
-- Table `meneame`.`mnm_news_comment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `meneame`.`mnm_news_comment` ;
 
CREATE  TABLE IF NOT EXISTS `meneame`.`mnm_news_comment` (
  `comment_id` INT NOT NULL AUTO_INCREMENT ,
  `news_id` INT UNSIGNED NOT NULL ,
  `author` INT UNSIGNED NOT NULL ,
  `create_at` TIMESTAMP NOT NULL DEFAULT NOW() ,
  `content` TEXT NOT NULL ,
  PRIMARY KEY (`comment_id`) ,
  INDEX `fk_mnm_comment_mnm_user1` (`author` ASC) ,
  INDEX `fk_mnm_comment_mnm_news1` (`news_id` ASC) ,
  CONSTRAINT `fk_mnm_comment_mnm_user1`
    FOREIGN KEY (`author` )
    REFERENCES `meneame`.`mnm_user` (`user_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mnm_comment_mnm_news1`
    FOREIGN KEY (`news_id` )
    REFERENCES `meneame`.`mnm_news` (`news_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
 
 
-- -----------------------------------------------------
-- Table `meneame`.`mnm_user_token`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `meneame`.`mnm_user_token` ;
 
CREATE  TABLE IF NOT EXISTS `meneame`.`mnm_user_token` (
  `user_id` INT UNSIGNED NOT NULL ,
  `token` CHAR(64) NOT NULL ,
  PRIMARY KEY (`user_id`) ,
  CONSTRAINT `fk_mnm_user_token_mnm_user1`
    FOREIGN KEY (`user_id` )
    REFERENCES `meneame`.`mnm_user` (`user_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
 
 
-- -----------------------------------------------------
-- Table `meneame`.`mnm_news_vote`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `meneame`.`mnm_news_vote` ;
 
CREATE  TABLE IF NOT EXISTS `meneame`.`mnm_news_vote` (
  `news_id` INT UNSIGNED NOT NULL ,
  `user_id` INT UNSIGNED NOT NULL ,
  `create_at` TIMESTAMP NOT NULL DEFAULT NOW() ,
  PRIMARY KEY (`news_id`, `user_id`) ,
  INDEX `fk_mnm_news_vote_mnm_news1` (`news_id` ASC) ,
  CONSTRAINT `fk_mnm_news_vote_mnm_user1`
    FOREIGN KEY (`user_id` )
    REFERENCES `meneame`.`mnm_user` (`user_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mnm_news_vote_mnm_news1`
    FOREIGN KEY (`news_id` )
    REFERENCES `meneame`.`mnm_news` (`news_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
 
USE `meneame`;
 
DELIMITER $$
 
USE `meneame`$$
DROP TRIGGER IF EXISTS `meneame`.`mnm_news_vote_ai` $$
USE `meneame`$$
 
 
 
CREATE TRIGGER mnm_news_vote_ai AFTER INSERT
 
ON mnm_news_vote FOR EACH ROW
 
BEGIN
 
    UPDATE
 
        mnm_user
 
    SET
 
        received_votes = received_votes + 1
 
    WHERE
 
        user_id = NEW.user_id;
 
 
 
    UPDATE
 
        mnm_news
 
    SET
 
        votes = votes + 1
 
    WHERE
 
        news_id = NEW.news.id;
 
END
 
$$
 
 
DELIMITER ;
 
 
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
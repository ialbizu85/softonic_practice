/* Select database*/
USE meneame;

/* Insert user */
INSERT INTO mnm_user(username, email,password, status,votes, published_news, received_votes) VALUES('ion','ion@meneame.internship','ion','ACTIVE',10,3,10);
INSERT INTO mnm_user(username, email,password, status,votes, published_news, received_votes) VALUES('jose','jose@meneame.internship','jose','ACTIVE',8,3,8);
INSERT INTO mnm_user(username, email,password, status,votes, published_news, received_votes) VALUES('salva','salva@meneame.internship','salva','ACTIVE',7,2,7);
INSERT INTO mnm_user(username, email,password, status,votes, published_news, received_votes) VALUES('natalio','natalio@meneame.internship','natalio','ACTIVE',4,2,4);


/* Insert news*/
INSERT INTO mnm_news(user_id, title, title_slug, summary, url, votes) VALUES( 1, 'El Gobierno indulta a dos de los 13 directivos de Azucarera Ebro condenados por el fraude del azúcar', 'El-Gobierno-indulta-a-dos-de-los-13-directivos-de-Azucarera-Ebro-condenados-por-el-fraude-del-azucar', 'El Gobierno ha indultado a Guillermo DAubarede Paya y a Fernando Pérez Lope, dos de los trece directivos de la antigua Azucarera Ebro condenados en junio de 2010 por delitos de falsedad documental y contra la Hacienda comunitaria a penas que oscilaban entre un año y nueve meses y nueve años y nueve meses de cárcel por el caso del fraude en el azúcar, según las resoluciones publicadas hoy por el Ministerio de Justicia en el Boletín Oficial del Estado (BOE).', 'http://noticias.interbusca.com/nacional/economialegal.--el-gobierno-indulta-a-dos-de-los-13-directivos-de-azucarera-ebro-condenados-por-el-fraude-del-azucar-20111208174633.html', 4);

INSERT INTO mnm_news(user_id, title, title_slug, summary, url, votes) VALUES( 1, 'Rajoy se encontrará sobre la mesa en Moncloa un informe de la Agencia Tributaria que propone subir el IVA al 20% ', 'Rajoy-se-encontrara-sobre-la-mesa-en-Moncloa-un-informe-de-la-Agencia-Tributaria-que-propone-subir-el-IVA-al-20 ', 'La subida de impuestos, y en concreto del IVA, vuelve a sonar con fuerza como una de las medidas para lograr los objetivos de consolidación fiscal a los que está obligada España. Mariano Rajoy se encontrará a su llegada a La Moncloa con un informe de la Agencia Tributaria que propone una subida del IVA al 20% antes de marzo.', 'http://www.elconfidencialdigital.com/dinero/070684/rajoy-se-encontrara-sobre-la-mesa-en-moncloa-un-informe-de-la-agencia-tributaria-que-propone-subir-el-iva-al-20-debe-hacerlo-antes-de-marzo-para-cumplir-con-el-deficit', 4);

INSERT INTO mnm_news(user_id, title, title_slug, summary, url, votes) VALUES( 2, 'La burbuja inmobiliaria llega a China ', 'La-burbuja-inmobiliaria-llega-a-China', 'China, la segunda economía del mundo, padece ya las consecuencias de la burbuja inmobiliaria. El caso más llamativo es el de una ciudad entera construida en el desierto para un millón de personas y que hoy está vacía. Nadie puede pagar el precio de los pisos.', 'http://www.rtve.es/alacarta/videos/telediario/burbuja-inmobiliaria-llega-china/1268210/', 4);

INSERT INTO mnm_news(user_id, title, title_slug, summary, url, votes) VALUES( 3, 'La acción destructora del fuego', 'La-accion-destructora-del-fuego', 'El fuego es una reacción química de oxidación violenta de una materia combustible, con desprendimiento de llamas, calor y gases, normalmente. Un incendio es una ocurrencia de fuego sin controlar y que en más de un 90% de los casos está ocasionado por el ser humano. Unas imágenes impresionantes de incendios por todo el mundo.', 'http://ibytes.es/blog_el_poder_del_fuego.html', 4);

INSERT INTO mnm_news(user_id, title, title_slug, summary, url, votes) VALUES( 2, 'La empresa que motivó #gratisnotrabajo valora medidas por daños a su imagen', 'La-empresa-que-motivo-gratisnotrabajo-valora-medidas-por-danos-a-su-imagen', '#gratisnotrabajo revuelve el periodismo en la Red Novodistribuciones critica a la joven periodista que denunció una de sus ofertas de trabajo y a la Asociación de la Prensa de Madrid (APM).', 'http://www.elmundo.es/elmundo/2011/12/08/comunicacion/1323345762.html', 3);

INSERT INTO mnm_news(user_id, title, title_slug, summary, url, votes) VALUES( 4, 'Irán muestra imágenes del avión no tripulado capturado', 'Iran-muestra-imagenes-del-avion-no-tripulado-capturado', 'La televisión iraní muestra las primeras imágenes de un avión drone que Teherán dice que derribó cerca de la frontera con Afganistán.', 'http://www.bbc.co.uk/news/world-middle-east-16098562', 3);

INSERT INTO mnm_news(user_id, title, title_slug, summary, url, votes) VALUES( 3, 'La Purísima Concepción no debe confundirse con la maternidad virginal de María', 'La-Purisima-Concepcion-no-debe-confundirse-con-la-maternidad-virginal-de-Maria', 'Viñeta de Manel Fontdevila en "Público"', 'http://blogs.publico.es/manel/4381/8-de-diciembre/', 3);

INSERT INTO mnm_news(user_id, title, title_slug, summary, url, votes) VALUES( 1, 'Anguita: España debe salir del euro y nacionalizar sectores estratégicos', 'Anguita-Espana-debe-salir-del-euro-y-nacionalizar-sectores-estrategicos', 'El que fuera coordinador general de Izquierda Unida entre 1989 y 2000, Julio Anguita, ha propuesto como una de las medidas para salir de la crisis la salida de España del euro, así como la nacionalización de los "sectores estratégicos".', 'http://www.eleconomista.es/economia/noticias/3534703/11/11/Anguita-Espana-debe-salir-del-euro-y-nacionalizar-sectores-estrategicos-.html', 2);

INSERT INTO mnm_news(user_id, title, title_slug, summary, url, votes) VALUES( 4, 'Javier Couso: ´No me da la gana de olvidar, queremos justicia´', 'Javier-Couso-No-me-da-la-gana-de-olvidar-queremos-justicia', 'La muerte de mi hermano y sus compañeros fue premeditada, por defender el derecho a la información». Con esta frase abrió de nuevo Couso el debate de la neutralidad y el derecho a la información en los medios de comunicación.', 'http://www.laopiniondemurcia.es/murcia/2011/12/08/javier-couso-gana-olvidar-queremos-justicia/369834.html', 1);

INSERT INTO mnm_news(user_id, title, title_slug, summary, url, votes) VALUES( 2, 'Condenan a un profesor por pedir sexo a una alumna a cambio de aprobarle una asignatura', 'Condenan-a-un-profesor-por-pedir-sexo-a-una-alumna-a-cambio-de-aprobarle-una-asignatura', 'La Audiencia Provincial ha condenado a un año de prisión y a seis de inhabilitación a un profesor de la Universidad de Málaga (UMA) que pidió sexo a una alumna a cambio de aprobarle la última convocatoria de una asignatura de Magisterio. En concreto, los hechos han sido calificados por la Sala como una falta de vejaciones injustas y un delito de abuso de funcionario en el ejercicio de su función. Además, deberá indemnizar a la joven con 2.000 euros por los daños morales sufridos. Según consta en el apartado de hechos probados... ', 'http://www.europapress.es/sociedad/noticia-condenan-profesor-pedir-sexo-alumna-cambio-aprobarle-asignatura-20111208125459.html', 1);


/* Insert news_vote */
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(1,1);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(1,2);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(1,3);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(1,4);

INSERT INTO mnm_news_vote(news_id,user_id) VALUES(2,1);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(2,2);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(2,3);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(2,4);

INSERT INTO mnm_news_vote(news_id,user_id) VALUES(3,1);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(3,2);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(3,3);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(3,4);

INSERT INTO mnm_news_vote(news_id,user_id) VALUES(4,1);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(4,2);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(4,3);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(4,4);

INSERT INTO mnm_news_vote(news_id,user_id) VALUES(5,1);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(5,2);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(5,3);

INSERT INTO mnm_news_vote(news_id,user_id) VALUES(6,1);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(6,2);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(6,3);

INSERT INTO mnm_news_vote(news_id,user_id) VALUES(7,1);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(7,2);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(7,3);

INSERT INTO mnm_news_vote(news_id,user_id) VALUES(8,1);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(8,2);

INSERT INTO mnm_news_vote(news_id,user_id) VALUES(9,1);

INSERT INTO mnm_news_vote(news_id,user_id) VALUES(10,1);

/* Insert news_comments */
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(1,1,'Toca pagar antes de salir');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(1,2,'Pregunto desde una cierta ignorancia:
¿Tan malo es producir azúcar como para que les caigan penas de prisión? Seguramente si hubiese más producción los precios no habrían subido tanto como lo han hecho en 2011. Supongo que es una forma de defender a los agricultores del norte de europa, pero vamos, pienso que no es para tanto, y seguramente es una política errónea autolimitar la capacidad productiva del conjunto de la UE.');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(1,3,'"Guillermo DAubarede Paya y Fernando Pérez Lope fueron condenados por el Supremo en junio de 2010 como autores y cooperadores necesarios de varios delitos contra la Hacienda europea con penas que oscilaban entre seis meses y un año y nueve meses más un día de prisión y multas que sumaban más de 14 millones de euros para cada uno."

"El fraude ascendió a 27,06 millones de euros, cantidad fijada en concepto de responsabilidad civil para Azucarera Ebro, más los intereses devengados por impago de la exacción reguladora y de la cotización para la compensación de los gastos de almacenamiento."

¿Lo del indulto, incluye las multas de 14 millones de euros a cada uno? Es que vamos, me parecería muy fuerte que defraudases 27 millones y te fueses de rositas.');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(1,4,'Que feo queda "dos" y "13"');


INSERT INTO mnm_news_comment(news_id,author,content) VALUES(2,1,'¿Para que al 20%? Dejémonos de medias tintas y lo ponemos directamente al 100% ');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(2,2,'Esto decían cuando el PSOE subió el IVA:

El PSOE en el año 93 subió el IVA del 12% al 15% y el resultado fue que 500.000 españoles fueron al paro y la economía decreció un 2,5%. El déficit público alcanzó la entonces cifra récord del 7,4%.

Para colmo la recaudación por IVA disminuyó. Ni mejoraron las cuentas públicas, ni el empleo ni la economía, sumida en ese momento en otra crisis que comparativamente no era tan grave como la de ahora.

Desde el 1 de julio de 2010 el PSOE nos vuelve a subir el IVA del 16% al 18% en productos y servicios, y del 7% al 8% en alimentación, transportes y vivienda. Ahora que ZP va a subir el IVA, tenemos cerca de 4,5 millones de parados. El déficit público supera el 11% del PIB.

Y la economía española a diferencia de los países de nuestro entorno, sigue estancada. La subida del IVA en estas circunstancias, cuando las familias españolas se ven obligadas a restringir el consumo y a ahorrar por la incertidumbre, equivale a dar la puntilla a muchos autónomos y pequeñas y medianas empresas que hacen esfuerzos sobrehumanos para resistir, mantener su actividad y mantener su plantilla.

Cuando el consumo está bajo mínimos, la subida de impuestos no puede repercutir en los precios. Y recortar un 2% el margen comercial de los pequeños y medianos empresarios, en las circunstancias actuales, significa abocarles al cierre de sus empresas y al despido masivo.

La única solución es que ZP se apriete el cinturón, suprima ministerios inútiles y gastos superfluos.

ZP no puede pedir más sacrificios a los españoles.

ZP debe empezar por ejemplarizar con su propio gobierno.');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(2,3,'El IVA de qué? si con el minieuro nadie va a poder comprar nada!!!');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(2,4,'el objetivo es un IVA del 25%. Lo hacen poco a poco para minimizar las protestas. ');


INSERT INTO mnm_news_comment(news_id,author,content) VALUES(3,1,'El pocero seal of aproval');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(3,2,'¿Llega? Pero si se sabe hace mucho.');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(3,3,'60 millones de pisos vacíos y el valor de los pisos se ha multiplicado por 8 en 8 años... los chinos imitando son la hostia, nos han superado con creces!!!');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(3,4,'Relacionada, muy bien documentada y muy recomendable, de lo mejor que he leido sobre el tema: www.meneame.net/story/estallido-burbuja-inmobiliaria-china');

INSERT INTO mnm_news_comment(news_id,author,content) VALUES(4,1,'Sobrecogedoras y de una calidad impresionante. ');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(4,2,'Curiosamente, varias especies de eucaliptos dependen de incendios como los que se ven en las fotos (y que tan aterradores nos parecen a los foráneos) para liberar sus semillas y poder continuar con su ciclo. Una rutina necesaria para ellos, vamos');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(4,3,'And it burns, burns, burns, the ring of fire... The ring of fire.

www.youtube.com/watch?v=gRlj5vjp3Ko');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(4,4,'Imágenes fatídicas pero a la vez muy hermosas. Joder, cualquiera que me oiga lea se va a pensar que soy un pirómano, pero vamos, que no.');

INSERT INTO mnm_news_comment(news_id,author,content) VALUES(5,1,'todas las empresas hacen lo mismo con sus trabajadores... o es que pagar 850 euros al mes a un ingeniero no es lo mismo?????');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(5,2,'si, es lo mismo y es una vergüenza también');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(5,3,'La periodista no lanzó "mensajes negativos", dijo la verdad, si creen que es tan malo, que no paguen eso.');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(5,4,'Delincuentes sin ley que se creen que por tener una empresa están por encima del bien y del mal, ¿nadie puede poner freno a estos psicópatas? #gratisnotrabajoyotampoco');


INSERT INTO mnm_news_comment(news_id,author,content) VALUES(6,1,'Relacionada: www.meneame.net/go.php?id=1462331');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(6,2,'Si hubiese sido al reves, eeuu nos mete en la 3ª guerra mundial.
Es lo que tiene vivir de las guerras, asesinos.');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(6,3,'Si nos va a meter en la 3º guerra mundial igualmente.');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(6,4,'Que lo desarmen y les cojan prestada la tecnología');

INSERT INTO mnm_news_comment(news_id,author,content) VALUES(7,1,'Es curioso, pero hay muchos católicos practicantes que confunden estos dos dogmas. Por ejemplo, hasta en la beata Intereconomía los han confundido: lacajadebajodelacam.blogspot.com/2009/12/intereconomia-catolicos-pero-');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(7,2,'Ostras, pos ahora me entero.
es.wikipedia.org/wiki/Purisima_Concepcion');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(7,3,'Y por FSM, no confundir con la Anunciación!!!... que lo del palomo y Mari se conmemora el 25 de marzo (convenientemente 9 meses antes de Navidad)...');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(7,4,'Qué esperabas? allí se hace lo que Benny mande, nada de cuestionarse ni informarse, no vaya ser que piensen demasiado y... ');

INSERT INTO mnm_news_comment(news_id,author,content) VALUES(8,1,'Dejaros de tonterías que me veo que el sueldo en pesetas y la hipoteca en € (que es en lo que la contraté, y el € no desaparece, ende...), 3 nóminas para pagar una letra.

Y ya me tragué la conversión en su momento, y no estoy dispuesto ha echar más noches, "si es una tontería solo hay que dividir por 166,386", eso será para el director económico para el puto informático era mucho más que eso.');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(8,2,'Uuuuuuuuuuuuuuuh');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(8,3,'¿Alemania ha nacionalizado a Siemens y yo sin enterarme?, ¿quizás algún banco ruinoso?, en serio desconozco que nacionalización ha hecho Alemana en telecos, petroleras, eléctricas, industria pesada.....');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(8,4,'Ya han venido los fachas, no te preocupes... cuando uno se viste mal, se dice vaya facha que me lleva, si son muchos vaya fachas que me llevan...');

INSERT INTO mnm_news_comment(news_id,author,content) VALUES(9,1,'Muchos tampoco olvidaremos, Juan!
Mientras quede gente como tú, que crea en la justicia y luche por ella (aunque la justicia no este de tu lado), habrá esperanza en que este país tiene una solución. Lo malo será cuando todos lo demos todo por perdido.');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(9,2,'Se invocan muchos derechos y se habla poco de obligaciones. Esto ocurrió en época de los malos malísimos Busch y Aznar y recuerdo el plante de los periodistas gráficos al último pero les sustituyó los buenos buenísimos de Obama y Zp y la cosa sigue igual. Mal asunto.');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(9,3,'un capitán de USA disparó su tanque desde la orilla contraria del Tigris al hotel Palestina, donde era sobradamente conocido que estaban los periodistas cubriendo la invasión de Irak sin beneplácito de la ONU. Ese petardazo mató a su hermano que iba de cámara con Jon Sistiaga.

es.wikipedia.org/wiki/Jos%C3%A9_Couso_Permuy');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(9,4,'Era sobradamente conocido, me alegro por fin de conocer la opinión de un soldado americano que entró en combate ese día.');


INSERT INTO mnm_news_comment(news_id,author,content) VALUES(10,1,'No entiendo en estos casos (en los que se considera probado) que tenga solo seis meses de inhabilitación');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(10,2,'Lo realmente increíble sería encontrar ni que fuera una única sentencia de culpabilidad en la que todo el mundo estuviera de acuerdo.');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(10,3,'Son seis años de inhabilitación (y uno de prisión)');
INSERT INTO mnm_news_comment(news_id,author,content) VALUES(10,4,'Aún así para mi esta gente debería ir a un psiquiátrico, por que no creo que cuando salga de la carcel pueda uno confiar en ella. Alguien que arriesga su carrera de esa forma puede cometer una locura en cualquier momento, que haga mucho daño a otras personas.');

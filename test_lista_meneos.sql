/* Insert users */
INSERT INTO mnm_user(user_id,username,password,email,status) VALUES(1, 'ion', 'Abc12345', 'ion@meneame.internship','ACTIVE');
INSERT INTO mnm_user(user_id,username,password,email,status) VALUES(2, 'salva', 'Abc12345', 'salva@meneame.internship','ACTIVE');
INSERT INTO mnm_user(user_id,username,password,email,status) VALUES(3, 'jose', 'Abc12345', 'jose@meneame.internship','ACTIVE');
INSERT INTO mnm_user(user_id,username,password,email,status) VALUES(4, 'natalio', 'Abc12345', 'natalio@meneame.internship','ACTIVE');

/* Insert news */
INSERT INTO mnm_news(news_id,user_id,title,title_slug,summary,url,votes) VALUES(1, 1, 'Hau lehenengo berria da','Hau-lehenengo-berria-da', 'Lehenego berriaren laburpena da.','http://berria1.com', 4);
INSERT INTO mnm_news(news_id,user_id,title,title_slug,summary,url,votes) VALUES(2, 1, 'Hau bigarren berria da','Hau-bigarren-berria-da', 'Bigarren berriaren laburpena da.','http://berria2.com', 3);
INSERT INTO mnm_news(news_id,user_id,title,title_slug,summary,url,votes) VALUES(3, 2, 'Hau hirugarren berria da','Hau-hirugarren-berria-da', 'Hirugarren berriaren laburpena da.','http://berria3.com', 2);
INSERT INTO mnm_news(news_id,user_id,title,title_slug,summary,url,votes) VALUES(4, 3, 'Hau laugarren berria da','Hau-laugarren-berria-da', 'Laugarren berriaren laburpena da.','http://berria4.com', 1);

/* Insert news votes */
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(1, 1);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(1, 2);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(1, 3);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(1, 4);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(2, 1);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(2, 2);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(2, 3);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(3, 1);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(3, 2);
INSERT INTO mnm_news_vote(news_id,user_id) VALUES(4, 1);
